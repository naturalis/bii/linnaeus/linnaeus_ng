# Linnaeus Next Generation

[![pipeline status](https://gitlab.com/naturalis/bii/linnaeus/linnaeus_ng/badges/develop/pipeline.svg)](https://gitlab.com/naturalis/bii/linnaeus/linnaeus_ng/-/commits/develop)

Linnaeus is a program, web application developed by Naturalis to setup and manage
taxonomic databases. It is used for many public project such as
[Nederlandse Soorten Register](https://www.nederlandsesoorten.nl/)
and [Dutch Caribbean Species](https://www.dutchcaribbeanspecies.org/),
but also for [Dierenzoeker](https://www.dierenzoeker.nl/) and
many others listed at [linnaeus.naturalis.nl](https://linnaeus.naturalis.nl/)
and [determineren.nederlandsesoorten.nl](https://determineren.nederlandsesoorten.nl).

This repository is now the basis of a dockerized version of the linnaeus system and
should be installed using docker-compose.

## Get started

Clone this repository.

- [download and install php dependency manager composer](https://getcomposer.org/)
- `mkdir -p data/database/init`
- `cp database/init/*.gz data/database/init/`
- `sudo echo "127.0.0.1  linnaeus.dryrun.link" >> /etc/hosts`
- Create a `.env` file with two values:

```bash
AWS_ACCESS_KEY_ID=<copy paste Bitwarden traefik-route53-dryrun.link access key id>
AWS_SECRET_ACCESS_KEY=<copy paste Bitwarden traefik-route53-dryrun.link secret access key>
```

- `./start.sh`

In a minute you can open your browser to
[linnaeus.dryrun.link](https://linnaeus.dryrun.link),
be sure to check that you have no web server running on your local environment,
before starting this script.

What the `start.sh` script does is a simple:

```bash
docker-compose up
```

Which equals to:

```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml
```

`docker-compose.override.yml` is a sublink to `overrides/docker-compose.dev-default.yml`
which starts the development version of a non-integrated site.

This will start linnaeus with the local code shared and it will use the _develop_ versions
of the latest [docker-linnaeus](https://gitlab.com/naturalis/bii/linnaeus/docker-linnaeus) images.
These are exactly the same compared to the ones running linnaeus in production but xdebug has
been added and is switched on.

By default an init database is read which resides in `./data/database/init/initdb.sql.gz` this
contains a very basic project without any data. The default credentials are 'sysadmin/sysadmin'.

If you want to use the database of an existing project, `mysql-dump`, download and put it in
`./data/database/init/`. Stop the environment. Delete the `./data/database/storage`
directory and start again. It will now load the other database while starting.

## Integrated

If you want to run Linnaeus integrated with for instance the drupal site
nederlandsesoorten.nl (or grasshoppers, or dcsr)
you need to start it with the following command:

```bash
docker-compose -f docker-compose.yml -f overrides/docker-compose.dev-integrated.yml up
```

This will start all containers needed to serve the Linnaeus content but depends on the
Traefik of the other project. See the `docker-composer.integrated.yml` for all specific configuration.

Do not forget that you also need the mandatory `/linnaeus_template` route in de Drupal
instance, which will be used to retrieve the base template for linnaeus.

## Linnaeus remote configuration

Usually Linnaeus is already running on a remote virtual host and already setup using
[ansible](/naturalis/bii/linnaeus/linnaeus_ng/-/tree/develop/ansible).

### Docker

`/opt/project/linnaeus_ng`

This directory contains a clone of [docker composer configuration](https://github.com/naturalis/docker-linnaeusng)
and is running a docker composer. This can be verified by logging into _the host_ machine.

```bash
cd /opt/project/linnaeus_ng
docker-compose ps
```

This should show a running instance consisting of the following components:

```bash
Name                       Command                  State        Ports
---------------------------------------------------------------------------------
linnaeus_ng_database_1       docker-entrypoint.sh mysqld     Up (healthy)
linnaeus_ng_docker-proxy_1   /docker-entrypoint.sh hapr      Up
                             ...
linnaeus_ng_nginx_1          /docker-entrypoint.sh ngin      Up             80/tcp
                             ...
linnaeus_ng_php-fpm_1        docker-php-entrypoint php-fpm   Up             9000/tcp
linnaeus_ng_traefik_1        /entrypoint.sh --global.se      Up             0.0.0.0:443->443/tcp,:::443->
                             ...                                            443/tcp, 0.0.0.0:80->80/tcp,:
                                                                            ::80->80/tcp
```

The setup contains a mariadb database, nginx and a php-fpm service stack.
To connect to the database
from _the host_ you can use the configuration parameters from `/opt/project/linnaeus_ng/.env`.
The `/project/linnaeus_ng/env/php-fpm.env` file contains these fields:

- `MYSQL_DATABASE`
- `MYSQL_USER`
- `MYSQL_PASSWORD`
- `MYSQL_ROOT_PASSWORD`

The other environment variables that can be set is:

- `IMAGE_VERSION`, which can pin an installation to a certain image.
- `INTEGRATED_TEMPLATE`, which is set by default to specify this is an integrated setup.
- `SKIN`, to set the default skin used.
- `FIXED_PROJECT_ID`, the id of the main project (0 if multi)
- `AWS_ACCESS_KEY_ID`, the key id for the aws user used for route53/letsencrypt
- `AWS_SECRET_ACCESS_KEY`, the key for the aws user used for route53/letsencrypt

In `configuration/app/Configuration.php` and `configuration/admin/Configuration.php` other
environments can be set, but they should be added to the docker-compose configuration first.

## Configurations

Many of the linnaeus configurations are basically the same.
There are three different setups used:

- solo

These configurations contain only one taxonomic project. This is the most common
setup for single purpose sites. The most popular example is for
instance
[dierenzoeker](https://www.dierenzoeker.nl/)
which has it's own skin, another site which uses the default skin is for instance
[Euphasiids](https://euphausiids.linnaeus.naturalis.nl/).

- integrated/hybrid

These configurations are also single taxonomic projects, but they are combined
with another application. Currently there are three instances using this type
of configuration:
[nederlandsesoorten.nl](https://www.nederlandsesoorten.nl),
[dutch caribbean species register](https://www.dutchcaribbeanspecies.org/) and
[grasshoppers of europe](https://www.grasshoppersofeurope.com/).

- multiple

These configurations contain many taxonomic projects and start with a list links
to each project. Good examples are:
[legacy](https://legacy.linnaeus.naturalis.nl/linnaeus_ng/) and
[determineren](https://determineren.nederlandsesoorten.nl/linnaeus_ng/).

The difference in these setup is basically done in the database. Some of these setups
use their own graphical interface images which reside in
`public/shared/media`. Another difference can also be found in some configurations
using a special rewrite script to rewrite incoming requests to the correct paths in the
application. These are used in the integrated setups.

## Development

Note that docker-compose override of the development environment uses the shared mount
directories, you can check this setup in docker-compose.override.yml.
The development environment also uses a
[development version of the docker image](https://gitlab.com/naturalis/bii/linnaeus/docker-linnaeus/-/blob/master/php/Dockerfile#L40) which contains a php
version with xdebug switched on. You can connect to this in your IDE,
this has been tested with PhpStorm.

To do this you need enable 'Listening to PHP debug connections', check the
[Intellij PhpStorm documentation](https://www.jetbrains.com/help/phpstorm/php-debugging-session.html).
You also need the xdebug helper installed in your browser
([firefox](https://addons.mozilla.org/nl/firefox/addon/xdebug-helper-for-firefox/),
[chrome](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc)).

You can verify if XDebug is correctly configured trying to connect to your local host by stopping your
IDE and inspecting the logs of php-fpm `docker-compose logs -f php-fpm`, you should see:

```bash
php-fpm_1       | NOTICE: PHP message: Xdebug: [Step Debug] Could not connect to debugging client. Tried: host.docker.internal:9000 (through xdebug.client_host/xdebug.client_port) :-(
```

When the IDE is listening to debug connections, also enable **Break at first line in PHP script**.
Now it should always stop whenever you do an http request via the browser. Familiarize yourself
with the
[many options of ide debugging in phpstorm](https://www.jetbrains.com/help/phpstorm/debugging-with-phpstorm-ultimate-guide.html),
try to refrain from dumping content to the browser or the console, if it is necessary, be sure
to remove this code as soon as you no longer need it: **check the diffs of your merge requests**.

## Schemaspy

If you want to create the schemaspy documentation you first need to create the destination directory:

```
mkdir public/schemaspy
chmod 777 schemaspy
```

During startup of the development environment up-to-date database documentation is also
generated by invoking [schemaspy](https://schemaspy.org),
this software analyses the database and creates a schematic overview of all the tables.

You can view the generated documentation by opening your browser directly to the index.html.
For instance when starting the nsr development version (integrated) this would be:

https://nsr.dryrun.link/linnaeus_ng/schemaspy/index.html

Or when using a single version of linnaeus:

https://linnaeus.dryrun.link/linnaeus_ng/schemaspy/index.html

The configuration of schemaspy is done in `schemaspy/schemaspy.properties` and the
relations of the tables are defined in `schemaspy/meta/develop.meta.xml`.

## Third party javascripts

You can install the third party javascript libraries and build them:

```bash
npm install --global gulp
npm install
gulp
```

Gulp can fail in the first run, because some gulp packages need to be installed globally
first, follow the hints given in the errors. Gulp will generate the following files that are
not (and should not be) in the git repository:

- `./public/app/vendor/bundle.js`
- `./public/app/vendor/*`
- `./public/admin/vendor/bundle.js`
- `./public/admin/vendor/*`

## Code quality

The code quality is checked in the gitlab pipeline.
By default the pipeline will run the following checks:

 - [phplint](https://github.com/php-parallel-lint/PHP-Parallel-Lint) syntax check
 - [phpstan](https://phpstan.org/) static analysis
 - [php codesniffer](https://github.com/PHPCSStandards/PHP_CodeSniffer) code style
 - [phpmd](https://phpmd.org/) mess detection

The last of these checks are important, but can be very strict.
If you want to run these checks locally you can use the following commands:

```bash
docker run --rm --interactive --tty \
  --volume $PWD:/var/www \
  registry.gitlab.com/naturalis/bii/linnaeus/docker-linnaeus/php-dev:latest /bin/sh -c 'composer lint'
```

## Tools

### tools/update_password.php

To set the password of a user via the command line you can use a
tools script. Reset the password to 'sysadmin' for the sysadmin user:

```bash
docker-compose exec -T php-fpm /bin/sh -c 'php tools/update_password.php sysadmin sysadmin'
```

### tools/update_setting.php

To set the value of a certain setting you can either use stdin to pass the content of a
value throught the script, for instance:

```bash
cat contents-of-file.txt | docker-compose exec -T php-fpm /bin/sh -c 'php tools/update_setting.php the_variable'
```

Or use it as a second parameter:

```bash
docker-compose exec -T php-fpm /bin/sh -c 'php tools/update_setting.php the_variable "value"'
```

To set the default value of a setting you can use the `--default` flag.

```bash
docker-compose exec -T php-fpm /bin/sh -c 'php tools/update_setting.php --default the_variable "value"'
```

## Pre-commit

If you want to actively work on this project, it is mandatory to install
[pre-commit](https://pre-commit.com/#install)
and [gitleaks](https://github.com/gitleaks/gitleaks) to make absolutely sure that you do not add any sensitive information
(keys, password, tokens) to this repository.

1. Install [pre-commit](https://pre-commit.com/#install)
2. Install [git-leaks](https://github.com/gitleaks/gitleaks)
3. `pre-commit install` and `pre-commit autoupdate`

The Gitleaks check is also added to the gitlab pipeline.
Even if you haven't installed these tools, Gitleaks will still scan your commit after a push.
Any credentials leaked this way, should be revoked, replaced and reported,
because the git history never forgets and the repository is open.

## Trivyignore

The gitlab pipeline checks the security of the build docker images using
[trivy](https://trivy.dev/).
Whenever trivy throws up security issues that stop the pipeline but
can't be fixed yet you can add a CVE number of the issue to the
ignore file.
This process has been centralized in
[lib / trivyignore](https://gitlab.com/naturalis/lib/trivyignore),
you should follow the
[instructions](https://gitlab.com/naturalis/lib/trivyignore#procedure) in the project.
