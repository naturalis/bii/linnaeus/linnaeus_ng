#!/bin/sh
set -eu

docker create --name vault registry.gitlab.com/naturalis/lib/docker/vault:latest
docker cp vault:/bin/vault vault
docker rm vault

# check if VAULT_TOKEN is empty if so exit with error
if [ -z "$VAULT_TOKEN" ]; then
  echo "VAULT_TOKEN is empty - could not get token from vault"
  exit 1
fi

# Retrieve hosts inventory
FULL_SECRET_PATH="kv2/$VAULT_PATH/hosts"
./vault kv get -field data --format=yaml -address="$VAULT_SERVER_URL" "$FULL_SECRET_PATH" > "ansible/inventory/hosts.yml"

# Retrieve all group_vars
FULL_SECRET_PATH="kv2/$VAULT_PATH/$DEPLOY_ENVIRONMENT/group_vars"
CONFIGS=$(./vault kv list -address="$VAULT_SERVER_URL" "$FULL_SECRET_PATH" | tail -n +3)
for config in $CONFIGS; do
    ./vault kv get -field data --format=yaml -address="$VAULT_SERVER_URL" "$FULL_SECRET_PATH/${config}" > "ansible/inventory/group_vars/${config}.yml"
done

# Retrieve all host_vars
FULL_SECRET_PATH="kv2/$VAULT_PATH/$DEPLOY_ENVIRONMENT/host_vars"
CONFIGS=$(./vault kv list -address="$VAULT_SERVER_URL" "$FULL_SECRET_PATH" | tail -n +3)
for config in $CONFIGS; do
    ./vault kv get -field data --format=yaml -address="$VAULT_SERVER_URL" "$FULL_SECRET_PATH/${config}" > "ansible/inventory/host_vars/${config}.yml"
done
