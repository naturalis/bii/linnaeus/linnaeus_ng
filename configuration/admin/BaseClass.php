<?php

namespace Linnaeus\Admin;

if (file_exists(__DIR__ . "/constants.php")) {
    include_once __DIR__ . "/constants.php";
}

use Linnaeus\Admin\Configuration;

class BaseClass
{
    public $config;
    public $customConfig;
    public $generalSettings;

    public function __construct()
    {

        $this->loadConfiguration();

        $this->setGeneralSettings();
    }


    public function __destruct()
    {
    }


    private function loadConfiguration()
    {

        $this->config = new Configuration();
    }

    private function setGeneralSettings()
    {

        $this->generalSettings = $this->config->getGeneralSettings();
    }
}
