<?php

namespace Linnaeus\Admin;

set_time_limit(600);

if (getenv('DEBUG')) {
    error_reporting(E_ALL);
} else {
    error_reporting(E_ALL & ~(E_NOTICE | E_STRICT));
}

date_default_timezone_set('Europe/Amsterdam');

if (getenv('MEMORYLIMIT')) {
    ini_set('memory_limit', getenv('MEMORYLIMIT') ? getenv('MEMORYLIMIT') : '512M');
}
require_once 'constants.php';
require_once 'projects.php';

if (!defined('FIXED_PROJECT_ID')) {
    define('FIXED_PROJECT_ID', getenv('FIXED_PROJECT_ID') ? getenv('FIXED_PROJECT_ID') : null);
}

class Configuration
{
    private $appFileRoot;

    public function __construct()
    {
        $this->appFileRoot = getenv('WEBROOT') ? getenv('WEBROOT') : '/var/www/';
    }

    public function getDatabaseSettings()
    {
        $settings = [];
        $settings['tablePrefix'] = getenv('TABLE_PREFIX') ? getenv('TABLE_PREFIX') : '';
        $settings['host'] = getenv('MYSQL_HOST') ? getenv('MYSQL_HOST') : 'database';
        $settings['user'] = getenv('MYSQL_USER') ? getenv('MYSQL_USER') : 'develop';
        $settings['password'] = getenv('MYSQL_PASSWORD') ? getenv('MYSQL_PASSWORD') : 'develop';
        $settings['database'] = getenv('MYSQL_DATABASE') ? getenv('MYSQL_DATABASE') : 'develop';
        $settings['characterSet'] = 'utf8';

        return $settings;
    }

    /*
     * Assign (sub)domain names to project ids; required to generate sitemaps!
     * Format is [project id => domain name]
     */
    public function getProjectDomains()
    {
        global $projectDomains;

        return $projectDomains;
    }

    public function getSmartySettings()
    {
        $settings = [];
        $settings['dir_template'] = $this->appFileRoot . 'public/admin/templates/';
        $settings['dir_config'] = $this->appFileRoot . 'public/admin/templates/configs';
        $settings['dir_compile'] = getenv('SMARTYCOMPILE') ? getenv('SMARTYCOMPILE') : '/cache/admin/smarty/templates_c';
        $settings['dir_cache'] = getenv('SMARTYCACHE') ? getenv('SMARTYCACHE') : '/cache/admin/smarty/cache';
        $settings['caching'] = 1;
        $settings['compile_check'] = true;

        return $settings;
    }

    public function getGeneralSettings()
    {
        return array(
            'app' => array(
                'applicationFileRoot' => $this->appFileRoot,
                'name' => getenv('NAME') ? getenv('NAME') : 'Linnaeus NG Administration',
                'version' => getenv('VERSION') ? getenv('VERSION') : 'unknown',
                'versionTimestamp' => date('r'),
                'pathName' => 'admin',
            ),
            'serverTimeZone' => 'Europe/Amsterdam',
            'paths' => array(
                'login' => '/views/users/login.php',
                'logout' => '/views/users/logout.php',
                'chooseProject' => '/views/users/choose_project.php',
                'projectIndex' => '/views/utilities/admin_index.php',
                'notAuthorized' => '/views/utilities/not_authorized.php',
                'moduleNotPresent' => '/views/utilities/module_not_present.php',
                'mediaBasePath' => '../../../shared/media/project',
            ),
            'directories' => array(
                'mediaDirProject' => $this->appFileRoot . 'public/shared/media/project',
                'log' => $this->appFileRoot . 'log',
                'runtimeStyleRoot' => $this->appFileRoot . 'public/app/style',
            ),
            'login-cookie' => array(
                'name' => 'linnaeus-login',
                'lifetime' => 30, // days
            ),
            //'uiLanguages' => array(LANGUAGECODE_ENGLISH,LANGUAGECODE_DUTCH),
            'uiLanguages' => array(LANGUAGECODE_ENGLISH),
            'storeNewTranslateStrings' => getenv('STORE_NEW_TRANSLATE_STRINGS') ?: false,
            'soundPlayerPath' => '../../media/system/',
            'soundPlayerName' => 'player_mp3.swf',
            'useJavascriptLinks' => false,
            'projectCssTemplateFile' => 'project-template.css',
            'appNameFrontEnd' => 'app',
            'pushUrl' => 'http://linnaeus.naturalis.nl/admin/server_csv.php'
        );
    }
}
