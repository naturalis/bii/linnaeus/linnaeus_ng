<?php

namespace Linnaeus\Admin;

class Database
{
    private static $instance = array();

    public static function createInstance($id, array $config)
    {
        self::$instance[$id] = mysqli_connect(
            $config['host'],
            $config['user'],
            $config['password'],
            $config['database']
        );

        if (mysqli_connect_error()) {
            return false;
        }

        if (isset($config['characterSet'])) {
            mysqli_query(
                self::$instance[$id],
                'SET NAMES ' . $config['characterSet']
            );
            mysqli_query(
                self::$instance[$id],
                'SET CHARACTER SET ' . $config['characterSet']
            );
        }

        // Problems with strict mode in Traits mode; force enable "loose" mode
        mysqli_query(
            self::$instance[$id],
            'SET SESSION sql_mode = "NO_ENGINE_SUBSTITUTION"'
        );

        return self::$instance[$id];
    }

    public static function getInstance($id, $settings)
    {
        if (isset(self::$instance[$id])) {
            if (property_exists(self::$instance[$id], 'client_info')) {
                return self::$instance[$id];
            }
        }
        return self::createInstance($id, $settings);
    }

    public static function deleteInstance($id)
    {
        if (isset(self::$instance[$id])) {
            unset(self::$instance[$id]);
        }
    }

    private function __clone()
    {
    }
}
