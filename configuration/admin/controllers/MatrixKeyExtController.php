<?php

namespace Linnaeus\Admin\Controllers;

use Linnaeus\Admin\Controllers\Controller;
use Linnaeus\Admin\Controllers\MediaController;
use Linnaeus\Admin\Controllers\ModuleSettingsReaderController;

class MatrixKeyExtController extends Controller
{
    public $usedModels = [
        'matrices',
        'matrices_names',
        'matrices_taxa',
        'matrices_taxa_states',
        'characteristics',
        'characteristics_matrices',
        'characteristics_labels',
        'characteristics_states',
        'characteristics_labels_states',
        'chargroups_labels',
        'chargroups',
        'characteristics_chargroups',
        'matrices_variations',
        'gui_menu_order'
    ];

    public $jsToLoad = [
        'all' => [
            'matrix_ext.js',
        ]
    ];

    public $modelNameOverride = 'MatrixKeyExtModel';

    private $characterTypes = [
        ['label' => 'text', 'description' => 'textual descriptions'],
        ['label' => 'media', 'description' => 'images, videos or soundfiles'],
        ['label' => 'range', 'description' => 'value ranges, defined by a lowest and a highest value'],
        ['label' => 'distribution', 'description' => 'value distributions, defined by a mean and values for one and two standard deviations']
    ];

    public function __construct()
    {
        parent::__construct();
        $this->initialize();
    }

    public function overviewAction()
    {
        $this->checkAuthorisation();
        $this->setPageName($this->translate('Overview'));
        $this->smarty->assign('matrices', $this->getMatrices());
        $this->printPage('ext_overview');
    }

    public function matrixAction()
    {
        $this->checkAuthorisation();
        if (!$this->rHasId()) {
            $this->redirect("ext_overview.php");
        }
        $this->setCurrentMatrixId($this->rGetId());
        $this->matrixActionFormActions();
        $this->setPageName($this->translate('Edit matrix'));
        $this->smarty->assign('matrix', $this->getMatrix());
        $this->smarty->assign('groupsCharactersStates', $this->getGroupsCharactersStates());
        $this->smarty->assign('languages', $this->getProjectLanguages());
        $this->smarty->assign('characterTypes', $this->characterTypes);

        $this->printPage('ext_matrix');
    }


    public function ajaxInterfaceAction()
    {
        $this->UserRights->setActionType($this->UserRights->getActionRead());

        if (!$this->rHasVal('action')) {
            $return = ACTION_REQUIRED;
        } elseif ($this->rGetVal('action') == 'get_group') {
            $return = $this->getAuthorisationState() && !is_null($this->rGetVal('id')) ?
                json_encode($this->getGroup($this->rGetId())) : LOGIN_REQUIRED;
        } elseif ($this->rGetVal('action') == 'get_taxa_for_state') {
            $return = $this->getAuthorisationState() && !is_null($this->rGetVal('id')) ?
                json_encode($this->getTaxaForState($this->rGetId())) : LOGIN_REQUIRED;
        }

        $this->smarty->assign('returnText', $return);
        $this->printPage('ext_ajax_interface');
    }

    private function initialize()
    {
        array_walk($this->characterTypes, function (&$a) {
            $a['description'] = $this->translate($a['description']);
        });
    }

    private function setCurrentMatrixId($id)
    {
        $this->moduleSession->setModuleSetting(['setting' => 'currentMatrixId', 'value' => $id]);
    }

    private function getCurrentMatrixId()
    {
        return $this->moduleSession->getModuleSetting('currentMatrixId');
    }

    private function getMatrices()
    {
        return $this->models->MatrixKeyExtModel->getMatrices([
            "project_id" => $this->getCurrentProjectId(),
            "default_project_language" => $this->getDefaultProjectLanguage()
        ]);
    }

    private function getMatrix()
    {
        return $this->models->MatrixKeyExtModel->getMatrix([
            "project_id" => $this->getCurrentProjectId(),
            "matrix_id" => $this->getCurrentMatrixId(),
            "default_project_language" => $this->getDefaultProjectLanguage()
        ]);
    }

    private function getGroups()
    {
        return
            $this->models->MatrixKeyExtModel->getGroups([
                'project_id' => $this->getCurrentProjectId(),
                'matrix_id' => $this->getCurrentMatrixId(),
                'default_project_language' => $this->getDefaultProjectLanguage()
            ]);
    }

    private function getGroup($id)
    {
        return
            $this->models->MatrixKeyExtModel->getGroup([
                'project_id' => $this->getCurrentProjectId(),
                'matrix_id' => $this->getCurrentMatrixId(),
                'group_id' => $id,
                'default_project_language' => $this->getDefaultProjectLanguage()
            ]);
    }

    private function getCharacters()
    {
        return
            $this->models->MatrixKeyExtModel->getCharacters([
                'project_id' => $this->getCurrentProjectId(),
                'matrix_id' => $this->getCurrentMatrixId(),
                'default_project_language' => $this->getDefaultProjectLanguage()
            ]);
    }

    private function getStates()
    {
        return
            $this->models->MatrixKeyExtModel->getStates([
                'project_id' => $this->getCurrentProjectId(),
                'default_project_language' => $this->getDefaultProjectLanguage()
            ]);
    }

    private function getGuiOrder()
    {
        return
            $this->models->MatrixKeyExtModel->getGuiOrder([
                'project_id' => $this->getCurrentProjectId(),
                'matrix_id' => $this->getCurrentMatrixId()
            ]);
    }

    private function getGroupsCharactersStates()
    {
        $groups = $this->getGroups();
        $characters = $this->getCharacters();
        $states = $this->getStates();
        $order = $this->getGuiOrder();

        // add states to their character
        foreach ((array)$characters as $key => $value) {
            $characters[$key]['states'] = [];
            foreach ($states as $sValue) {
                if ($sValue['characteristic_id'] == $value['id']) {
                    $characters[$key]['states'][] = $sValue;
                }
            }
        }

        // adding characters to groups
        foreach ((array)$groups as $key => $value) {
            if ($value['characters']) {
                foreach ($value['characters'] as $cKey => $cValue) {
                    $hKey = array_search($cValue['characteristic_id'], array_column($characters, 'id'));
                    if ($hKey !== false) {
                        $groups[$key]['characters'][$cKey] = $characters[$hKey];
                        array_splice($characters, $hKey, 1);
                    }
                }
            }
        }

        // re-ordening groups and ungrouped characters
        foreach ((array)$order as $key => $value) {
            if ($value['ref_type'] == 'char') {
                $hKey = array_search($value['ref_id'], array_column($characters, 'id'));

                if ($hKey !== false) {
                    $order[$key]['item'] = $characters[$hKey];
                    array_splice($characters, $hKey, 1);
                }
            } elseif ($value['ref_type'] == 'group') {
                $hKey = array_search($value['ref_id'], array_column($groups, 'id'));
                if ($hKey !== false) {
                    $order[$key]['item'] = $groups[$hKey];
                    array_splice($groups, $hKey, 1);
                }
            }
        }


        // see if there's anything left (which was not in the order table)
        $i = count($order);
        foreach ((array)$groups as $key => $value) {
            $order[$i]['item'] = $value;
            $order[$i++]['ref_type'] = 'group';
        }

        foreach ((array)$characters as $key => $value) {
            $order[$i]['item'] = $value;
            $order[$i++]['ref_type'] = 'char';
        }

        // eliminating left-over empty items in the order list
        foreach ((array)$order as $key => $value) {
            if (!isset($value['item'])) {
                unset($order[$key]);
            }
        }

        return $order;
    }

    private function getTaxaForState($id)
    {
        return
            $this->models->MatrixKeyExtModel->getTaxaForState([
                'project_id' => $this->getCurrentProjectId(),
                'matrix_id' => $this->getCurrentMatrixId(),
                'state_id' => $id,
                'language_id' => $this->getDefaultProjectLanguage(),
                'name_type_id' => $this->getNameTypeId(PREDICATE_PREFERRED_NAME)
            ]);
    }

    private function matrixActionFormActions()
    {
        if ($this->rHasVal('action', 'saveSortOrder')) {
            $this->saveSortOrders($this->rGetVal('newOrder'));
            $this->addMessage($this->translate("saved new order"));
        } elseif ($this->rHasVal('action', 'saveGroup')) {
            if ($this->saveGroup($this->rGetAll())) {
                $this->addMessage($this->translate("saved group"));
            } else {
                $this->addError($this->translate("could not save group"));
            }
        } elseif ($this->rHasVal('action', 'deleteGroup')) {
            if (!$this->rHasVal('groupId')) {
                return;
            }
            if ($this->deleteGroup($this->rGetVal('groupId'))) {
                $this->addMessage($this->translate("deleted group"));
            }
        }
    }


    private function saveSortOrders($order)
    {

        // saving order of groups and characters
        $this->models->GuiMenuOrder->delete([
            "project_id" => $this->getCurrentProjectId(),
            "matrix_id" => $this->getCurrentMatrixId()
        ]);

        $i = 1;
        foreach ((array)$order as $val) {
            $val = json_decode($val, true);

            if ($val['type'] == 'group' || $val['type'] == 'character') {
                $this->models->GuiMenuOrder->save([
                    "project_id" => $this->getCurrentProjectId(),
                    "matrix_id" => $this->getCurrentMatrixId(),
                    "ref_id" => $val['id'],
                    "ref_type" => ($val['type'] == 'character' ? 'char' : 'group'),
                    "show_order" => $i++
                ]);
            }
        }

        // saving order of states
        $i = 1;
        foreach ((array)$order as $val) {
            $val = json_decode($val, true);

            if ($val['type'] == 'state') {
                $this->models->CharacteristicsStates->update(
                    [
                    "show_order" => $i++
                    ],
                    [
                        "project_id" => $this->getCurrentProjectId(),
                        "id" => $val['id']
                    ]
                );
            }
        }
    }

    private function saveGroup($data)
    {
        if (isset($data['groupId'])) {
            $groupId = $data['groupId'];
        } else {
            $groupId = null;
        }

        $names = [];
        foreach ($data['newNames'] as $key => $val) {
            $val = json_decode($val, true);
            if ($val['id'] == 'sys_name' && !empty($val['value'])) {
                $this->models->Chargroups->save([
                    "id" => $groupId,
                    "project_id" => $this->getCurrentProjectId(),
                    "matrix_id" => $this->getCurrentMatrixId(),
                    "label" => $val['value'],
                    "show_order" => 99
                ]);

                $groupId = !is_null($groupId) ? $groupId : $this->models->Chargroups->getNewId();
            } elseif (!empty($val['value'])) {
                $names[$val['id']] = $val['value'];
            }
        }

        if (isset($groupId)) {
            $this->models->ChargroupsLabels->delete([
                "project_id" => $this->getCurrentProjectId(),
                "chargroup_id" => $groupId
            ]);

            foreach ($names as $key => $value) {
                $this->models->ChargroupsLabels->save([
                    "project_id" => $this->getCurrentProjectId(),
                    "chargroup_id" => $groupId,
                    "language_id" => $key,
                    "label" => $value,
                ]);
            }
            return true;
        } else {
            return true;
        }
    }

    private function deleteGroup($id)
    {
        // remove from GUI
        $this->models->GuiMenuOrder->delete([
            "project_id" => $this->getCurrentProjectId(),
            "matrix_id" => $this->getCurrentMatrixId(),
            "ref_id" => $id,
            "ref_type" => 'group'
        ]);

        // orphan characters
        $this->models->CharacteristicsChargroups->delete([
            "project_id" => $this->getCurrentProjectId(),
            "chargroup_id" => $id
        ]);

        // delete labels
        $this->models->ChargroupsLabels->delete([
            "project_id" => $this->getCurrentProjectId(),
            "chargroup_id" => $id
        ]);

        // delete group
        $this->models->Chargroups->delete([
            "project_id" => $this->getCurrentProjectId(),
            "matrix_id" => $this->getCurrentMatrixId(),
            "id" => $id
        ]);

        return true;
    }
}
