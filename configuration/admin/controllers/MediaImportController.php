<?php

namespace Linnaeus\Admin\Controllers;

use Linnaeus\Admin\Controllers\MediaController;
use Linnaeus\Admin\Helpers\HrFilesizeHelper;
use Exception;

/**
 * Import media via csv file
 */

class MediaImportController extends MediaController
{
    private $projectModules;
    private $totals = array();
    private $media = array();

    private $currentModule;
    private $currentModuleId;
    private $currentItemId;
    private $currentMediaId;
    private $originalMediaId;
    private $currentFileName;
    private $originalFileName;
    private $overview;
    private $caption;
    private $rsFile;
    private $sortOrder;
    private $lastItem;

    private $maxFileSize;
    private $filePath;
    private $error;
    private $cli = false;
    private $br = '<br>';

    private $csvData = [];
    private $missingFiles = [];
    private $missingTaxa = [];

    public $usedModels = array(
        'media',
        'media_metadata',
        'media_modules',
        'media_captions',
        'media_tags',
        'media_conversion_log',
        'media_descriptions_taxon',
        'glossary_media_captions',
        'modules',
        'modules_projects'
    );

    public $usedHelpers = array('hr_filesize_helper');


    /**
     * Constructor, calls parent's constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        $this->initialize();
    }

    /**
     * Destructor
     *
     * @access public
     */
    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * Initializes Controller
     */
    private function initialize()
    {
        $this->setProjectModules();
        $this->setTaxonEditorModule();
        $this->setPaths();
        $this->setMaxFileSize();
    }

    public function indexAction()
    {
        $this->UserRights->setActionType($this->UserRights->getActionCreate());
        $this->checkAuthorisation();

        $this->setPageName($this->translate('Csv file upload'));

        if ($this->requestDataFiles) {
            $this->parseCsv($this->requestDataFiles[0]["tmp_name"]);
            $this->setParsedData();
            $this->setParsedFileName();
            $this->smarty->assign('file_name', $this->requestDataFiles[0]["name"]);

            $this->getMissingTaxa();
            if ($this->missingTaxa) {
                $this->smarty->assign('missing_taxa', $this->missingTaxa);
            }

            $this->getMissingFiles();
            if ($this->missingFiles) {
                $this->smarty->assign('missing_files', $this->missingFiles);
            }
        }

        $this->printPage();
    }

    public function initAction()
    {
        $this->UserRights->setActionType($this->UserRights->getActionCreate());
        $this->checkAuthorisation();

        $this->setPageName($this->translate('Import media'));
        $this->csvData = $this->getParsedData();

        $this->smarty->assign('csv_parsed', !empty($this->getParsedData()));
        $this->smarty->assign('csv_file', $this->getParsedFileName());
        $this->smarty->assign('totals', $this->setImportMedia());
        $this->smarty->assign('file_path', $this->filePath);
        $this->printPage();
    }

    /**
     * Get missing files
     *
     * @throws Exception
     */
    public function getMissingFiles()
    {
        if (empty($this->csvData)) {
            throw new Exception('Csv file not parsed yet');
        }
        foreach ($this->csvData as $i => $row) {
            if (!file_exists($this->filePath . $row['file_name'])) {
                $notFound[$i] = $row['file_name'];
                unset($this->_images[$i]);
            }
        }
        if (isset($notFound)) {
            $this->missingFiles = array_unique($notFound);
            sort($this->missingFiles);
        }
    }

    /**
     * Get missing taxa
     *
     * @throws Exception
     */
    public function getMissingTaxa()
    {
        if (empty($this->csvData)) {
            throw new Exception('Csv file not parsed yet');
        }
        $taxa = array_unique(array_column($this->getParsedData(), 'taxon'));
        sort($taxa);
        foreach ($taxa as $taxon) {
            if (!$this->getCurrentItemId($taxon)) {
                $this->missingTaxa[] = $taxon;
            }
        }
    }

    /**
     * Parse CSV
     *
     * @throws Exception
     */
    public function parseCsv($csvPath = false)
    {
        ini_set("auto_detect_line_endings", true);
        if (empty($csvPath)) {
            throw new Exception('Path to csv file is not set');
        }

        $fh = fopen($csvPath, "r");
        if ($fh === false) {
            throw new Exception('Cannot read csv file');
        }
        $firstLine = true;
        while ($row = fgetcsv($fh)) {
            if ($firstLine) {
                $legend = $row;
                $firstLine = false;
            } else {
                foreach ($row as $k => $v) {
                    $data[$legend[$k]] = $v;
                }
                $this->csvData[] = $data;
            }
        }
    }


    /**
     * Shows the progress of the import
     *
     * @param bool $cli
     */
    public function printImportProgressAction($cli = false)
    {
        if ($cli) {
            $this->setCli();
        }

        foreach ($this->getParsedData() as $row) {
            $this->resetProperties();
            $this->setCurrentItem($row);

            // Taxon or image do not exist
            if ($this->error) {
                echo 'ERROR: ' . $this->error . $this->br;
                continue;
            }

            // Check if file has already been converted
            if ($this->fileHasBeenConverted()) {
                // Item has already been attached to specific item in a previous conversion
                if ($this->itemHasBeenAttached()) {
                    echo "Already attached: $this->currentFileName $this->br";
                } else {
                    // File has been converted but not yet attached to item
                    $this->attachFile();
                    echo "Attached: $this->currentFileName $this->br";
                }

                // Upload file
            } else {
                $this->uploadFile();

                // Oops
                if (!empty($this->error)) {
                    echo 'ERROR: ' . $this->error . $this->br;

                    // Success!
                } else {
                    echo "Uploaded: $this->currentFileName $this->br";
                }
            }
        }
    }

    private function setMaxFileSize()
    {
        $h = new HrFilesizeHelper();

        $upload = $h->returnBytes(ini_get('upload_max_filesize'));
        $post = $h->returnBytes(ini_get('post_max_size'));

        $this->maxFileSize = $upload <= $post ? $upload : $post;
    }

    private function setPaths()
    {
        $this->filePath = $_SESSION['admin']['project']['urls']['project_media'];
    }

    private function setCli()
    {
        $this->cli = true;
        $this->br = "\n";
    }

    private function setImportMedia()
    {
        // Determine if process was interrupted
        $this->totals['converted'] = $this->models->MediaModel->getConvertedMediaCount(
            array(
                'project_id' => $this->getCurrentProjectId()
            )
        );

        // Taxa
        $moduleId = $this->getModuleId('nsr');
        if ($moduleId) {
            $media = $this->models->MediaModel->getConverterTaxonMedia(
                array(
                    'project_id' => $this->getCurrentProjectId()
                )
            );
            $this->media['modules']['Taxon editor'] = array(
                'id' => $moduleId,
                'media' => $media
            );
            $this->totals['modules']['Taxon editor'] = count($media);
            $this->setLastItem($media, $moduleId);
        }

        $this->totals['total'] = count($this->getParsedData());

        return $this->totals;
    }

    private function setLastItem($media, $moduleId)
    {
        if (!empty($media)) {
            $this->lastItem = end($media);
            $this->lastItem['module_id'] = $moduleId;
        }
    }

    private function setProjectModules()
    {
        $d = $this->getProjectModules();

        // Set simplified array
        foreach ($d['modules'] as $m) {
            $this->projectModules['modules'][$m['module_id']] = $m['controller'];
        }
        if (isset($d['freeModules'])) {
            foreach ($d['freeModules'] as $m) {
                $this->projectModules['freeModules'][$m['id']] = $m['module'];
            }
        }
    }

    private function setTaxonEditorModule()
    {
        if ($this->getModuleId('species') && !$this->getModuleId('nsr')) {
            $d = $this->models->Modules->getColumn(
                array(
                    'columns' => 'id',
                    'id' => array(
                        'project_id' => $this->getCurrentProjectId(),
                        'controller' => 'nsr'
                    )
                )
            );

            $this->models->ModulesProjects->insert(
                array(
                    'module_id' => $d[0]['id'],
                    'project_id' => $this->getCurrentProjectId(),
                    'show_order' => 0,
                    'active' => 'y'
                )
            );

            $this->setProjectModules();
        }

        $this->currentModule = "Taxon Editor";
        $this->currentModuleId = $this->getModuleId('nsr');
    }

    private function getModuleId($m)
    {
        return array_search($m, $this->projectModules['modules']);
    }

    private function setCurrentItem($row)
    {
        $row = array_map('trim', $row);

        $this->currentFileName = $row['file_name'];
        $this->originalFileName = $row['new_file_name'];
        $this->currentItemId = $this->getCurrentItemId($row['taxon']);
        $this->overview = strtolower($row['overview']) == 'yes' ? 1 : 0;
        $this->sortOrder = $this->overview == 0 ? 0 : 99;
        $this->caption = $row['caption'];

        // Basic validation
        if (!$this->currentItemId) {
            $this->error = 'taxon ' . $row['taxon'] . ' does not exist';
        }
        if (!file_exists($this->filePath . $this->currentFileName)) {
            $this->error = "image $this->currentFileName does not exist";
        }
    }

    private function getCurrentItemId($name)
    {
        $taxon = $this->getTaxonByName($name);
        return $taxon ? $taxon['id'] : false;
    }

    private function resetProperties()
    {
        $this->currentMediaId = false;
        $this->files = false;
        $this->result = false;
        $this->originalMediaId = false;
        $this->mediaId = false;
        $this->rsFile = false;
        $this->originalFileName = false;
        $this->error = false;
        $this->overview = $this->sortOrder = 0;
    }

    private function fileHasBeenConverted()
    {
        $d = $this->models->MediaModel->getMediaId(
            array(
                'project_id' => $this->getCurrentProjectId(),
                'old_file' => $this->currentFileName
            )
        );

        if (!empty($d)) {
            $this->currentMediaId = $d['media_id'];
            $this->rsFile = $d['new_file'];
        }

        return !empty($d);
    }

    private function itemHasBeenAttached()
    {
        $d = $this->models->MediaModel->getMediaConversionId(
            array(
                'project_id' => $this->getCurrentProjectId(),
                'module' => $this->currentModule,
                'item_id' => $this->currentItemId,
                'media_id' => $this->currentMediaId
            )
        );

        return !empty($d);
    }

    private function attachFile()
    {
        if ($this->currentMediaId && $this->currentItemId) {
            $this->models->MediaModules->insert(
                array(
                    'module_id' => $this->currentModuleId,
                    'media_id' => $this->currentMediaId,
                    'project_id' => $this->getCurrentProjectId(),
                    'item_id' => $this->currentItemId,
                    'overview_image' => $this->overview,
                    'sort_order' => $this->sortOrder
                )
            );
        }
        $this->setCaption();
        $this->logAction();
    }

    private function uploadFile()
    {
        $this->setFiles();

        if (empty($this->error)) {
            $this->setItemId($this->currentItemId);
            $this->setModuleId($this->currentModuleId);

            $this->uploadFiles(
                array(
                    'overview' => $this->overview,
                    'sort_order' => $this->sortOrder
                )
            );

            $this->setCurrentMediaId();

            // Upload errors are stored in $this->errors
            if (!empty($this->errors)) {
                $this->error = implode('; ', $this->errors);
                $this->errors = array();
            } else {
                $this->setCaption();
            }
        }

        $this->logAction();
    }

    private function setCurrentMediaId()
    {
        $this->currentMediaId = $this->mediaId;
    }

    public function setCaption()
    {
        if (!$this->currentMediaId) {
            return false;
        }

        $this->setItemId($this->currentItemId);
        $this->setModuleId($this->currentModuleId);

        if ($this->caption) {
            $this->saveCaption(
                array(
                    'caption' => $this->caption,
                    'media_id' => $this->currentMediaId,
                    'language_id' => $this->getDefaultProjectLanguage(),
                )
            );
        }
    }

    private function logAction()
    {
        // File has been uploaded to RS
        if (!empty($this->result) && empty($this->result->error)) {
            $mediaId = $this->mediaId;
            $newFile = $this->result->resource->files[0]->src;

            // File has been attached
        } elseif (!empty($this->currentMediaId) && !empty($this->rsFile)) {
            $mediaId = $this->currentMediaId;
            $newFile = $this->rsFile;

            // Something went horribly wrong
        } else {
            $mediaId = -1;
            $newFile = 'failed';
        }

        if (isset($mediaId)) {
            $this->models->MediaConversionLog->insert(
                array(
                    'project_id' => $this->getCurrentProjectId(),
                    'module' => $this->currentModule,
                    'media_id' => $mediaId,
                    'item_id' => $this->currentItemId,
                    'old_file' => $this->currentFileName,
                    'new_file' => $newFile,
                    'error' => $this->error
                )
            );
        }
    }

    private function setFiles()
    {
        $file = $this->filePath . $this->currentFileName;
        $type = $tmp_name = $size = null;

        // It's not there!
        if (!file_exists($file)) {
            $error = 4;
            $this->error = "Local file $file cannot be found";

            // It's too large!
        } elseif (filesize($file) > $this->maxFileSize) {
            $error = 1;
            $this->error = "Size of $file exceeds maximum file size";

            // It's OK!
        } else {
            $type = mime_content_type($file);
            $tmp_name = $file;
            $size = filesize($file);
            $error = 0;
        }

        $this->files = array(
            array(
                'name' => $this->currentFileName,
                'type' => $type,
                'tmp_name' => $tmp_name,
                'error' => $error,
                'size' => $size,
                'title' => !empty($this->originalFileName) ? $this->originalFileName : ''
            )
        );
    }

    private function setParsedData()
    {
        $_SESSION['admin']['system']['csv_data'] = $this->csvData;
    }

    private function getParsedData()
    {
        return isset($_SESSION['admin']['system']['csv_data']) ?
            $_SESSION['admin']['system']['csv_data'] : [];
    }

    private function setParsedFileName()
    {
        $_SESSION['admin']['system']['csv_file'] = $this->requestDataFiles[0]["name"];
    }

    private function getParsedFileName()
    {
        return isset($_SESSION['admin']['system']['csv_file']) ?
            $_SESSION['admin']['system']['csv_file'] : false;
    }
}
