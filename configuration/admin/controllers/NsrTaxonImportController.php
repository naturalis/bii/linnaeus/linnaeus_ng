<?php

namespace Linnaeus\Admin\Controllers;

use Linnaeus\Admin\Controllers\Controller;
use Linnaeus\Admin\Controllers\ModuleSettingsReaderController;
use stdClass;

class NsrTaxonImportController extends NsrController
{
    public $modelNameOverride = 'NsrTaxonModel';
    public $controllerPublicName = 'Taxon editor';

    public $usedModels = array(
        'tab_order',
        'pages_taxa',
        'pages_taxa_titles',
        'labels_languages',
        'content_taxa',
        'nsr_ids',
        'names',
        'presence_taxa'
    );

    public $usedHelpers = array(
        'csv_parser_helper',
        'file_upload_helper',
    );

    private $importColumns = [
        'conceptName' => 0,
        'authorship' => 1,
        'rank' => 2,
        'parent' => 3,
        'commonName' => 4,
        'status' => 5,
    ];
    private $importRows = ['topicNames' => 0, 'languages' => 1];
    private $doNotImport = [];
    private $doNewLineToBr = true;
    private $nameTypes = [];

    public function __construct()
    {
        parent::__construct();
        $this->lines = new stdClass();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function importTaxonFileAction()
    {
        $this->UserRights->setActionType($this->UserRights->getActionCreate());
        $this->checkAuthorisation();

        $this->setPageName($this->translate('Taxon file upload'));

        if ($this->requestDataFiles) {
            unset($_SESSION['admin']['system']['csv_data']);

            $this->setCsvDelimiter();
            $this->helpers->CsvParserHelper->parseFile($this->requestDataFiles[0]["tmp_name"]);

            $this->addError($this->helpers->CsvParserHelper->getErrors());

            if (!$this->getErrors()) {
                $this->setLines($this->helpers->CsvParserHelper->getResults());
                if ($this->checkColumns()) {
                    $this->cleanLines();
                    $this->addLineId();
                    $this->checkRanks();
                    $this->checkParents();
                    $this->checkConcepts();
                    $this->checkNames();
                    $this->checkStatuses();
                    $this->setSessionLines();

                    $this->smarty->assign('lines', $this->getLines());
                    $this->smarty->assign('importColumns', $this->importColumns);
                }
            }
        }

        $this->smarty->assign('projectRanks', $this->getProjectRanks());
        $this->smarty->assign('statuses', $this->getStatuses());
        $this->printPage();
    }

    public function importStatusFileAction()
    {
        $this->UserRights->setActionType($this->UserRights->getActionCreate());
        $this->checkAuthorisation();

        $this->setPageName($this->translate('Presence status file upload'));
        $this->setStatusImportColumns();

        if ($this->requestDataFiles) {
            unset($_SESSION['admin']['system']['csv_data']);

            $this->setCsvDelimiter();
            $this->helpers->CsvParserHelper->parseFile($this->requestDataFiles[0]["tmp_name"]);

            $this->addError($this->helpers->CsvParserHelper->getErrors());

            if (!$this->getErrors()) {
                $this->setLines($this->helpers->CsvParserHelper->getResults());
                $this->cleanLines();
                $this->addLineId();
                $this->checkValidTaxa();
                $this->checkStatuses();
                $this->setSessionLines();

                $this->smarty->assign('lines', $this->getLines());
                $this->smarty->assign('importColumns', $this->importColumns);
            }
        }

        $this->smarty->assign('projectRanks', $this->getProjectRanks());
        $this->smarty->assign('statuses', $this->getStatuses());
        $this->printPage();
    }

    public function importSynonymFileAction()
    {
        $this->UserRights->setActionType($this->UserRights->getActionCreate());
        $this->checkAuthorisation();

        $this->setPageName($this->translate('Synonym file upload'));
        $this->setSynonymImportColumns();

        if ($this->requestDataFiles) {
            unset($_SESSION['admin']['system']['csv_data']);

            $this->setCsvDelimiter();
            $this->helpers->CsvParserHelper->parseFile($this->requestDataFiles[0]["tmp_name"]);

            $this->addError($this->helpers->CsvParserHelper->getErrors());

            if (!$this->getErrors()) {
                $this->setLines($this->helpers->CsvParserHelper->getResults());

                $this->cleanLines();
                $this->addLineId();
                // First validate taxon and name type, so both are available for synonym check
                $this->checkValidTaxa();
                $this->checkNameTypes();
                // Again: first taxon and name type!
                $this->checkSynonyms();
                $this->checkRanks();
                $this->setSessionLines();

                $this->smarty->assign('lines', $this->getLines());
                $this->smarty->assign('importColumns', $this->importColumns);
            }
        }

        $this->smarty->assign('nameTypes', $this->synonymNameTypes());
        $this->printPage();
    }


    public function importFileResetAction($redirect = 'import_taxon_file.php')
    {
        $this->resetImport();
        $this->redirect($redirect);
    }

    public function importTaxonFileProcessAction()
    {
        $this->UserRights->setActionType($this->UserRights->getActionCreate());
        $this->checkAuthorisation();

        $this->setPageName($this->translate('Processing taxon file upload'));

        $lines = $this->getSessionLines();
        $this->setLines($lines);

        if ($this->rHasVar('action', 'save')) {
            $this->setDoNotImport($this->rGetVal('do_not_import'));
            $this->saveConcepts();
            $this->saveParents();
            $this->saveNames();
            $this->saveStatuses();
            $this->setSessionLines();

            $this->smarty->assign('lines', $this->getLines());
            $this->smarty->assign('importColumns', $this->importColumns);

            // Prevent resubmit of data
            $this->resetImport();
        }

        $this->printPage();
    }

    public function importStatusFileProcessAction()
    {
        $this->UserRights->setActionType($this->UserRights->getActionCreate());
        $this->checkAuthorisation();

        $this->setPageName($this->translate('Processing presence status file upload'));
        $this->setStatusImportColumns();

        $lines = $this->getSessionLines();
        $this->setLines($lines);

        if ($this->rHasVar('action', 'save')) {
            $this->setDoNotImport($this->rGetVal('do_not_import'));
            $this->saveStatuses();
            $this->setSessionLines();

            $this->smarty->assign('lines', $this->getLines());
            $this->smarty->assign('importColumns', $this->importColumns);

            $this->resetImport();
        }

        $this->printPage();
    }


    public function importSynonymFileProcessAction()
    {
        $this->UserRights->setActionType($this->UserRights->getActionCreate());
        $this->checkAuthorisation();

        $this->setPageName($this->translate('Processing synonym file upload'));
        $this->setSynonymImportColumns();

        $lines = $this->getSessionLines();
        $this->setLines($lines);

        if ($this->rHasVar('action', 'save')) {
            $this->setDoNotImport($this->rGetVal('do_not_import'));
            $this->saveSynonyms();
            $this->setSessionLines();

            $this->smarty->assign('lines', $this->getLines());
            $this->smarty->assign('importColumns', $this->importColumns);

            $this->resetImport();
        }

        $this->printPage();
    }


    public function importPassportFileAction()
    {
        $this->UserRights->setActionType($this->UserRights->getActionCreate());
        $this->checkAuthorisation();

        $this->setPageName($this->translate('Taxon passport content upload'));

        if ($this->requestDataFiles) {
            unset($_SESSION['admin']['system']['csv_data']);

            $this->setCsvDelimiter();
            $this->helpers->CsvParserHelper->setMaxLineLength(100000);
            $this->helpers->CsvParserHelper->parseFile($this->requestDataFiles[0]["tmp_name"]);

            $this->addError($this->helpers->CsvParserHelper->getErrors());

            if (!$this->getErrors()) {
                $this->setLines($this->helpers->CsvParserHelper->getResults());
                $this->cleanLines();
                $this->prepPassportData();
                $this->checkLanguages();
                $this->checkTopics();
                $this->setSessionLines();
                $this->smarty->assign('lines', $this->getLines());
            }
        }

        $this->smarty->assign('importColumns', $this->importColumns);
        $this->smarty->assign('importRows', $this->importRows);
        $this->smarty->assign('categories', $this->getCategories());
        $this->smarty->assign('languages', $this->getProjectLanguages());

        $this->printPage();
    }

    public function importPassportFileProcessAction()
    {
        $this->UserRights->setActionType($this->UserRights->getActionCreate());

        $this->checkAuthorisation();

        $this->setPageName($this->translate('Processing taxon passport file upload'));

        $lines = $this->getSessionLines();
        $this->setLines($lines);

        if ($this->rHasVar('action', 'save')) {
            $this->setHandleExisting($this->rGetVal('handle_existing'));
            $this->saveTopics();

            $this->smarty->assign('lines', $this->getLines());
        }

        $this->printPage();
    }

    public function importPassportFileExampleAction()
    {
        $this->checkAuthorisation();

        $taxa = $this->models->Taxa->getColumn([
            'id' => ['project_id' => $this->getCurrentProjectId()],
            'order' => 'rank_id desc, taxon asc',
            'limit' => 10,
            'columns' => 'taxon'
        ]);

        $this->smarty->assign('taxa', $taxa);
        $this->smarty->assign('categories', $this->getCategories());
        $this->smarty->assign('languages', $this->getProjectLanguages());

        header('Content-disposition:attachment;filename=example-taxon-passport-import-file.csv');
        header('Content-type:text/csv; charset=utf-8');

        $this->printPage();
    }


    private function saveTopics()
    {
        $lines = $this->getLines();
        $results = array();

        foreach ((array)$lines['data'] as $key1 => $line) {
            foreach ((array)$line['data'] as $key2 => $val) {
                $results[$key1]['cells'][$key2]['saved'] = true;

                if (isset($line['taxon']['error'])) {
                    $results[$key1]['cells'][$key2]['saved'] = false;
                    $results[$key1]['cells'][$key2]['errors'][] = $line['taxon']['error'];
                }

                if (isset($lines['languages'][$key2]['error'])) {
                    $results[$key1]['cells'][$key2]['saved'] = false;
                    $results[$key1]['cells'][$key2]['errors'][] = $lines['languages'][$key2]['error'];
                }

                if (isset($lines['topics'][$key2]['error'])) {
                    $results[$key1]['cells'][$key2]['saved'] = false;
                    $results[$key1]['cells'][$key2]['errors'][] = $lines['topics'][$key2]['error'];
                }


                if ($results[$key1]['cells'][$key2]['saved'] == false) {
                    continue;
                }

                $where = [
                    'project_id' => $this->getCurrentProjectId(),
                    'taxon_id' => $line['taxon']['id'],
                    'language_id' => $lines['languages'][$key2]['language_id'],
                    'page_id' => $lines['topics'][$key2]['page_id'],
                ];

                $d = $this->models->ContentTaxa->getColumn(['id' => $where]);
                $beforeData = array_merge($d, $where);

                if ($d) {
                    $handle_existing = $this->getHandleExisting();

                    if ($handle_existing == 'skip') {
                        $results[$key1]['cells'][$key2]['saved'] = false;
                        $results[$key1]['cells'][$key2]['errors'][] = $this->translate('skipped (data exists)');
                        continue;
                    }

                    $where = [
                        'id' => $d[0]['id'],
                        'project_id' => $this->getCurrentProjectId(),
                        'taxon_id' => $line['taxon']['id'],
                        'language_id' => $lines['languages'][$key2]['language_id'],
                        'page_id' => $lines['topics'][$key2]['page_id'],
                    ];

                    if (str_starts_with($val, '#')) {
                        $val = '&#35;' . ltrim($val, "#");
                    }
                    if ($this->doNewLineToBr) {
                        $val = nl2br($val);
                    }

                    if ($handle_existing == 'overwrite') {
                        $data = $val;
                    }
                    if ($handle_existing == 'append') {
                        $data = $d[0]['content'] . ($this->doNewLineToBr ? nl2br("\n") : "\n") . $val;
                    }
                    if ($handle_existing == 'prepend') {
                        $data = $val . ($this->doNewLineToBr ? nl2br("\n") : "\n") . $d[0]['content'];
                    }

                    $this->models->ContentTaxa->update(['content' => $data, 'publish' => 1], $where);
                    $this->logChange([
                        'note' => 'Merged/overwrote topic (taxon batch import)',
                        'before' => $beforeData,
                        'after' => array_merge($data, $where)
                    ]);
                } else {
                    $saveData = array_merge(['content' => $val, 'publish' => 1], $where);
                    $this->models->ContentTaxa->save($saveData);
                    $this->logChange([
                        'note' => 'Saved topic (taxon batch import)',
                        'after' => $saveData
                    ]);
                }
            }
        }

        $lines['results'] = $results;

        $this->setLines($lines);
    }

    private function setLines($lines)
    {
        $this->lines = $lines;
    }

    private function getLines()
    {
        return $this->lines;
    }

    private function setSessionLines()
    {
        $this->moduleSession->setModuleSetting(['setting' => 'lines', 'value' => $this->getLines()]);
    }

    private function getSessionLines()
    {
        return $this->moduleSession->getModuleSetting('lines');
    }

    private function cleanLines()
    {
        $lines = $this->getLines();
        array_walk_recursive($lines, function(&$value) {
            $value = trim($value);
        });
        $this->setLines($lines);
    }

    private function checkColumns()
    {
        $lines = $this->getLines();
        $maxwidth = 0;
        foreach ((array)$lines as $val) {
            if (count((array)$val) > $maxwidth) {
                $maxwidth = count((array)$val);
            }
        }

        if ($maxwidth < 2) {
            $this->addError($this->translate('Your file looks wrongly formatted: there are is only one column. Check the file and try again.'));
            return false;
        } elseif ($maxwidth < 5) {
            $this->addError($this->translate('There are less than five columns in your data file. Please make sure that the data is complete and the file formatting is correct before you save the data.'));
            return false;
        }
        return true;
    }

    private function addLineId()
    {
        $lines = $this->getLines();
        array_walk($lines, function (&$a) {
            $a['line_id'] = md5(++$this->tmp);
        });
        $this->setLines($lines);
    }

    private function conceptNameWellFormed($name)
    {
        return (strlen($name) > 0 && strlen($name) < 255);
    }

    private function checkConcepts()
    {
        $lines = $this->getLines();
        $projectRanks = $this->getProjectRanks();

        foreach ((array)$lines as $key => $val) {
            if (empty($val[$this->importColumns['conceptName']])) {
                $lines[$key]['errors'][] = ['message' => $this->translate('no concept name')];
                continue;
            }

            $name = $val[$this->importColumns['conceptName']];
            $nameKey = array_search($name, array_column($lines, $this->importColumns['conceptName']));

            if ($nameKey !== false && $nameKey !== $key) {
                $lines[$nameKey]['errors'][] = ['message' => $this->translate('duplicate name in list')];
                continue;
            }

            if (!$this->conceptNameWellFormed($name)) {
                $lines[$key]['errors'][] = ['message' => $this->translate('name not well formed')];
                continue;
            }

            // Check if name exists as a scientific name
            $validName = $this->getValidName($name);

            if (isset($validName[0]['taxon_id'])) {
                // Extra check on rank
                $rankKey = array_search($val[$this->importColumns['rank']], array_column($projectRanks, 'rank'));
                $d = $this->models->Taxa->getColumn([
                    'id' => [
                        'project_id' => $this->getCurrentProjectId(),
                        'id' => $validName[0]['taxon_id'],
                        'rank_id' => $projectRanks[$rankKey]['id'],
                    ]
                ]);

                if ($d) {
                    $lines[$key]['errors'][] = [
                        'message' => sprintf($this->translate('"%s" already exists in database'),
                            sprintf('<a href="taxon.php?id=%s" target="_new">%s</a>', $d[0]['id'], $d[0]['taxon']))
                    ];
                    // Doesn't make sense to display warnings if taxon won't be imported anyway
                    $lines[$key]['warnings'] = null;
                } else {
                    $lines[$key]['warnings'][] = [
                        'message' => sprintf($this->translate('"%s" already exists in database, but with rank %s'),
                            $name, $val[$this->importColumns['rank']])
                    ];
               }
            }
        }

        $this->setLines($lines);
    }

    private function checkValidTaxa()
    {
        $lines = $this->getLines();

        foreach ((array)$lines as $key => $val) {
            if (empty($val[$this->importColumns['conceptName']])) {
                $lines[$key]['errors'][] = ['message' => $this->translate('no concept name')];
                continue;
            }

            $name = $this->getValidName($val[$this->importColumns['conceptName']]);

            if (!$name) {
                $lines[$key]['errors'][] = [
                    'message' => sprintf($this->translate('valid taxon "%s" does not exist in database'),
                        $val[$this->importColumns['conceptName']])
                ];
                continue;
            }

            $lines[$key]['taxon_id'] = $name[0]['taxon_id'];
        }

        $this->setLines($lines);
    }

    private function getValidName($name)
    {
        $nameParts = explode(' ', $name);
        $where = [
            'project_id' => $this->getCurrentProjectId(),
            'uninomial' => $nameParts[0],
            'type_id' => $this->nameTypeId(PREDICATE_VALID_NAME),
        ];
        // Oh how well-documented this getColumn method is...
        if (count($nameParts) == 1) {
            $where['specific_epithet is'] = null;
            $where['infra_specific_epithet is'] = null;
        } elseif (count($nameParts) == 2) {
            $where['specific_epithet'] = $nameParts[1];
            $where['infra_specific_epithet is'] = null;
        } else {
            $where['specific_epithet'] = $nameParts[1];
            $where['infra_specific_epithet'] = $nameParts[2];
        }
        return $this->models->Names->getColumn(['id' => $where]);
    }

    private function getSynonym($name, $taxonId = false, $nameTypeId = false)
    {
        $nameParts = explode(' ', $name);

        $where = [
            'project_id' => $this->getCurrentProjectId(),
            'uninomial' => $nameParts[0],
        ];
        if ($taxonId) {
            $where['taxon_id'] = $taxonId;
        }
        if ($nameTypeId) {
            $where['type_id'] = $nameTypeId;
        } else {
            $where['type_id in'] = '(' . implode(',', array_column($this->synonymNameTypes(), 'id')) . ')';
        }

        if (count($nameParts) == 1) {
            $where['specific_epithet is'] = null;
            $where['infra_specific_epithet is'] = null;
        } elseif (count($nameParts) == 2) {
            $where['specific_epithet'] = $nameParts[1];
            $where['infra_specific_epithet is'] = null;
        } else {
            $where['specific_epithet'] = $nameParts[1];
            $where['infra_specific_epithet'] = $nameParts[2];
        }

        return $this->models->Names->getColumn(['id' => $where]);
    }

    private function getStatus($taxonId, $statusId = false)
    {
        $where = [
            'project_id' => $this->getCurrentProjectId(),
            'taxon_id' => $taxonId,
        ];
        if ($statusId) {
            $where['presence_id'] = $statusId;
        }

        return $this->models->PresenceTaxa->getColumn(['id' => $where]);
    }

    private function checkRanks()
    {
        $lines = $this->getLines();
        $validranks = $this->getProjectRanks();

        foreach ((array)$lines as $key => $val) {
            if (!isset($val[$this->importColumns['rank']])) {
                $lines[$key]['errors'][] = ['message' => $this->translate('no rank')];
                continue;
            }

            $rank = $val[$this->importColumns['rank']];
            $rankKey = array_search($rank, array_column($validranks, 'rank'));

            if ($rankKey === false) {
                $lines[$key]['errors'][] = ['message' => sprintf($this->translate('illegal rank "%s"'), $rank)];
            } else {
                $lines[$key]['rank_id'] = $validranks[$rankKey]['id'];
            }
        }

        $this->setLines($lines);
    }

    private function checkNameTypes($nameTypes = [])
    {
        $lines = $this->getLines();
        // Could theoretically also be used for non-synonyms, hence the option to also drop in all name types
        $nameTypes = $nameTypes ?: $this->synonymNameTypes();

        foreach ((array)$lines as $key => $val) {
            if (!isset($val[$this->importColumns['nameType']])) {
                $lines[$key]['errors'][] = ['message' => $this->translate('no name type')];
                continue;
            }

            $nameType = $val[$this->importColumns['nameType']];
            $keyNameType = array_search($nameType, array_column($nameTypes, 'nametype_label'));

            if ($keyNameType === false) {
                $lines[$key]['errors'][] = [
                    'message' => sprintf($this->translate('illegal name type "%s"'), $nameType)
                ];
            } else {
                $lines[$key]['name_type_id'] = $nameTypes[$keyNameType]['id'];
            }
        }

        $this->setLines($lines);
    }

    // Use this method after checking for taxa and name types!
    private function checkSynonyms()
    {
        $lines = $this->getLines();

        foreach ((array)$lines as $key => $val) {
            $synonym = $val[$this->importColumns['synonym']] ?? false;
            if (!$synonym) {
                $lines[$key]['errors'][] = ['message' => $this->translate('no synonym')];
                continue;
            }
            // Check if synonym already exists for this taxon; throw error
            if (isset($val['taxon_id']) && isset($val['name_type_id']) && $this->getSynonym($synonym, $val['taxon_id'],
                    $val['name_type_id'])) {
                $lines[$key]['errors'][] = ['message' => $this->translate('name already exists as synonym for this taxon')];
                continue;
            }
            // Check if synonym exists in general, throw warning
            if ($this->getSynonym($synonym)) {
                $lines[$key]['warnings'][] = ['message' => $this->translate('name already exists as synonym')];
            }
        }

        $this->setLines($lines);
    }

    private function checkParents()
    {
        $lines = $this->getLines();

        foreach ((array)$lines as $key => $val) {
            if (!isset($val[$this->importColumns['parent']])) {
                $lines[$key]['warnings'][] = ['message' => $this->translate('no parent specified in file, will save taxon as orphan')];
                continue;
            }

            $parent = $val[$this->importColumns['parent']];
            $parentKey = array_search($parent, array_column($lines, $this->importColumns['conceptName']));

            // suggested parent exists in file
            if ($parentKey !== false) {
                // suggested parent is taxon itself: error
                if ($parentKey == $key) {
                    $lines[$key]['warnings'][] = ['message' => $this->translate('taxon cannot be its own parent')];
                } else {
                    // suggested parent in file has errors, warn (but allow)
                    if (isset($lines[$parentKey]['errors']) && count((array)$lines[$parentKey]['errors']) > 0) {
                        $lines[$key]['warnings'][] = [
                            'message' => sprintf($this->translate('proposed parent "%s" has errors'), $parent)
                        ];
                    } else {
                        // suggested parent in file has no errors (and is not the taxon itself): will use
                        $lines[$key]['parent_id'] = ['source' => 'new', 'id' => $lines[$parentKey]['line_id']];
                    }
                }
            } else {
                $lines[$key]['warnings'][] = [
                    'message' => sprintf($this->translate('parent "%s" not found in file'), $parent)
                ];

                // suggested parent does not exist in file, lets look it up in the database
                $d = $this->getTaxonByName(strtolower($parent));

                // suggested parent exists in the database: will us
                if ($d) {
                    $lines[$key]['parent_id'] = ['source' => 'existing', 'id' => $d['id']];
                    $lines[$key]['warnings'][] = [
                        'message' => sprintf($this->translate('will use valid parent from database (%s)'),
                            sprintf('<a href="taxon.php?id=%s" target="_new">%s</a>', $d['id'], $d['taxon']))
                    ];
                } else {
                    // suggested parent doesn't exist in the database either, will not use (but save taxon as orphan)
                    $lines[$key]['warnings'][] = [
                        'message' => $this->translate('no valid parent found (will save taxon as orphan)')
                    ];
                }
            }
        }

        $this->setLines($lines);
    }

    private function checkStatuses()
    {
        $lines = $this->getLines();
        $statuses = $this->getStatuses();

        foreach ((array)$lines as $key => $val) {
            $status = $val[$this->importColumns['status']];
            if (!isset($status) || $status == '') {
                continue;
            }

            $statusKey = array_search($status, array_column($statuses, 'index_label'));

            if ($statusKey === false) {
                // Warning for taxon import, error for status import
                $errorType = isset($lines[$key]['taxon_id']) ? 'errors' : 'warnings';
                $lines[$key][$errorType][] = [
                    'message' => $this->translate('not a valid presence status')
                ];
                continue;
            }

            // Check if status is already linked to taxon (relevant only for status import!)
            if (isset($lines[$key]['taxon_id'])) {
                // The exact status is already linked to the taxon; skip as error
                if ($this->getStatus($lines[$key]['taxon_id'], $statuses[$statusKey]['id'])) {
                    $lines[$key]['errors'][] = [
                        'message' => $this->translate('presence status already linked to taxon')
                    ];
                    continue;
                }
                // Taxon already has a linked status, present warning
                $currentStatus = $this->getStatus($lines[$key]['taxon_id']);
                if ($currentStatus) {
                    // Fetch label for current status
                    $currentStatusKey = array_search($currentStatus[0]['presence_id'], array_column($statuses, 'id'));
                    $lines[$key]['warnings'][] = [
                        'message' => sprintf($this->translate('status "%s" already linked to taxon, will be unlinked'),
                            $statuses[$currentStatusKey]['index_label'] . " = " . $statuses[$currentStatusKey]['label'])
                    ];
                }
            }

            $lines[$key]['status_id'] = $statuses[$statusKey]['id'];
        }

        $this->setLines($lines);
    }

    private function checkNames()
    {
        $lines = $this->getLines();

        foreach ((array)$lines as $key => $val) {
            if (!isset($val[$this->importColumns['commonName']]) || $val[$this->importColumns['commonName']] == '') {
                continue;
            }

            $name = $val[$this->importColumns['commonName']];

            // checks go here

            $lines[$key]['common_names'][] = ['name' => $name, 'language_id' => $this->getDefaultProjectLanguage()];
        }

        $this->setLines($lines);
    }

    private function checkLanguages()
    {
        $lines = $this->getLines();
        $line = $lines[$this->importRows['languages']];

        foreach ((array)$line as $key => $language) {
            if ($key == 0) {
                continue;
            }

            if (empty($language)) {
                $lines['languages'][$key] = ['column' => $language, 'error' => $this->translate('no language')];
                continue;
            }

            $d = $this->models->Languages->getColumn([
                'id' => [
                    'language' => strtolower($language)
                ]
            ]);

            if ($d) {
                $lines['languages'][$key] = ['column' => $language, 'language_id' => $d[0]['id']];
            } else {
                $d = $this->models->LabelsLanguages->getColumn([
                    'id' => [
                        'project_id' => $this->getCurrentProjectId(),
                        'label' => strtolower($language)
                    ]
                ]);

                if ($d) {
                    $lines['languages'][$key] = ['column' => $language, 'language_id' => $d[0]['language_id']];
                } else {
                    $lines['languages'][$key] = [
                        'column' => $language,
                        'error' => $this->translate('unknown language')
                    ];
                }
            }
        }

        $this->setLines($lines);
    }

    private function checkTopics()
    {
        $lines = $this->getLines();
        $line = $lines[$this->importRows['topicNames']];

        foreach ((array)$line as $key => $topic) {
            if ($key == 0) {
                continue;
            }

            if (empty($topic)) {
                $lines['topics'][$key] = ['column' => $topic, 'error' => $this->translate('no topic')];
                continue;
            }

            $d = $this->models->PagesTaxaTitles->getColumn([
                'id' => [
                    'project_id' => $this->getCurrentProjectId(),
                    'title' => strtolower($topic)
                ]
            ]);

            if (count((array)$d) > 1) {
                $lines['topics'][$key] = [
                    'column' => $topic,
                    'error' => $this->translate('there are multiple topics with that name.')
                ];
            } elseif ($d) {
                $lines['topics'][$key] = ['column' => $topic, 'page_id' => $d[0]['page_id']];
            } else {
                $d = $this->models->PagesTaxa->getColumn([
                    'id' => [
                        'project_id' => $this->getCurrentProjectId(),
                        'page' => strtolower($topic)
                    ]
                ]);

                if ($d) {
                    $lines['topics'][$key] = ['column' => $topic, 'page_id' => $d[0]['id']];
                } else {
                    $lines['topics'][$key] = ['column' => $topic, 'error' => $this->translate('unknown topic')];
                }
            }
        }

        $this->setLines($lines);
    }

    private function prepPassportData()
    {
        $lines = $this->getLines();

        $d = array();
        $i = 0;

        foreach ((array)$lines as $i => $line) {
            if (array_search($i, $this->importRows) !== false) {
                continue;
            }

            foreach ((array)$line as $key => $cell) {
                if ($key == $this->importColumns['conceptName']) {
                    $d[$i]['taxon']['conceptName'] = $cell;

                    $taxon = $this->models->Taxa->getColumn([
                        'id' => [
                            'project_id' => $this->getCurrentProjectId(),
                            'taxon' => strtolower($cell)
                        ]
                    ]);

                    if ($taxon) {
                        $d[$i]['taxon']['id'] = $taxon[0]['id'];
                    } else {
                        $d[$i]['taxon']['error'] = $this->translate('unknown taxon');
                    }
                } else {
                    $d[$i]['data'][$key] = $cell;
                }
            }

            $i++;
        }

        $lines['data'] = $d;

        $this->setLines($lines);
    }

    private function setDoNotImport($ids)
    {
        $this->doNotImport = $ids;
    }

    private function getDoNotImport()
    {
        return $this->doNotImport;
    }

    private function setHandleExisting($setting)
    {
        $this->handleExisting = $setting;
    }

    private function getHandleExisting()
    {
        return $this->handleExisting;
    }

    private function saveConcepts()
    {
        $ignore = $this->getDoNotImport();
        $lines = $this->getLines();

        foreach ((array)$lines as $key => $val) {
            if (isset($val['errors']) && count((array)$val['errors']) > 0) {
                $lines[$key]['import_messages'][] = $this->translate('skipped (due to errors)');
                $lines[$key]['saved'] = false;
                continue;
            }

            if (in_array($val['line_id'], (array)$ignore)) {
                $lines[$key]['import_messages'][] = $this->translate('skipped (by request)');
                $lines[$key]['saved'] = false;
                continue;
            }

            $name = $val[$this->importColumns['conceptName']];
            $authorship = $val[$this->importColumns['authorship']];
            if (!empty($authorship)) {
                $name .= ' ' . $authorship;
            }

            $saveData = [
                'project_id' => $this->getCurrentProjectId(),
                'is_empty' => '0',
                'rank_id' => $val['rank_id'],
                'taxon' => $name,
            ];

            $d = $this->models->Taxa->save($saveData);
            $this->logChange([
                'note' => 'Saved taxon name (taxon batch import)',
                'after' => $saveData
            ]);

            if ($d) {
                $taxon_id = $this->models->Taxa->getNewId();
                $lines[$key]['taxon_id'] = $taxon_id;
                $lines[$key]['import_messages'][] = $this->translate('created taxon concept');
                $lines[$key]['saved'] = true;
                $this->createNsrIds(array('id' => $taxon_id, 'type' => 'taxon'));

                $nameParts = explode(' ', $val[$this->importColumns['conceptName']]);
                $uninomial = $nameParts[0];
                $specificEpithet = $nameParts[1] ?? null;
                $infraspecificEpithet = $nameParts[2] ?? null;

                $saveData = [
                    'project_id' => $this->getCurrentProjectId(),
                    'taxon_id' => $taxon_id,
                    'language_id' => LANGUAGE_ID_SCIENTIFIC,
                    'type_id' => $this->nameTypeId(PREDICATE_VALID_NAME),
                    'name' => $name,
                    'uninomial' => $uninomial,
                    'specific_epithet' => $specificEpithet,
                    'infra_specific_epithet' => $infraspecificEpithet,
                    'authorship' => $authorship,
                    'rank_id' => $val['rank_id']
                ];

                $d = $this->models->Names->save($saveData);

                if ($d) {
                    $this->logChange([
                        'note' => 'Saved taxon concept (taxon batch import)',
                        'after' => $saveData
                    ]);
                    $lines[$key]['valid_name_id'] = $d;
                    $lines[$key]['import_messages'][] = $this->translate('saved valid name');
                } else {
                    $lines[$key]['import_messages'][] = $this->translate('creating valid name failed');
                }
            } else {
                $lines[$key]['import_messages'][] = $this->translate('creating taxon failed');
                $lines[$key]['saved'] = false;
            }
        }

        $this->setLines($lines);
    }

    private function saveSynonyms()
    {
        $ignore = $this->getDoNotImport();
        $lines = $this->getLines();

        foreach ((array)$lines as $key => $val) {
            if (isset($val['errors']) && count((array)$val['errors']) > 0) {
                $lines[$key]['import_messages'][] = $this->translate('skipped (due to errors)');
                $lines[$key]['saved'] = false;
                continue;
            }

            if (in_array($val['line_id'], (array)$ignore)) {
                $lines[$key]['import_messages'][] = $this->translate('skipped (by request)');
                $lines[$key]['saved'] = false;
                continue;
            }

            $name = $val[$this->importColumns['synonym']];
            $nameParts = explode(' ', $name);
            $uninomial = $nameParts[0];
            $specificEpithet = $nameParts[1] ?? null;
            $infraspecificEpithet = $nameParts[2] ?? null;
            $authorship = $val[$this->importColumns['authorship']];

            $saveData = [
                'project_id' => $this->getCurrentProjectId(),
                'taxon_id' => $val['taxon_id'],
                'name' => $name . (!empty($authorship) ? ' ' . $authorship : ''),
                'language_id' => LANGUAGE_ID_SCIENTIFIC,
                'type_id' => $val['name_type_id'],
                'uninomial' => $uninomial,
                'specific_epithet' => $specificEpithet,
                'infra_specific_epithet' => $infraspecificEpithet,
                'authorship' => $authorship,
                'rank_id' => $val['rank_id']
            ];

            $d = $this->models->Names->save($saveData);

            if ($d) {
                $this->logChange([
                    'note' => 'Saved synonym (taxon batch import)',
                    'after' => $saveData
                ]);
                $lines[$key]['import_messages'][] = $this->translate('created synonym');
                $lines[$key]['saved'] = true;
            } else {
                $lines[$key]['import_messages'][] = $this->translate('creating synonym failed');
                $lines[$key]['saved'] = false;
            }
        }

        $this->setLines($lines);
    }

    private function saveNames()
    {
        $lines = $this->getLines();

        foreach ((array)$lines as $key => $val) {
            if ($val['saved'] !== true || !isset($val['common_names'])) {
                continue;
            }

            foreach ((array)$val['common_names'] as $ckey => $cval) {
                $saveData = [
                    'project_id' => $this->getCurrentProjectId(),
                    'taxon_id' => $val['taxon_id'],
                    'language_id' => $cval['language_id'],
                    'type_id' => $this->nameTypeId(PREDICATE_PREFERRED_NAME),
                    'name' => $cval['name'],
                ];

                $d = $this->models->Names->save($saveData);

                if ($d) {
                    $this->logChange([
                        'note' => 'Saved name (taxon batch import)',
                        'after' => $saveData
                    ]);
                    $lines[$key]['import_messages'][] = $this->translate('saved common name');
                    $lines[$key]['common_names'][$ckey]['valid_name_id'] = $d;
                } else {
                    $lines[$key]['import_messages'][] = $this->translate('saving common name failed');
                }
            }
        }

        $this->setLines($lines);
    }

    private function saveParents()
    {
        $lines = $this->getLines();

        foreach ((array)$lines as $key => $val) {
            if ($val['saved'] !== true) {
                continue;
            }

            if (!isset($val['parent_id'])) {
                $lines[$key]['import_messages'][] = $this->translate('saving without parent');
                continue;
            }

            $parentId = null;

            if ($val['parent_id']['source'] == 'new') {
                $parentKey = array_search($val['parent_id']['id'], array_column($lines, 'line_id'));

                if ($parentKey !== false && isset($lines[$parentKey]['taxon_id'])) {
                    $parentId = $lines[$parentKey]['taxon_id'];
                }
            } elseif ($val['parent_id']['source'] == 'existing' && isset($val['parent_id']['id'])) {
                $parentId = $val['parent_id']['id'];
            }

            if (is_null($parentId)) {
                continue;
            }

            $d = $this->models->Taxa->update(['parent_id' => $parentId],
                ['id' => $val['taxon_id'], 'project_id' => $this->getCurrentProjectId()]);

            if ($d) {
                $lines[$key]['import_messages'][] = $this->translate('saved parent');
            } else {
                $lines[$key]['import_messages'][] = $this->translate('saving parent failed');
            }
        }

        $this->setLines($lines);
    }

    private function saveStatuses()
    {
        $lines = $this->getLines();

        foreach ((array)$lines as $key => $val) {
            if (!isset($val['status_id'])) {
                continue;
            }

            // Delete any previous status (cannot have more than one)
            $this->models->PresenceTaxa->delete([
                'project_id' => $this->getCurrentProjectId(),
                'taxon_id' => $val['taxon_id'],
            ]);

            $d = $this->models->PresenceTaxa->save([
                'project_id' => $this->getCurrentProjectId(),
                'taxon_id' => $val['taxon_id'],
                'presence_id' => $val['status_id'],
            ]);

            if ($d) {
                $lines[$key]['import_messages'][] = $this->translate('saved presence status');
                $lines[$key]['saved'] = true;
            } else {
                $lines[$key]['import_messages'][] = $this->translate('saving presence status failed');
            }
        }

        $this->setLines($lines);
    }

    private function setSynonymImportColumns()
    {
        return $this->importColumns = [
            'synonym' => 0,
            'authorship' => 1,
            'rank' => 2,
            'conceptName' => 3,
            'nameType' => 4,
        ];
    }

    private function setStatusImportColumns()
    {
        return $this->importColumns = [
            'conceptName' => 0,
            'status' => 1,
        ];
    }

    private function setCsvDelimiter()
    {
        switch ($this->rGetVal("delimiter")) {
            case 'tab':
                $this->helpers->CsvParserHelper->setFieldDelimiter("\t");
                break;
            case 'semi-colon':
                $this->helpers->CsvParserHelper->setFieldDelimiter(';');
                break;
            default:
                //case 'comma':
                $this->helpers->CsvParserHelper->setFieldDelimiter(',');
        }
    }

    private function resetImport()
    {
        $this->setLines(null);
        $this->setSessionLines();
    }

    private function synonymNameTypes()
    {
        // Remove valid name from the list and re-index values
        return array_values(
            array_filter($this->nameTypes(), function ($value) {
                return $value['id'] != $this->nameTypeId(PREDICATE_VALID_NAME);
            })
        );
    }

    private function nameTypes()
    {
        if (empty($this->nameTypes)) {
            $this->nameTypes = $this->getNameTypes();
        }
        return $this->nameTypes;
    }

    private function nameTypeId($constant)
    {
        $key = array_search($constant, array_column($this->nameTypes(), 'nametype'));
        return $this->nameTypes()[$key]['id'] ?? false;
    }
}