<?php

namespace Linnaeus\Admin\Controllers;

use Linnaeus\Admin\Controllers\Controller;

class TaxonParentageController extends Controller
{
    public $usedModels = array(
        'taxon_quick_parentage'
    );

    public $controllerPublicName = 'Quick Taxon Parentage';
    public $modelNameOverride = 'TaxonParentageModel';

    public function __construct()
    {
        parent::__construct();
        $this->initialize();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    private function initialize()
    {
        if (!$this->models->TaxonQuickParentage->hasTable()) {
            $this->addError('table TaxonQuickParentage does not exist');
            return;
        }

        set_time_limit(600);
    }

    public function getParentageTableRowCount()
    {
        $d = $this->models->TaxonQuickParentage->getColumn([
            'id' => ['project_id' => $this->getCurrentProjectId()],
            'columns' => 'count(*) as total'
        ]);

        return $d[0]['total'];
    }

    public function generateParentageAll()
    {
        $id = $this->treeGetTop();

        if (empty($id)) {
            $this->addError("Didn't find a taxon tree top.");
            return;
        }

        $this->tmp = 0;
        // circumventing unexplained errors (mysqli spontaneously getting lost)
        //$this->models->TaxonQuickParentage->delete(['project_id' => $this->getCurrentProjectId()]);

        // Truncate table if only one project used to circumvent auto-increment running into limit
        if (FIXED_PROJECT_ID <= 0 || empty(FIXED_PROJECT_ID)) {
            $queries[] = "delete from " . $this->models->TaxonQuickParentage->tableName . " where project_id = " . $this->getCurrentProjectId();
        } else {
            $queries[] = "truncate table " . $this->models->TaxonQuickParentage->tableName;
            $queries[] = "alter table " . $this->models->TaxonQuickParentage->tableName . " auto_increment = 1";
        }
        foreach ($queries as $query) {
            mysqli_query($this->models->TaxonQuickParentage->databaseConnection, $query);
        }
        $this->getProgeny($id, 0, array());
        return $this->tmp;
    }

    public function generateParentage($id)
    {
        $this->tmp = 0;

        // circumventing unexplained errors (mysqli spontaneously getting lost)
        // $this->models->TaxonQuickParentage->delete( [ 'project_id' => $this->getCurrentProjectId(), 'taxon_id'=>$id ] );

        $query = "delete from " . $this->models->TaxonQuickParentage->tableName
            . " where project_id = " . $this->getCurrentProjectId()
            . " and MATCH(parentage) AGAINST ('" . self::generateTaxonParentageId($id) . "' in boolean mode)";

        mysqli_query($this->models->TaxonQuickParentage->databaseConnection, $query);

        $this->getProgeny($id, 0, array());
        return $this->tmp;
    }

    public function addTaxonById($id)
    {
        $taxon = $this->getTaxonById($id);

        if ($taxon) {
            $d = $this->models->TaxonQuickParentage->getColumn([
                'id' => [
                    'project_id' => $this->getCurrentProjectId(),
                    'taxon_id' => $taxon['parent_id']
                ],
                'columns' => 'parentage'
            ]);

            if (isset($d[0])) {
                // First delete existing entry
                $this->models->TaxonQuickParentage->delete([
                    'taxon_id' => $id,
                    'project_id' => $this->getCurrentProjectId()
                ]);

                $parentage = $d[0]['parentage'] . ' ' . $this->generateTaxonParentageId($taxon['parent_id']);

                $this->models->TaxonQuickParentage->save([
                    'project_id' => $this->getCurrentProjectId(),
                    'taxon_id' => $id,
                    'parentage' => $parentage
                ]);
            }
        }
    }

    private function treeGetTop()
    {
        /*
            get the top taxon = no parent
            "_r.id < 10" added as there might be orphans, which are ususally low-level ranks
        */

        $p = $this->models->TaxonParentageModel->getTreeTop(['project_id' => $this->getCurrentProjectId()]);

        if ($p && count((array)$p) == 1) {
            $p = $p[0]['id'];
        } else {
            $p = null;
        }

        if (count((array)$p) > 1) {
            $this->addError('Detected multiple high-order taxa without a parent. Unable to determine which is the top of the tree.');
        }

        return $p;
    }

    private function getProgeny($parent, $level, $family)
    {
        $family[] = $this->generateTaxonParentageId($parent);

        $result = $this->models->Taxa->getColumn([
            'id' => [
                'project_id' => $this->getCurrentProjectId(),
                'parent_id' => $parent
            ],
            'columns' => 'id,parent_id,taxon,' . $level . ' as level'
        ]);

        foreach ((array)$result as $row) {
            $row['parentage'] = $family;

            $this->models->TaxonQuickParentage->save([
                'id' => null,
                'project_id' => $this->getCurrentProjectId(),
                'taxon_id' => $row['id'],
                'parentage' => implode(' ', $row['parentage'])
            ]);

            $this->tmp++;

            $this->getProgeny($row['id'], $level + 1, $family);
        }
    }
}
