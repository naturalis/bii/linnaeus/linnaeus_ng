<?php

namespace Linnaeus\Admin\Controllers;

use Linnaeus\Admin\Configuration;

class TranslatorController
{
    private $environment;
    private $languageid;
    private $model;
    private $text;
    private $translation;
    private $didTranslate = false;
    private $isNewString = false;
    private $newStrings = array();
    private $storeNewInterfaceStrings;

    public function __construct($p)
    {
        $this->config = new Configuration();
        $this->generalSettings = $this->config->getGeneralSettings();

        $this->model = $p['model'] ?: null;
        $this->environment = $p['environment'] ?: null;
        $this->languageid = $p['language_id'] ?: null;
        $this->storeNewInterfaceStrings = $p['store_new_interface_strings'] ?: false;
    }

    public function translate($s)
    {
        if (empty($s)) {
            return;
        }

        if (is_array($s)) {
            $translations = array();
            foreach ($s as $key => $val) {
                $this->text = $val;
                $this->doTranslate();
                if ($this->storeNewInterfaceStrings) {
                    $this->rememberNewString();

                    $this->saveNewStrings();
                    $this->newStrings = [];
                }

                if ($this->didTranslate) {
                    $translations[$key] = $this->translation;
                } else {
                    $translations[$key] = $this->text;
                }
            }
            return $translations;
        } else {
            $this->text = $s;
            $this->doTranslate();
            if ($this->storeNewInterfaceStrings) {
                $this->rememberNewString();
                $this->saveNewStrings();
                $this->newStrings = [];
            }

            if ($this->didTranslate) {
                return $this->translation;
            } else {
                return $this->text;
            }
        }
    }

    private function doTranslate()
    {
        $this->didTranslate = false;
        $this->isNewString = false;

        if (empty($this->text)) {
            return;
        }

        if (empty($this->languageid)) {
            return;
        }

        // get id of the text
        $i = $this->model->freeQuery("
			select
				id
			from
				%PRE%interface_texts
			where
				text = '" . $this->escapeString($this->text) . "'
				and env = '" . $this->escapeString($this->environment) . "'
        ");

        // if not found, return unchanged
        if (empty($i[0]['id'])) {
            $this->isNewString = true;
            return;
        }

        // fetch appropriate translation
        $it = $this->model->freeQuery("
			select
				id,translation
			from
				%PRE%interface_translations
			where
				interface_text_id = " . $i[0]['id'] . " 
				and language_id = " . $this->languageid . "
        ");

        // if not found, return unchanged
        if (empty($it[0]['id'])) {
            return;
        }

        $this->translation = $it[0]['translation'];
        $this->didTranslate = true;
    }

    private function rememberNewString()
    {
        if (!$this->didTranslate) {
            array_push($this->newStrings, $this->text);
        }
    }

    private function saveNewStrings()
    {
        $this->newStrings = array_unique($this->newStrings);

        foreach ($this->newStrings as $text) {
            $d = $this->model->freeQuery("
				select
					id
				from
					%PRE%interface_texts
				where
					text = '" . $this->escapeString($text) . "'
					and env = '" . $this->escapeString($this->environment) . "'
			");

            if (!$d) {
                $d = $this->model->freeQuery("
					insert ignore into %PRE%interface_texts
						(text,env)
					values
						('" . $this->escapeString($text) . "','" . $this->escapeString($this->environment) . "')
				");
            }
        }
    }

    private function escapeString($s)
    {
        return mysqli_real_escape_string($this->model->databaseConnection, $s);
    }
}
