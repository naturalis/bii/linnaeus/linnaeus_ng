<?php

namespace Linnaeus\Admin\Controllers;

use Linnaeus\Admin\Controllers\Controller;
use Linnaeus\Admin\Controllers\ModuleSettingsReaderController;
use Linnaeus\Admin\Controllers\TraitsTaxonController;

// run as .../export_versatile.php?printquery to print actual query as well

class VersatileExportController extends Controller
{
    public $usedModels = array(
        'presence',
        'names',
        'name_types',
        'traits_index',
    );

    public $modelNameOverride = 'VersatileExportModel';
    public $controllerPublicName = 'Export';

    public $usedHelpers = array(
        'zip_file'
    );

    public $cssToLoad = array(
        'lookup.css',
        'nsr_taxon_beheer.css'
    );

    public $extraModels = array(
        'TraitsModel'
    );

    public $jsToLoad = array(
        'all' => array(
            'lookup.js',
            'nsr_taxon_beheer.js'
        )
    );

    private $branchTopId;
    private $presenceLabels;
    private $selectedRanks;
    private $selectedSynonymTypes;
    private $allRanks;
    private $rankOperator;
    private $cols;
    private $nameParts;
    private $ancestors;
    private $doSynonyms;
    private $doHybridMarker;
    private $fieldSep;
    private $newLine;
    private $noQuotes;
    private $keepTags;
    private $utf8ToUtf16;
    private $outputTarget;
    private $printQuery = false;
    private $nameTypeIds;
    private $suppressUnderscoredFields = true;
    private $replaceUnderscoresInHeaders = true;
    private $doPrintQueryParameters = true;
    private $printEOFMarker = false;
    private $quoteChar = '"';
    private $queryBitNameParts;
    private $query;
    private $queries;
    private $conceptUrl;
    private $names = array();
    private $synonyms = array();
    private $tc;
    private $traitGroups;
    private $selectedTraits = [];
    private $parentRegister = array();
    private $operators = [
        "eq" => ["operator" => "=", "label" => "is:"],
        "ge" => ["operator" => ">=", "label" => "equal or below:"],
        "in" => ["operator" => "in", "label" => "in:"],
    ];

    private $dbFirstCreated;
    private $dbLastCreated;
    private $dbFirstModified;
    private $dbLastModified;
    private $firstCreated;
    private $lastCreated;
    private $firstModified;
    private $lastModified;
    private $firstLastCreatedErrors = [];
    private $firstLastModifiedErrors = [];
    private $orderBy = "_f.rank_id asc,_r.id, _t.taxon";  // rank, rank id, taxon
    private $showNsrSpecificStuff;
    private $spoofSettings;

    private $limit = 20000;
    private $offset = 0;

    private $traitGroupLookup = [];

    /*
     when the number of names is larger than synonymStrategyThreshold, the
     program will fetch all synonyms in one query, and filter and assign them
     to the correct taxon in PHP next. below the threshold, a separate query
     will be executed for each taxon.
     2000 is an arbitrary number, and shoud be calibrated.
     */
    private $synonymStrategyThreshold = 2000;

    private $fileHandleNames;
    private $namesFileName = "%s-export--%s.csv";
    private $namesFilePath;

    private $fileHandleSynonyms;
    private $synonymsFileName = "%s-export-synonyms--%s.csv";
    private $synonymsFilePath;

    private $fileHandle;
    private $endOfFileMarker = '(end of file)';

    private $columnHeaders = [
        'sci_name' => 'scientific_name',
        'dutch_name' => 'common_name',
        'rank' => 'rank',
        'nsr_id' => 'nsr_id',
        'presence_status' => 'presence_status',
        'presence_status_publication' => 'presence_status_publication',
        'habitat' => 'habitat',
        'concept_url' => 'concept_url',
        'database_id' => 'database_id',
        'parent_taxon' => 'parent_taxon',
        'parent_taxon_nsr_id' => 'parent_taxon_nsr_id',
        'uninomial' => 'uninomial',
        'specific_epithet' => 'specific_epithet',
        'infra_specific_epithet' => 'infra_specific_epithet',
        'authorship' => 'authorship',
        'name_author' => 'name_author',
        'authorship_year' => 'authorship_year',
        'synonym' => 'synonym',
        'synonym_type' => 'type_synonym',
        'language' => 'language',
        'taxon' => 'taxon',
        'remark' => 'remark',
        'reference' => 'reference',
        'created' => 'created',
        'modified' => 'last_modified',
    ];


    public function __construct()
    {
        set_time_limit(600);
        parent::__construct();

        $this->switchToPersistentConnection();
        $this->initialize();

        $this->tc = new TraitsTaxonController();
        $this->tc->switchToPersistentConnection();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function ajaxInterfaceAction()
    {
        if (!$this->rHasVal('action')) {
            $this->smarty->assign('returnText', 'error');
        }

        if ($this->rHasVal('action', 'update')) {
            $this->smarty->assign('returnText', $this->createTraitsIndex(5000));
        }

        if ($this->rHasVal('action', 'empty')) {
            $this->emptyTraitsIndex();
            $this->smarty->assign('returnText', 'index emptied');
        }

        $this->allowEditPageOverlay = false;
        $this->printPage();
    }

    private function initialize()
    {
        $this->UserRights->setRequiredLevel(ID_ROLE_LEAD_EXPERT);
        $this->checkAuthorisation();
        $this->setNameTypeIds();
        $this->getTraits();
        $this->setFirstLastCreatedModified();

        $this->moduleSettings = new ModuleSettingsReaderController();
        $this->conceptUrl = $this->moduleSettings->getGeneralSetting('concept_base_url');
        $this->showNsrSpecificStuff = $this->moduleSettings->getGeneralSetting('show_nsr_specific_stuff', 0) == 1;

        $this->namesFileName = sprintf($this->namesFileName, $this->getProjectTitle(true), date('Ymd-His'));
        $this->synonymsFileName = sprintf($this->synonymsFileName, $this->getProjectTitle(true), date('Ymd-His'));

        if (!is_null($this->customConfig) && method_exists($this->customConfig, 'getVersatileExportSpoof')) {
            $this->spoofSettings = $this->customConfig->getVersatileExportSpoof();
            if ($this->spoofSettings->do_spoof_export) {
                $this->smarty->assign('spoof_settings_warning', $this->spoofSettings->texts->warning);
            }
        }

        foreach ($this->columnHeaders as $key => $val) {
            $this->columnHeaders[$key] = $this->translate($val);
        }
    }

    public function exportAction()
    {
        $this->setPageName($this->translate('Multi-purpose export'));

        if ($this->rHasVal('action', 'export')) {
            $this->setBranchTopSession(array($this->rGetVal('branch_top_id'), $this->rGetVal('branch_top_label')));
            $this->setBranchTopId($this->rGetVal('branch_top_id'));
            $this->setPresenceStatusLabels($this->rGetVal('presence_labels'));
            $this->setAllRanks($this->rGetVal('all_ranks'));
            $this->setSelectedRanks($this->rGetVal('selected_ranks'));
            $this->setRankOperator($this->rGetVal('rank_operator'));
            $this->setCols($this->rGetVal('cols'));
            $this->setDoHybridMarker($this->rGetVal('add_hybrid_marker'));
            $this->setNameParts($this->rGetVal('name_parts'));
            $this->setAncestors($this->rGetVal('ancestors'));
            $this->setDoSynonyms($this->rGetVal('synonyms'));
            $this->setSelectedTraits($this->rGetVal('traits'));
            $this->setSelectedSynonymTypes($this->rGetVal('nametypes'));
            $this->setOrderBy($this->rGetVal('order_by'));
            $this->setFieldSep($this->rGetVal('field_sep'));
            $this->setNewLine($this->rGetVal('new_line'));
            $this->setNoQuotes($this->rGetVal('no_quotes'));
            $this->setReplaceUnderscoresInHeaders($this->rGetVal('replace_underscores_in_headers'));
            $this->setKeepTags($this->rGetVal('keep_tags'));
            $this->setDoPrintQueryParameters($this->rGetVal('print_query_parameters'));
            $this->setPrintEOFMarker($this->rGetVal('print_eof_marker'));
            $this->setOutputTarget($this->rGetVal('output_target'));
            $this->setPrintQuery($this->rHasVar('printquery')); // undoc'd feat. start as export_versatile.php?printquery to add full queries to output
            $this->setNameTypeIds();

            echo $this->doOutputStart();
            do {
                $this->doMainQuery();
                $this->doAncestry();
                $this->doTraits();
                $this->doSynonymsQuery();
                $this->doOutput();
                $this->offset += $this->limit;
            } while (!empty($this->names));
            $this->doOutputEnd();
        }

        $dt = $this->models->VersatileExportModel->getIndexLastUpdate();
        $lastUpdate = $this->formatDateTime($dt) ?? '';

        $this->smarty->assign('presence_labels', $this->getPresenceStatuses());
        $this->smarty->assign('ranks', $this->getRanks());
        $this->smarty->assign('traits', $this->traitGroups);
        $this->smarty->assign('branch_top', $this->getBranchTopSession());
        $this->smarty->assign('nametypes', $this->nameTypeIds);
        $this->smarty->assign('is_nsr', $this->showNsrSpecificStuff);
        $this->smarty->assign('index_last_update', $lastUpdate);

        $this->smarty->assign('first_created', $this->firstCreated);
        $this->smarty->assign('last_created', $this->lastCreated);
        $this->smarty->assign('first_modified', $this->firstModified);
        $this->smarty->assign('last_modified', $this->lastModified);

        $this->smarty->assign('db_first_created', $this->dbFirstCreated);
        $this->smarty->assign('db_last_created', $this->dbLastCreated);
        $this->smarty->assign('db_first_modified', $this->dbFirstModified);
        $this->smarty->assign('db_last_modified', $this->dbLastModified);

        $this->printPage();
    }

    private function setSelectedTraits($selected = false)
    {
        if (!empty($selected) && !empty($this->traitGroups)) {
            foreach ($selected as $group => $traits) {
                $groupName = $this->traitGroups[$group]['name'];
                $groupTraits = array_column($this->traitGroups[$group]['traits'], 'name', 'id');
                foreach ($traits as $trait) {
                    $this->selectedTraits[$group . '-' . $trait] = strtolower(
                        str_replace(
                            ' ',
                            '_',
                            $groupName . ':' . $groupTraits[$trait]
                        )
                    );
                }
            }
        }
    }

    private function setFirstLastCreatedModified()
    {
        [$this->dbFirstCreated, $this->dbLastCreated, $this->dbFirstModified, $this->dbLastModified] =
            array_values($this->models->VersatileExportModel->getFirstLastCreatedModified());

        // isValidDate method handles potential SQL injection
        $firstCreated = $this->rGetVal('first_created');
        $this->firstCreated = $this->isValidDate($firstCreated) && $firstCreated > $this->dbFirstCreated ?
            $firstCreated : $this->dbFirstCreated;
        if ($firstCreated != $this->firstCreated) {
            $this->firstLastCreatedErrors[] = "first date '$firstCreated'";
        }

        $lastCreated = $this->rGetVal('last_created');
        $this->lastCreated = $this->isValidDate($lastCreated) && $lastCreated < $this->dbLastCreated ?
            $lastCreated : $this->dbLastCreated;
        if ($lastCreated != $this->lastCreated) {
            $this->firstLastCreatedErrors[] = "last date '$lastCreated'";
        }

        $firstModified = $this->rGetVal('first_modified');
        $this->firstModified = $this->isValidDate($firstModified) && $firstModified > $this->dbFirstModified ?
            $firstModified : $this->dbFirstModified;
        if ($firstModified != $this->firstModified) {
            $this->firstLastModifiedErrors[] = "first date '$firstModified'";
        }

        $lastModified = $this->rGetVal('last_modified');
        $this->lastModified = $this->isValidDate($lastModified) && $lastModified < $this->dbLastModified ?
            $lastModified : $this->dbLastModified;
        if ($lastModified != $this->lastModified) {
            $this->firstLastModifiedErrors[] = "last date '$lastModified'";
        }
    }

    private function getPresenceStatuses()
    {
        return $this->models->VersatileExportModel->getPresenceStatuses(
            array(
                "project_id" => $this->getCurrentProjectId()
            )
        );
    }

    private function getTraits()
    {
        $this->tc = new TraitsTaxonController();
        $groups = $this->tc->getTraitgroups();
        if (!empty($groups)) {
            foreach ($groups as $group) {
                $this->traitGroups[$group['id']] = $this->tc->getTraitgroup($group['id']);
            }
            //            $this->customSortArray($this->traitGroups, ['key' => 'name', 'maintainKeys' => true]);
            foreach ($this->traitGroups as $i => $d) {
                $traits = $d['traits'];
                //                $this->customSortArray($traits, ['key' => 'name']);
                $this->traitGroups[$i]['traits'] = $traits;
            }
            return $this->traitGroups;
        }
        return false;
    }

    private function getRanks()
    {
        return $this->models->VersatileExportModel->getRanks(
            array(
                "project_id" => $this->getCurrentProjectId()
            )
        );
    }

    private function setNameTypeIds()
    {
        $this->nameTypeIds = $this->models->NameTypes->getColumn(
            array(
                'id' => array(
                    'project_id' => $this->getCurrentProjectId()
                ),
                'columns' => "
				id,
				nametype,substring(substring(name_types.nametype,3),1,length(name_types.nametype)-4) as nametype_hr,
				case
					when
						nametype = '" . PREDICATE_PREFERRED_NAME . "'
					then 0
					when
						nametype = '" . PREDICATE_ALTERNATIVE_NAME . "'
					then 1
					else 2
				end as sortfield",
                'fieldAsIndex' => 'nametype',
                'order' => 'sortfield,nametype'
            )
        );
    }

    private function doMainQuery()
    {

        $this->queryBitNameParts = '';

        if ($this->hasCol('name_parts')) {
            foreach ($this->getNameParts() as $key => $val) {
                $this->queryBitNameParts .= '_names.' . $key . ' as ' . $this->columnHeaders[$key] . ', ';
            }
        }

        $ranksClause = "";
        if (!$this->getAllRanks()) {
            $ranksClause = "and _f.rank_id " . $this->operators[$this->getRankOperator()]['operator'] . " (" . implode(",", $this->getSelectedRanks()) . ")";
        }

        $presenceStatusClause = "";
        $p = $this->getPresenceStatusLabels();
        if (!empty($p)) {
            $presenceStatusClause = "and _h.index_label in ('" . implode("','", $p) . "') ";
        }

        $createdClause = $modifiedClause = "";
        // Only add clause if values entered differ from database values
        // Database first/last values have been set at initialisation
        if ($this->firstCreated > $this->dbFirstCreated || $this->lastCreated < $this->dbLastCreated) {
            $createdClause = "and _t.created >= '$this->firstCreated' and _t.created <= '$this->lastCreated'";
        }
        if ($this->firstModified > $this->dbFirstModified || $this->lastModified < $this->dbLastModified) {
            $modifiedClause = "and _t.last_change >= '$this->firstModified' and _t.last_change <= '$this->lastModified'";
        }

        $this->query = "
			select
				" . ($this->hasCol('sci_name') ? " _t.taxon as " . $this->columnHeaders['sci_name'] . ", " : "") . "
				" . ($this->queryBitNameParts) . "
				" . ($this->hasCol('dutch_name') ? " _z.name as " . $this->columnHeaders['dutch_name'] . ", " : "") . "
				" . ($this->hasCol('rank') ? " ifnull(_lpr.label,_r.rank) as " . $this->columnHeaders['rank'] . ", " : "") . "
				" . ($this->hasCol('nsr_id') ? " replace(_b.nsr_id,'tn.nlsr.concept/','') as " . $this->columnHeaders['nsr_id'] . ", " : "") . "
				" . ($this->hasCol('presence_status') ? " _h.index_label as " . $this->columnHeaders['presence_status'] . ", " : "") . "
				" . ($this->hasCol('presence_status_publication') ? " 
                    if (_gl.author is null, 
                        concat(coalesce(_gl.`label`,''),', ',coalesce(_gl.`date`,'')), 
                        concat(coalesce(_gl.author,''),', ',coalesce(_gl.`date`,''),', ',coalesce(_gl.`label`,''))
                    ) as " . $this->columnHeaders['presence_status_publication'] . ", " : "") . "
				" . ($this->hasCol('habitat') ? " _hab.label as " . $this->columnHeaders['habitat'] . ", " : "") . "
				" . ($this->hasCol('concept_url') ? " concat('" . $this->conceptUrl . "',replace(_b.nsr_id,'tn.nlsr.concept/','')) as " . $this->columnHeaders['concept_url'] . ", " : "") . "
				" . ($this->hasCol('database_id') ? " _q.taxon_id as " . $this->columnHeaders['database_id'] . ", " : "") . "
				" . ($this->hasCol('parent_taxon') ? " _pnames.name as " . $this->columnHeaders['parent_taxon'] . ", " : "") . "
				" . ($this->hasCol('parent_taxon_nsr_id') ? " replace(_pid.nsr_id,'tn.nlsr.concept/','') as " . $this->columnHeaders['parent_taxon_nsr_id'] . ", " : "") . "
				_q.taxon_id as _taxon_id,
				_t.parent_id as _parent_id,
				_r.id as _base_rank_id
				" . ($this->hasCol('created') ? ", date(_t.created) as " . $this->columnHeaders['created'] : "") . "
				" . ($this->hasCol('modified') ? ", date(_t.last_change) as " . $this->columnHeaders['modified'] : "") . "
				    
			from %PRE%taxon_quick_parentage _q
				    
			right join %PRE%taxa _t
				on _q.taxon_id = _t.id
				    
			left join %PRE%names _z
				on _q.taxon_id=_z.taxon_id
				and _q.project_id=_z.project_id
				and _z.type_id= " . $this->nameTypeIds[PREDICATE_PREFERRED_NAME]['id'] . "
				and _z.language_id=" . $this->getDefaultProjectLanguage() . "
				    
			left join %PRE%names _names
				on _q.taxon_id=_names.taxon_id
				and _q.project_id=_names.project_id
				and _names.type_id= " . $this->nameTypeIds[PREDICATE_VALID_NAME]['id'] . "
				and _names.language_id=" . LANGUAGE_ID_SCIENTIFIC . "
				    
			" . ($this->hasCol('habitat') ? "
			    
				left join %PRE%presence_taxa _pre
					on _q.taxon_id=_pre.taxon_id
					and _q.project_id=_pre.project_id
			    
				left join %PRE%habitat_labels _hab
					on _pre.habitat_id = _hab.habitat_id
					and _pre.project_id=_hab.project_id
					and _hab.language_id=" . LANGUAGE_ID_DUTCH . "
			    
			" : "") .

            ($this->hasCol('parent_taxon') || $this->hasCol('parent_taxon_nsr_id') ? "
			    
				left join %PRE%taxa _ptaxa
					on _t.parent_id=_ptaxa.id
					and _t.project_id=_ptaxa.project_id
			    
				left join %PRE%names _pnames
					on _ptaxa.id=_pnames.taxon_id
					and _ptaxa.project_id=_pnames.project_id
					and _pnames.type_id= " . $this->nameTypeIds[PREDICATE_VALID_NAME]['id'] . "
					and _pnames.language_id=" . LANGUAGE_ID_SCIENTIFIC . "
			    
				left join %PRE%nsr_ids _pid
					on _ptaxa.project_id = _pid.project_id
					and _ptaxa.id = _pid.lng_id
					and _pid.item_type = 'taxon'
			    
			" : "") . "
			    
			left join %PRE%projects_ranks _f
				on _t.rank_id=_f.id
				and _t.project_id=_f.project_id
			    
			left join %PRE%ranks _r
				on _f.rank_id=_r.id
			    
			left join %PRE%labels_projects_ranks _lpr
				on _f.project_id=_lpr.project_id
				and _f.id=_lpr.project_rank_id
				and _lpr.language_id = " . LANGUAGE_ID_DUTCH . "
				    
			left join %PRE%nsr_ids _b
				on _t.project_id = _b.project_id
				and _t.id = _b.lng_id
				and _b.item_type = 'taxon'
				    
			left join %PRE%presence_taxa _g
				on _t.id=_g.taxon_id
				and _t.project_id=_g.project_id
				    
			left join %PRE%literature2 _gl
				on _g.reference_id=_gl.id
				and _t.project_id=_gl.project_id
				    
			left join %PRE%presence_labels _h
				on _g.presence_id = _h.presence_id
				and _g.project_id=_h.project_id
				and _h.language_id=" . LANGUAGE_ID_DUTCH . "
				    
			left join %PRE%trash_can _trash
				on _t.project_id = _trash.project_id
				and _t.id =  _trash.lng_id
				and _trash.item_type='taxon'
				    
			where
				_q.project_id=" . $this->getCurrentProjectId() . "
				" . $ranksClause . "
				" . $presenceStatusClause . "
				" . $createdClause . "
				" . $modifiedClause . "
				and (
					match(_q.parentage) against ('" . $this->generateTaxonParentageId($this->getBranchTopId()) . "' in boolean mode) or
					_q.taxon_id = " . $this->getBranchTopId() . "
				)
					    
			and ifnull(_trash.is_deleted,0)=0
					    
			order by " . $this->getOrderBy() . "
			    
			limit " . $this->offset . ',' . $this->limit;

        $this->names = $this->models->VersatileExportModel->doMainQuery(array("query" => $this->query));

        if ($this->getPrintQuery()) {
            $this->queries['main'] = $this->models->VersatileExportModel->getLastQuery();
        }

        if ($this->hasCol('sci_name') && $this->getDoHybridMarker()) {
            foreach ((array) $this->names as $key => $val) {
                $this->names[$key]['scientific_name'] =
                    $this->addHybridMarkerAndInfixes([
                        'name' => $val['scientific_name'],
                        'base_rank_id' => $val['_base_rank_id'],
                        'taxon_id' => $val['_taxon_id'],
                        'parent_id' => $val['_parent_id']
                    ]);
                }
        }

        if ($this->hasCol('sci_name') && $this->getKeepTags() == false) {
            foreach ((array) $this->names as $key => $val) {
                $this->names[$key]['scientific_name'] = html_entity_decode(strip_tags($this->names[$key]['scientific_name']));
            }
        }
    }

    private function findAncestor($id, $rankId)
    {
        if (!isset($this->parentRegister[$id])) {
            $row = $this->models->VersatileExportModel->findAncestor(
                array(
                    "project_id" => $this->getCurrentProjectId(),
                    "taxon_id" => $id
                )
            );

            $this->parentRegister[$id] = $row;
        } else {
            $row = $this->parentRegister[$id];
        }

        if ($row['rank_id'] == $rankId) {
            return $row;
        } elseif (!empty($row['parent_id'])) {
            return $this->findAncestor($row['parent_id'], $rankId);
        }
    }

    private function doAncestry()
    {
        if (!$this->hasCol('ancestors')) {
            return;
        }

        foreach ((array) $this->names as $key => $val) {
            foreach ((array) $this->getAncestors() as $label => $id) {
                $d = $this->findAncestor($val['_parent_id'], $id);
                $this->names[$key][$label] = $d['taxon'];
            }
        }
    }

    private function doTraits()
    {
        if (!$this->getDoTraits()) {
            return;
        }

        if (empty($this->tc)) {
            $this->tc = new TraitsTaxonController();
        }

        foreach ($this->names as $key => $val) {
            foreach ($this->selectedTraits as $id => $name) {
                list($groupId, $traitId) = explode('-', $id);
                $value = $this->getTraitValueFromMatrix([
                    'group_id' => $groupId,
                    'trait_id' => $traitId,
                    'taxon_id' => $val['_taxon_id'],
                ]);
                $this->names[$key][$name] = $value;
            }
        }
    }

    private function getTraitValueFromMatrix($p)
    {
        $groupId = $p['group_id'] ?? null;
        $traitId = $p['trait_id'] ?? null;
        $taxonId = $p['taxon_id'] ?? null;

        if (is_null($groupId) || is_null($traitId) || is_null($taxonId)) {
            return false;
        }

        return $this->models->VersatileExportModel->getTraitValueFromMatrix([
            'group_id' => $groupId,
            'taxon_id' => $taxonId,
            'trait_id' => $traitId,
            'language_id' => $this->getDefaultProjectLanguage(),
            'project_id' => $this->getCurrentProjectId(),
        ]);
    }

    private function doSynonymsQuery()
    {
        if (!$this->getDoSynonyms()) {
            return;
        }

        // Ruud: restart synonyms for each $names output cycle
        $this->synonyms = [];

        $this->query = "
            SELECT
                _names.id,
                _names.name,
                " . ($this->queryBitNameParts) . "
                " . ($this->hasCol('database_id') ? " _names.id as database_id, " : "") . "
                " . ($this->hasCol('rank') ? " ifnull(_lpr.label,_r.rank) as " . $this->columnHeaders['rank'] . ", " : "") . "
                _b.nametype,
                _c.language,
                _names.taxon_id as _taxon_id,
                _na.addition as remark,
                concat(_lit.author, ', ' , _lit.date) as reference
                    
            FROM
                %PRE%names _names
                    
            left join %PRE%name_types _b
                on _names.project_id=_b.project_id
                and _names.type_id=_b.id
                    
            left join %PRE%languages _c
                on _names.language_id=_c.id
                    
            left join %PRE%projects_ranks _f
                on _names.rank_id=_f.id
                and _names.project_id=_f.project_id
                    
            left join %PRE%ranks _r
                on _f.rank_id=_r.id
                    
            left join %PRE%labels_projects_ranks _lpr
                on _f.project_id=_lpr.project_id
                and _f.id=_lpr.project_rank_id
                and _lpr.language_id = " . LANGUAGE_ID_DUTCH . "
                
            left join %PRE%names_additions _na
                on _names.id = _na.name_id 
                and _names.project_id=_na.project_id
                    
            left join %PRE%literature2 _lit
            	on _names.reference_id = _lit.id
            	and _names.project_id = _lit.project_id 
                    
            where
                _names.project_id=" . $this->getCurrentProjectId() . "
                and _names.type_id in (" . implode(",", (array) $this->getSelectedSynonymTypes()) . ")
                %ID-CLAUSE%
            
            order by
                _names.name
            ";

        $get_all = count((array) $this->names) >= $this->synonymStrategyThreshold;

        if (!$get_all) {
            $this->query = str_replace('%ID-CLAUSE%', 'and _names.taxon_id = %ID%', $this->query);
        } else {
            $all_synonyms = array();
            $q = str_replace('%ID-CLAUSE%', '', $this->query);
            $synonyms = $this->models->Names->freeQuery($q);
            foreach ((array) $synonyms as $key => $val) {
                $all_synonyms[$val['_taxon_id']][] = $val;
            }
            unset($synonyms);
        }

        foreach ($this->names as $key => $val) {
            if (!$get_all) {
                $q = str_replace('%ID%', $val['_taxon_id'], $this->query);
                $synonyms = $this->models->Names->freeQuery($q);
            } else {
                $synonyms = isset($all_synonyms[$val['_taxon_id']]) ? $all_synonyms[$val['_taxon_id']] : array();
            }

            foreach ((array) $synonyms as $row) {
                $tmp = array();

                if ($this->hasCol('name_parts')) {
                    foreach (array_keys($this->getNameParts()) as $name_part) {
                        $tmp[$name_part] = isset($row[$name_part]) ? $row[$name_part] : null;
                    }
                }
                if ($this->hasCol('database_id')) {
                    if (isset($row[$this->columnHeaders['database_id']])) {
                        $tmp[$this->columnHeaders['database_id']] = $row[$this->columnHeaders['database_id']];
                    }
                }

                if ($this->hasCol('rank')) {
                    if (isset($row[$this->columnHeaders['rank']])) {
                        $tmp[$this->columnHeaders['rank']] = $row[$this->columnHeaders['rank']];
                    }
                }

                if (isset($row[$this->columnHeaders['reference']])) {
                    $tmp[$this->columnHeaders['reference']] = $row[$this->columnHeaders['reference']];
                }

                $d = array(
                    $this->columnHeaders['synonym'] => $row['name'] ?? null,
                    $this->columnHeaders['synonym_type'] => $row['nametype'] ?? null,
                    $this->columnHeaders['language'] => $row['language'] ?? null,
                    $this->columnHeaders['taxon'] => $val['scientific_name'] ?? null,
                    $this->columnHeaders['remark'] => $row['remark'] ?? null,
                    $this->columnHeaders['reference'] => $row['reference'] ?? null,
                );

                if (isset($val['nsr_id'])) {
                    $d['taxon_nsr_id'] = $val['nsr_id'];
                }

                $this->synonyms[] = array_merge($d, $tmp);

                unset($d);
            }
        }
    }

    private function doOutputStart()
    {

        if ($this->getOutputTarget() == 'download') {
            $this->openFileHandlers();
            $this->setFileHandler($this->fileHandleNames);
        }

        if ($this->getOutputTarget() == 'screen') {
            header('Content-Type: text/html; charset=utf-8');
            $this->print("<pre>", $this->getNewLine());
        }

        $this->doQueryParametersOutput();
    }

    private function doSynonymsOutputStart()
    {
        $this->doQueryParametersOutput();
    }

    private function doOutputEnd()
    {
        $this->doEOFMarkerOutput();

        if ($this->getOutputTarget() == 'download') {
            $this->closeFileHandlers();
            $this->createZipFile();
            $this->deleteTmpFiles();
        }

        if ($this->getOutputTarget() == 'screen') {
            $this->print("</pre>", $this->getNewLine());
        }
    }


    private function doOutput()
    {
        if (isset($this->spoofSettings->do_spoof_export) && $this->spoofSettings->do_spoof_export) {
            $this->doSpoofOutput();
        } else {
            $this->doNamesOutput();
            $this->doSynonymsOutput();
            $this->doQueriesOutput();
        }
    }

    private function doSpoofOutput()
    {
        $this->doQueryParametersOutput();
        $this->printHeaderLine($this->names);
        $this->printNewLine();
        $this->printNewLine();
        $this->print($this->spoofSettings->texts->download_body);
        $this->printNewLine();

        if ($this->getDoSynonyms()) {
            $this->printNewLine();
            $this->printHeaderLine($this->synonyms);
            $this->printNewLine();
            $this->printNewLine();
            $this->print($this->spoofSettings->texts->download_synonyms);
            $this->printNewLine();
        }
    }

    // Used to switch between two file handlers
    private function setFileHandler($fh)
    {
        $this->fileHandle = $fh;
    }

    private function print($string)
    {
        if ($this->getOutputTarget() == 'screen') {
            echo $string;
        } elseif (!empty($this->fileHandle)) {
            fwrite($this->fileHandle, $string);
        }
    }

    private function openFileHandlers()
    {
        $basePath = sys_get_temp_dir() . '/';

        $this->namesFilePath = $basePath . $this->namesFileName;
        $this->fileHandleNames = fopen($this->namesFilePath, 'w');

        if ($this->getDoSynonyms()) {
            $this->synonymsFilePath = $basePath . $this->synonymsFileName;
            $this->fileHandleSynonyms = fopen($this->synonymsFilePath, 'w');
        }
    }

    private function closeFileHandlers()
    {
        foreach ([$this->fileHandleNames, $this->fileHandleSynonyms] as $fh) {
            if (!empty($fh)) {
                fclose($fh);
            }
        }
    }

    private function printHeaderLine($lines)
    {
        $header_line = array();

        foreach ((array) $lines as $line) {
            if (count((array) $line) > count($header_line)) {
                $header_line = $line;
            }
        }

        // printing headers
        foreach (array_keys((array) $header_line) as $rkey) {
            if ($this->getSuppressUnderscoredFields() && substr($rkey, 0, 1) == '_') {
                continue;
            }

            $this->print($this->getEnclosure());

            $this->print($this->getReplaceUnderscoresInHeaders() ? str_replace('_', ' ', $rkey) : $rkey);

            $this->print($this->getEnclosure());

            $this->print($this->getFieldSep());
        }
    }

    private function printBodyLines($bodyLines)
    {
        // printing lines
        foreach ((array) $bodyLines as $row) {
            foreach ($row as $key => $cell) {
                if (is_int($key)) {
                    continue;
                }

                $cell = ($cell) ? trim(preg_replace('#\R+#', '   ', $cell)) : '';

                if ($this->getSuppressUnderscoredFields() && substr($key, 0, 1) == '_') {
                    continue;
                }

                $this->print($this->getEnclosure());

                $this->print($this->getUtf8ToUtf16() ? mb_convert_encoding($cell, 'utf-16', 'utf-8') : $cell);

                $this->print($this->getEnclosure());

                $this->print($this->getFieldSep());
            }

            $this->print($this->getNewLine());
        }
    }

    private function printNewLine()
    {
        $this->print($this->getNewLine());
    }

    private function getEnclosure()
    {
        return !$this->getNoQuotes() ? $this->getQuoteChar() : "";
    }

    private function doQueryParametersOutput()
    {
        if (!$this->getDoPrintQueryParameters()) {
            return;
        }

        $d = $this->getTaxonById($this->getBranchTopId());
        $this->print($this->translate("top") . $this->getFieldSep() . $this->getEnclosure() . $d['taxon'] . $this->getEnclosure());
        $this->printNewLine();

        $d = array();
        foreach ($this->getRanks() as $val) {
            if (in_array($val["id"], (array) $this->getSelectedRanks())) {
                $d[] = $val["rank"];
            }
        }
        $this->print($this->translate("ranks") . $this->getFieldSep() . $this->getEnclosure() . ($this->getAllRanks() ? $this->translate("(all)") : $this->translate($this->operators[$this->getRankOperator()]['label']) . " " . implode(", ", $d) . $this->getEnclosure()));
        $this->printNewLine();

        $this->print($this->translate("statuses") . $this->getFieldSep() . $this->getEnclosure());

        $p = $this->getPresenceStatusLabels();
        if (!empty($p)) {
            $this->print("(" . implode(",", $this->getPresenceStatusLabels()) . ")");
        } else {
            $this->print($this->translate("(all)"));
        }
        $this->print($this->getEnclosure());
        $this->printNewLine();

        $this->print($this->translate("taxa created") . $this->getFieldSep() . $this->getEnclosure());
        $this->printDateFilterLine($this->firstCreated, $this->dbFirstCreated, $this->lastCreated, $this->dbLastCreated, $this->firstLastCreatedErrors);
        $this->print($this->getEnclosure());
        $this->printNewLine();

        $this->print($this->translate("taxa modified") . $this->getFieldSep() . $this->getEnclosure());
        $this->printDateFilterLine($this->firstModified, $this->dbFirstModified, $this->lastModified, $this->dbLastModified, $this->firstLastModifiedErrors);
        $this->print($this->getEnclosure());
        $this->printNewLine();

        $this->printNewLine();
    }

    private function printDateFilterLine($first, $dbFirst, $last, $dbLast, $errors)
    {
        if ($first > $dbFirst || $last < $dbLast || !empty($errors)) {
            $this->print($first . '-' . $last .
                (!empty($errors) ? ' ( ' . $this->translate('invalid dates have been replaced') . ': ' . implode('; ', $errors) . ')' : ''));
        } else {
            $this->print($this->translate("(no filter)"));
        }
    }

    private function doNamesOutput()
    {
        $this->setFileHandler($this->fileHandleNames);
        // Ruud: print header line only at start (when offset = 0)
        if ($this->offset == 0) {
            $this->printHeaderLine($this->names);
            $this->printNewLine();
        }
        $this->printBodyLines($this->names);
    }

    private function doSynonymsOutput()
    {
        // Ruud: exclude synonyms from screen output
        if (!$this->getDoSynonyms() || $this->getOutputTarget() == 'screen') {
            return;
        }
        $this->setFileHandler($this->fileHandleSynonyms);
        // Ruud: create separate file for synonyms
        if ($this->offset == 0) {
            $this->doSynonymsOutputStart();
            $this->printHeaderLine($this->synonyms);
            $this->printNewLine();
        }
        $this->printBodyLines($this->synonyms);
    }

    private function doQueriesOutput()
    {
        if (!$this->getPrintQuery()) {
            return;
        }

        $this->printNewLine();
        $this->printNewLine();

        foreach ((array) $this->queries as $key => $val) {
            $this->print("query:", $key, str_repeat('-', 60));
            $this->printNewLine();
            $this->print($val);
            $this->printNewLine();
        }
        $this->printNewLine();
    }

    private function setBranchTopId($branchTopId)
    {
        $this->branchTopId = $branchTopId;
    }

    private function getBranchTopId()
    {
        return $this->branchTopId;
    }

    private function setPresenceStatusLabels($presenceLabels)
    {
        $this->presenceLabels = $presenceLabels;
    }

    private function getPresenceStatusLabels()
    {
        return $this->presenceLabels;
    }

    private function setSelectedRanks($selectedRanks)
    {
        $this->selectedRanks = !is_null($selectedRanks) ? array_unique($selectedRanks) : null;
    }

    private function getSelectedRanks()
    {
        return $this->selectedRanks;
    }

    private function setAllRanks($allRanks)
    {
        $this->allRanks = isset($allRanks) && $allRanks == 'on';
    }

    private function getAllRanks()
    {
        return $this->allRanks;
    }

    private function setRankOperator($rankOperator)
    {
        $this->rankOperator = $rankOperator;
    }

    private function getRankOperator()
    {
        return $this->rankOperator;
    }

    private function setCols($cols)
    {
        $this->cols = $cols;
    }

    private function setOrderBy($order)
    {
        if ($order == 'rank-sci_name' && $this->hasCol('rank') && $this->hasCol('sci_name')) {
            $this->orderBy = '_r.id,_t.taxon';
        } elseif ($order == 'rank-dutch_name' && $this->hasCol('rank') && $this->hasCol('dutch_name')) {
            $this->orderBy = '_r.id,_z.name';
        } elseif ($order == 'sci_name' && $this->hasCol('sci_name')) {
            $this->orderBy = '_t.taxon';
        } elseif ($order == 'dutch_name' && $this->hasCol('dutch_name')) {
            $this->orderBy = '_z.name';
        } elseif ($order == 'presence_status-sci_name' && $this->hasCol('presence_status') && $this->hasCol('sci_name')) {
            $this->orderBy = '_h.index_label,_t.taxon';
        } elseif ($order == 'presence_status-dutch_name' && $this->hasCol('presence_status') && $this->hasCol('dutch_name')) {
            $this->orderBy = '_h.index_label,_z.name';
        }
    }

    private function getOrderBy()
    {
        return $this->orderBy;
    }

    private function hasCol($col)
    {
        return isset($this->cols[$col]) && $this->cols[$col] == 'on';
    }

    private function setNameParts($nameParts)
    {
        $this->nameParts = $nameParts;
    }

    private function getNameParts()
    {
        return $this->nameParts;
    }

    private function setAncestors($ancestors)
    {
        $this->ancestors = $ancestors;
    }

    private function getAncestors()
    {
        return $this->ancestors;
    }

    private function setDoSynonyms($state)
    {
        $this->doSynonyms = isset($state) && $state == 'on';
    }

    private function getDoSynonyms()
    {
        return $this->doSynonyms;
    }

    private function getDoTraits()
    {
        return !empty($this->selectedTraits);
    }

    private function setDoHybridMarker($state)
    {
        $this->doHybridMarker = isset($state) && $state == 'on';
    }

    private function getDoHybridMarker()
    {
        return $this->doHybridMarker;
    }

    private function setSelectedSynonymTypes($selectedSynonymTypes)
    {
        $this->selectedSynonymTypes = $selectedSynonymTypes;
    }

    private function getSelectedSynonymTypes()
    {
        return $this->selectedSynonymTypes;
    }

    private function setFieldSep($fieldSep)
    {
        $fieldSep = ($fieldSep == 'tab' ? "\t" : ($fieldSep == 'comma' ? "," : $fieldSep));
        $this->fieldSep = $fieldSep;
    }

    private function getFieldSep()
    {
        return $this->fieldSep;
    }

    private function setNewLine($newLine)
    {
        $newLine = ($newLine == "CrLf" ? chr(13) . chr(10) : ($newLine == "Cr" ? chr(13) : chr(10)));
        $this->newLine = $newLine;
    }

    private function getNewLine()
    {
        return $this->newLine;
    }

    private function setNoQuotes($state)
    {
        $this->noQuotes = isset($state) && $state == 'on';
    }

    private function getNoQuotes()
    {
        return $this->noQuotes;
    }

    private function getQuoteChar()
    {
        return $this->quoteChar;
    }

    private function setReplaceUnderscoresInHeaders($state)
    {
        $this->replaceUnderscoresInHeaders = isset($state) && $state == 'on';
    }

    private function getReplaceUnderscoresInHeaders()
    {
        return $this->replaceUnderscoresInHeaders;
    }

    private function setDoPrintQueryParameters($state)
    {
        $this->doPrintQueryParameters = isset($state) && $state == 'on';
    }

    private function getDoPrintQueryParameters()
    {
        return $this->doPrintQueryParameters;
    }

    private function setPrintEOFMarker($state)
    {
        $this->printEOFMarker = isset($state) && $state == 'on';
    }

    private function getPrintEOFMarker()
    {
        return $this->printEOFMarker;
    }

    private function doEOFMarkerOutput()
    {
        if ($this->getPrintEOFMarker()) {
            $this->print($this->endOfFileMarker);
        }
    }

    private function setOutputTarget($target)
    {
        $this->outputTarget = $target;
    }

    private function getOutputTarget()
    {
        return $this->outputTarget;
    }

    private function getUtf8ToUtf16()
    {
        return $this->utf8ToUtf16;
    }

    private function setKeepTags($state)
    {
        $this->keepTags = isset($state) && $state == 'on';
    }

    private function getKeepTags()
    {
        return $this->keepTags;
    }

    private function setPrintQuery($state)
    {
        $this->printQuery = $state;
    }

    private function getPrintQuery()
    {
        return $this->printQuery;
    }

    private function getSuppressUnderscoredFields()
    {
        return $this->suppressUnderscoredFields;
    }

    private function setBranchTopSession($top)
    {
        if (is_null($top)) {
            unset($_SESSION['admin']['user']['export']['selected_branch_top']);
        } else {
            $_SESSION['admin']['user']['export']['selected_branch_top'] = array('id' => $top[0], 'label' => $top[1]);
        }
    }

    private function getBranchTopSession()
    {
        return isset($_SESSION['admin']['user']['export']['selected_branch_top']) ? $_SESSION['admin']['user']['export']['selected_branch_top'] : null;
    }

    private function createZipFile()
    {
        $this->helpers->ZipFile->createArchive(str_replace('.csv', '', $this->namesFileName));

        $this->helpers->ZipFile->addFile($this->namesFilePath, $this->namesFileName);


        if ($this->getDoSynonyms()) {
            $this->helpers->ZipFile->addFile($this->synonymsFilePath, $this->synonymsFileName);
        }

        $this->helpers->ZipFile->downloadArchive();
    }

    private function deleteTmpFiles()
    {
        unlink($this->namesFilePath);

        if ($this->getDoSynonyms()) {
            unlink($this->synonymsFilePath);
        }
    }

    private function emptyTraitsIndex()
    {
        $this->models->VersatileExportModel->emptyTraitsIndex();
    }

    public function createTraitsIndex($batchSize = 5000)
    {
        $this->emptyTraitsIndex();

        // Fixed values
        $total = count($this->models->VersatileExportModel->getTaxaTraitsFixedValues([
            'project_id' => $this->getCurrentProjectId(),
        ]));
        for ($offset = 0; $offset <= $total; $offset += $batchSize) {
            $values = $this->models->VersatileExportModel->getTaxaTraitsFixedValues([
                'project_id' => $this->getCurrentProjectId(),
                'offset' => $offset,
                'limit' => $batchSize
            ]);
            $this->saveTaxonTraitValues($values, 'fixed');
        }

        // Free values
        $total = count($this->models->VersatileExportModel->getTaxaTraitsFreeValues([
            'project_id' => $this->getCurrentProjectId(),
        ]));
        for ($offset = 0; $offset <= $total; $offset += $batchSize) {
            $values = $this->models->VersatileExportModel->getTaxaTraitsFreeValues([
                'project_id' => $this->getCurrentProjectId(),
                'offset' => $offset,
                'limit' => $batchSize
            ]);
            $this->saveTaxonTraitValues($values, 'free');
        }

        $dt = $this->models->VersatileExportModel->getIndexLastUpdate();
        return $this->formatDateTime($dt);
    }

    private function saveTaxonTraitValues($values, $type)
    {
        $method = 'getTaxonTraits' . ucfirst(strtolower($type)) . 'Values';

        foreach ($values as $row) {
            $data = [
                'language_id' => $this->getDefaultProjectLanguage(),
                'project_id' => $this->getCurrentProjectId(),
                'taxon_id' => $row['taxon_id']
            ];
            $traits = [];
            foreach (explode(',', $row['trait_ids']) as $traitId) {
                // Simplified query for values used
                $taxonValues = $this->models->VersatileExportModel->{$method}([
                    'project_id' => $this->getCurrentProjectId(),
                    'language_id' => $this->getDefaultProjectLanguage(),
                    'taxon_id' => $row['taxon_id'],
                    'trait_id' => $traitId,
                ]);

                if (!empty($taxonValues)) {
                    // Values have not been formatted yet
                    $taxonValues = $this->tc->formatTraitsTaxonValues($taxonValues);
                    $v = [];
                    foreach ($taxonValues[0]['values'] as $value) {
                        $v[] = $value['value_start'] . (!empty($value['value_end']) ? '-' . $value['value_end'] : '');
                    }
                    $group = $this->traitGroupLookup($traitId);
                    if (!isset($group['id'])) {
                        $this->log('Could not retrieve group for trait ' . $traitId, 2);
                        continue;
                    }
                    $traits[] = [
                        'trait_group_id' => $group['id'],
                        'trait_group_name' => $group['name'],
                        'trait_id' => $traitId,
                        'trait_name' => $taxonValues[0]['trait']['name'],
                        'trait_value' => implode('|', $v),
                    ];
                } else {
                    $this->log('Could not retrieve export values for trait ' . $traitId, 2);
                    return false;
                }
            }
            $data['traits'] = $traits;
            $this->models->VersatileExportModel->saveTaxonTraitValues($data);
        }
    }

    private function traitGroupLookup($traitId)
    {
        if (!isset($this->traitGroupLookup[$traitId])) {
            $trait = $this->tc->getTraitGroupTrait(['trait' => $traitId]);
            // Break on empty traits or incomplete group info (caused crash when updating index)
            if (empty($trait)) {
                return false;
            }
            $group = $this->tc->getTraitgroup($trait['trait_group_id']);
            if (!isset($group['id'])) {
                return false;
            }

            $this->traitGroupLookup[$traitId] = [
                'id' => $group['id'],
                'name' => isset($group['names'][$this->getDefaultProjectLanguage()]) ?
                    $group['names'][$this->getDefaultProjectLanguage()] :
                    $group['sysname']
            ];
        }
        return $this->traitGroupLookup[$traitId];
    }
}
