<?php

namespace Linnaeus\Admin\Controllers;

use stdClass;
use Linnaeus\Admin\Controllers\Controller;
use Linnaeus\Admin\Controllers\ModuleSettingsReaderController;
use Linnaeus\Admin\Controllers\LoginController;
use Linnaeus\Admin\Controllers\ProjectsController;

/**
 *  The webservices controller handles the Webservices api
 */

class WebservicesController extends Controller
{
    public $controllerPublicName = 'Webservices';

    private $projectId;
    private $project;

    private $actionData = array();
    private $head;

    private $jsonPCallback = false;
    private $json = null;

    public $usedHelpers = array(
        'http_basic_authentication'
    );

    public $extraModels = array(
        'ProjectsModel',
        'UsersModel'
    );

    private $services = array(
        'projects.php' => array(
            'description' => 'all projects on this server'
        ),
        'project_users.php' => array(
            'description' => 'all users in a project',
            'parameters' => array(
                'pid' => array('mandatory' => true, 'description' => 'project ID')
            )
        ),
        'users.php' => array(
            'description' => 'all users on this server'
        ),
    );

    private $generalParameters = array('callback' => array('mandatory' => false, 'description' => 'name of JSONP callback function'));

    /**
     * WebservicesController constructor.
     *
     * @param null $p
     */
    public function __construct($p = null)
    {
        parent::__construct($p);
        $this->initialise();
    }

    /**
     * WebservicesController destructor.
     */
    public function __destruct()
    {
        parent::__destruct();
    }


    /**
     * Show the index page
     */
    public function indexAction()
    {
        $this->authenticateUser();
        $this->smarty->assign('base_url', 'https://' . '%AUTH%' . $_SERVER['HTTP_HOST'] . pathinfo($_SERVER['PHP_SELF'])['dirname'] . '/');
        $this->smarty->assign('services', $this->services);
        $this->smarty->assign('general_parameters', $this->generalParameters);

        $this->printPage();
    }


    /**
     * Show the projects page
     */
    public function projectsAction()
    {
        $this->authenticateUser();
        $this->head->service = 'projects';
        $this->head->description = 'list of all projects on this server';

        foreach ($this->models->ProjectsModel->getUserProjects(array('user_id' => $this->getCurrentUserId(), 'show_all' => true)) as $project) {
            $d = new stdClass();

            $d->id = $project['id'];
            $d->sys_name = $project['sys_name'];
            $d->sys_description = $project['sys_description'];
            $d->title = $project['title'];
            $d->published = $project['published'];

            $this->actionData[] = $d;
        }

        header('Content-Type: application/json');
        $this->printOutput();
    }

    /**
     * Show the project users page
     */
    public function projectUsersAction()
    {
        $this->authenticateUser();
        $this->authenticateProject();
        $this->head->service = 'project_users';
        $this->head->description = sprintf('list of all users in project "%s"', $this->project['sys_name']);
        $data = $this->models->UsersModel->getProjectUsers(array('project_id' => $this->getCurrentProjectId()));
        array_walk($data, function (&$a) {
            unset($a['password']);
        });
        $this->actionData = $data;
        $this->printOutput();
    }

    /**
     * Show the users page
     */
    public function usersAction()
    {
        $this->authenticateUser();
        $this->head->service = 'users';
        $this->head->description = sprintf('list of all users');
        $data = $this->models->UsersModel->getAllUsers();
        array_walk($data, function (&$a) {
            unset($a['password']);
        });
        $this->actionData = $data;
        $this->printOutput();
    }

    /**
     * Set project Id
     */
    private function setProjectId()
    {
        $this->projectId = $this->rGetVal('pid');
    }

    /**
     * Get project Id
     */
    private function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Initialise the web services controller
     */
    private function initialise()
    {
        $this->setProjectId();
        $this->checkJSONPCallback();

        $this->head = new stdClass();
    }

    /**
     * Set the Project
     */
    private function setProject()
    {
        $d = $this->models->ProjectsModel->getUserProjects(array('user_id' => $this->getCurrentUserId(), 'show_all' => true, 'project_id' => $this->getProjectId()));
        $this->project = $d[0];
    }

    /**
     * Authenticate User
     */
    private function authenticateUser()
    {
        $this->loginController = new LoginController();

        $this->helpers->HttpBasicAuthentication->setVerificationCallback(
            function ($username, $password) {
                return $this->loginController->loginUser(array('username' => $username, 'password' => $password));
            }
        );

        $this->helpers->HttpBasicAuthentication->authenticate() or die('not authorized');
    }

    /**
     * Authenticate Project
     */
    private function authenticateProject()
    {
        if (is_null($this->getProjectId())) {
            die('no project id');
        }

        $this->head->projectId = $this->getProjectId();
        $this->setProject();

        if (!$this->project) {
            die('unknown project id');
        }

        $this->projectsController = new ProjectsController();
        $this->projectsController->doChooseProject($this->getProjectId());
        $this->UserRights->setRequiredLevel(ID_ROLE_SYS_ADMIN);

        if (!$this->getAuthorisationState()) {
            die('user not authorized for project');
        }
    }

    /**
     * Print the result in json
     */
    private function printOutput()
    {
        $this->_server = new stdClass();
        $this->_server->address = $_SERVER['SERVER_ADDR'];
        $this->_server->name = $_SERVER['SERVER_NAME'];
        $this->_server->host_name = $_SERVER['HTTP_HOST'];

        $this->json = json_encode(array_merge((array) $this->head, array("results" => (array) $this->actionData, (array) $this->_server)));

        if ($this->hasJSONPCallback()) {
            $this->json = $this->getJSONPCallback() . '(' . $this->json . ');';
        }

        header('Content-Type: application/json');

        echo $this->json;
    }

    /**
     *  Check if jsonp callback
     */
    private function checkJSONPCallback()
    {
        if ($this->rHasVal('callback')) {
            $this->setJSONPCallback($this->rGetVal('callback'));
        }
    }

    /**
     *  Set callback
     */
    private function setJSONPCallback($callback)
    {
        $this->jsonPCallback = $callback;
    }

    /**
     *  Get callback
     */
    private function getJSONPCallback()
    {
        return $this->jsonPCallback;
    }

    /**
     *  Check callback
     */
    private function hasJSONPCallback()
    {
        return $this->getJSONPCallback() != false;
    }
}
