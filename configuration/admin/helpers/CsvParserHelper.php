<?php

namespace Linnaeus\Admin\Helpers;

class CsvParserHelper
{
    private $file;
    private $results;
    private $errors;
    private $delimiter = ',';
    private $enclosure = '"';
    private $rawLineEnd = PHP_EOL;
    private $lineMax = false;
    private $fieldMax = false;
    private $dropAllWhites = true;
    private $trimFields = true;
    private $maxLineLength = 1000;
    private $oldADLEsetting;
    private $rawData = null;
    private $stripBOM = true;

    private $allowedDelimiters = array(',', ';', "\t");
    private $allowedEnclosures = array('"', "'", false);

    public function __construct()
    {
        $this->oldADLEsetting = ini_get('auto_detect_line_endings');
        ini_set('auto_detect_line_endings', true);
    }

    public function __destruct()
    {
        ini_set('auto_detect_line_endings', $this->oldADLEsetting);
    }

    public function parseFile($file)
    {
        $this->file = $file;
        if ($this->testIfCSV()) {
            $this->readData();
            $this->postProcessData();
        } else {
            $this->addError(_('File does not seem to be a CSV-file.'));
        }
    }

    public function parseRawData($raw)
    {
        $this->setRawData($raw);
        if ($this->testIfCSV(true)) {
            $this->readRawData();
            $this->postProcessData();
        } else {
            $this->addError(_('File does not seem to be a CSV-file.'));
        }
    }

    public function setFieldEnclosure($enclosure = false)
    {
        if (in_array($enclosure, $this->allowedEnclosures)) {
            $this->enclosure = $enclosure;
        }
    }

    public function setFieldDelimiter($delimiter = false)
    {
        if (in_array($delimiter, $this->allowedDelimiters)) {
            $this->delimiter = $delimiter;
        }
    }

    public function setMaxLineLength($length)
    {
        if (is_numeric($length) && $length > 0) {
            $this->maxLineLength = $length;
        }
    }

    public function getMaxLineLength()
    {
        return $this->maxLineLength;
    }

    public function setFieldMax($num)
    {
        $this->fieldMax = $num;
    }

    public function setDropAllWhites($mode)
    {
        $this->dropAllWhites = $mode;
    }

    public function setTrimFields($mode)
    {
        $this->trimFields = $mode;
    }

    public function setRawLineEnd($char)
    {
        $this->rawLineEnd = $char;
    }

    public function getRawLineEnd()
    {
        return $this->rawLineEnd;
    }

    public function setRawData($data)
    {
        $this->rawData = $data;
    }

    public function getRawData()
    {
        return $this->rawData;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getResults()
    {
        return $this->results;
    }

    public function setStripBOM($state)
    {
        $this->stripBOM = $state;
    }

    private function testIfCSV($useRaw = false)
    {
        $b = null;

        try {
            if (!$useRaw) {
                $handle = fopen($this->file, "r");
                if ($handle) {
                    $b = fgets($handle, 1000);
                    fclose($handle);
                }
            } else {
                $d = explode($this->getRawLineEnd(), $this->getRawData());
                $b = $d[0];
            }

            $is_text = true;
            $bLength = strlen($b);
            for ($i = 0; $i < $bLength; $i++) {
                $v = substr($b, $i, 1);
                if (ord($v) == 0) {
                    $is_text = false;
                    break;
                }
            }

            return $is_text;
        } catch (\Exception $e) {
            print $e->getMessage();
        }
    }

    private function readData()
    {
        $handle = fopen($this->file, 'r');
        if ($handle !== false) {
            $firstLine = true;
            // fgetcsv does not allow for enclosure being absent
            if ($this->enclosure == false) {
                while (($data = fgets($handle, $this->getMaxLineLength())) !== false) {
                    if ($firstLine && $this->stripBOM) {
                        $data[0] = ltrim($data[0], chr(239) . chr(187) . chr(191));
                        $firstLine = false;
                    }

                    /*
                        wish we had str_getcsv, but the specs say stick to PHPv5.2
                        anyway, since there is no enclosure we can safely assume *every* delimiter is
                        an actual delimiter and not part of the data, so... bombs away!

                        ps: great, even when $this->_enclosure is defined, its actual presence in the
                        csv-file is totally optional, making this entire piece of code totally unnecessary. drat.
                    */
                    $this->results[] = explode($this->delimiter, $data);

                    if ($this->lineMax && count((array) $this->results) >= $this->lineMax) {
                        break;
                    }
                }
            } else {
                while (($data = fgetcsv($handle, $this->getMaxLineLength(), $this->delimiter, $this->enclosure)) !== false) {
                    if ($firstLine && $this->stripBOM) {
                        $data[0] = ltrim($data[0], chr(239) . chr(187) . chr(191));
                        $firstLine = false;
                    }

                    $this->results[] = $data;
                    if ($this->lineMax && count((array) $this->results) >= $this->lineMax) {
                        break;
                    }
                }
            }

            fclose($handle);
        } else {
            $this->addError(_('Could not read data from file.'));
        }
    }

    private function readRawData()
    {
        $d = explode($this->getRawLineEnd(), $this->getRawData());

        if ($this->enclosure == false) {
            foreach ((array) $d as $data) {
                $this->results[] = explode($this->delimiter, $data);

                if ($this->lineMax && count((array) $this->results) >= $this->lineMax) {
                    break;
                }
            }
        } else {
            foreach ((array) $d as $data) {
                $data = str_getcsv($data, $this->delimiter, $this->enclosure);

                $this->results[] = $data;
                if ($this->lineMax && count((array) $this->results) >= $this->lineMax) {
                    break;
                }
            }
        }
        $this->postProcessData();
    }

    private function postProcessData()
    {
        if ($this->fieldMax) {
            foreach ((array) $this->results as $key => $val) {
                $this->results[$key] = array_slice($val, 0, $this->fieldMax);
            }
        }

        if ($this->dropAllWhites) {
            foreach ((array) $this->results as $key => $val) {
                $allWhite = true;
                foreach ((array) $val as $resultskey => $result) {
                    if (strlen(trim($result)) > 0) {
                        $allWhite = false;
                    }

                    if ($this->trimFields) {
                        $val[$resultskey] = trim($result);
                    }
                }

                if (!$allWhite) {
                    $d[] = $val;
                }
            }
            $this->results = $d;
        }
    }

    private function addError($e)
    {
        $this->errors[] = $e;
    }
}
