<?php

namespace Linnaeus\Admin\Helpers;

define('KILOBYTE', 1024);

class HrFilesizeHelper
{
    public function returnBytes($sizeString)
    {
        switch (substr($sizeString, -1)) {
            case 'G':
            case 'g':
                return intval($sizeString) * (KILOBYTE * KILOBYTE * KILOBYTE);
            case 'M':
            case 'm':
                return intval($sizeString) * (KILOBYTE * KILOBYTE);
            case 'K':
            case 'k':
                return intval($sizeString) * KILOBYTE;
            default:
                return intval($sizeString);
        }
    }
}
