<?php

namespace Linnaeus\Admin\Helpers;

class HttpBasicAuthentication
{
    /*

        $this->helpers->HttpBasicAuthentication->setVerificationCallback(
            function($username,$password)
            {
                return $this->verifyCredentials($username,$password);
            }
        );

        $this->helpers->HttpBasicAuthentication->authenticate() or die('not authorized');

    */


    private $realm = 'realm';
    private $phpAuthUser;
    private $phpAuthPass;
    private $verificationCallback;

    public function authenticate()
    {
        $this->setServerVars();

        if (!isset($this->phpAuthUser)) {
            $this->sendAuthHeaders();
            return false;
        } else {
            return $this->verifyCredentials();
        }
    }

    public function setRealm($realm)
    {
        $this->realm = $realm;
    }

    public function setVerificationCallback($callback)
    {
        $this->verificationCallback = $callback;
    }

    private function verifyCredentials()
    {
        /*
            requires a callback that takes two parameters, username and password,
            and that return true or false.
        */
        if (isset($this->verificationCallback)) {
            return call_user_func($this->verificationCallback, $this->phpAuthUser, $this->phpAuthPass);
        } else {
            return !empty($this->phpAuthUser) && !empty($this->phpAuthPass);
        }
    }

    private function setServerVars()
    {
        $this->phpAuthUser = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : null;
        $this->phpAuthPass = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : null;
    }

    private function sendAuthHeaders()
    {
        header('WWW-Authenticate: Basic realm="' . $this->realm . '"');
        header('HTTP/1.0 401 Unauthorized');
    }
}
