<?php

namespace Linnaeus\Admin\Helpers;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class LoggingHelper
{
    private $logger = null;
    private $levels = array(
        0 => Logger::INFO,    // not an error
        1 => Logger::WARNING, // non-fatal error
        2 => Logger::ERROR    // fatal error
    );

    public function __construct()
    {
        $this->logger = new Logger('admin');
        $level = getEnv('DEBUG') ? Logger::INFO : Logger::WARNING;
        $stream = new StreamHandler('php://stdout', $level);
        $this->logger->pushHandler($stream);
    }

    public function log($msg, $severity = 0, $source = false)
    {
        $level = isset($this->levels[$severity]) ? $this->levels[$severity] : Logger::ERROR;
        if ($source) {
            $msg = $msg . " - " . $source;
        }
        return $this->write($msg, $level);
    }

    public function write($msg, $level)
    {
        if (is_array($msg)) {
            foreach ((array) $msg as $val) {
                $this->logger->log($level, trim(preg_replace("/\s+/", " ", $val)));
            }
        } else {
            $this->logger->log($level, trim(preg_replace("/\s+/", " ", $msg)));
        }

        return true;
    }
}
