<?php

namespace Linnaeus\Admin\Helpers;

class PasswordEncoder
{
    private $forceMd5 = false;
    private $password;
    private $hash;

    public function setForceMd5($state)
    {
        if (!is_bool($state)) {
            return;
        }
        $this->forceMd5 = $state;
    }

    public function setPassword($password)
    {
        if (empty($password)) {
            return;
        }
        $this->password = $password;
    }

    public function encodePassword()
    {
        if ($this->forceMd5) {
            $this->setHash(md5($this->getPassword()));
        } else {
            $this->setHash(password_hash($this->getPassword(), PASSWORD_DEFAULT));
        }
    }

    public function getHash()
    {
        return $this->hash;
    }

    private function setHash($hash)
    {
        $this->hash = $hash;
    }

    private function getPassword()
    {
        return $this->password;
    }
}
