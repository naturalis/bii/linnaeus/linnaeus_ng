<?php

namespace Linnaeus\Admin\Helpers;

use SimpleXMLElement;

class SimpleXMLExtended extends SimpleXMLElement
{
    // http://coffeerings.posterous.com/php-simplexml-and-cdata
    public function addCData($text)
    {
        $node = dom_import_simplexml($this);
        $ownerDocument = $node->ownerDocument;
        $node->appendChild($ownerDocument->createCDATASection($text));
    }
}
