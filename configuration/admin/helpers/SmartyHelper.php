<?php

namespace Linnaeus\Admin\Helpers;

use Smarty;

/**
 * When using PHP 8.1+ and Smarty 4.3, deprecated warnings such as this started appearing in the logs:
 *
 * NOTICE: PHP message: PHP Deprecated:  Using php-function "array_merge" as a modifier is deprecated and will be removed in a future release. Use Smarty::registerPlugin to explicitly register a custom modifier. in /var/www/vendor/smarty/smarty/libs/sysplugins/smarty_internal_compile_private_modifier.php on line 114
 *
 * In order to tackle these, the Smarty initialision was moved to a dedicated helper rather
 * than being handled in the main Controller. Basically, it's sufficient to register the default
 * PHP function with the function used in the template. However, with PHP 8.1 being more picky
 * than 7.4, this resulted in more serious errors if e.g. null was passed where a string was
 * expected. The solution is to create "safe" callbacks for those problematic functions.
 *
 * The list of internal PHP functions being used in the templates is stored in `$smartyModifiers`.
 * This list may be incomplete. In most cases, it's enough to add the function to the list.
 * If warnings or errors still occur, you need to add a "safe" callback method. This can be mapped
 * to the default PHP function in the `initialize` method.
 */

class SmartyHelper
{
    private $smarty;
    private $settings;
    private $projectUrl;
    private $translator;
    private $smartyModifiers = [
        'addslashes',
        'array_merge',
        'chr',
        'count',
        'end',
        'htmlentities',
        'implode',
        'is_array',
        'json_encode',
        'mb_strtolower',
        'ord',
        'rand',
        'sprintf',
        'strip_tags',
        'strpos',
        'strstr',
        'strtolower',
        'strtoupper',
        'substr',
        'trim',
        'ucfirst',
        'ucwords',
        'urlencode',
    ];

    public function __construct($settings)
    {
        $this->smarty = new Smarty();
        $this->settings = $settings;
    }

    public function __call($name, $args)
    {
        return $this->smarty->$name($args[0], $args[1] ?? null);
    }

    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }

    public function setProjectUrl($url)
    {
        $this->projectUrl = $url;
    }

    public function initialize()
    {
        $this->smarty->compile_dir = $this->settings['dir_compile'];
        $this->smarty->cache_dir = $this->settings['dir_cache'];
        $this->smarty->config_dir = $this->settings['dir_config'];
        $this->smarty->compile_check = $this->settings['compile_check'];
        $this->smarty->registerPlugin("block", "t", [$this, "translate"]);

        // Never switch caching on, it makes the application unstable!
        $this->smarty->caching = 0;
        $this->smarty->force_compile = (bool)getenv('DEBUG');
        if (getenv('SMARTY_WARNINGS')) {
            $this->smarty->error_reporting = E_ALL;
        } else {
            $this->smarty->error_reporting = E_ALL & ~(E_WARNING | E_NOTICE | E_STRICT);
        }

        // Add "safe" callbacks; when no safe option is provided, the vanilla PHP function is used.
        foreach ($this->smartyModifiers as $modifier) {
            switch ($modifier) {
                case 'count':
                    $callback = [$this, 'safeCount'];
                    break;
                case 'implode':
                    $callback = [$this, 'safeImplode'];
                    break;
                case 'strpos':
                    $callback = [$this, 'safeStrpos'];
                    break;
                case 'stripos':
                    $callback = [$this, 'safeStripos'];
                    break;
                case 'strtolower':
                    $callback = [$this, 'safeStrtolower'];
                    break;
                default:
                    $callback = $modifier;
            }
            $this->smarty->registerPlugin('modifier', $modifier, $callback);
        }

        return $this->smarty;
    }

    public function safeCount($array)
    {
        return is_array($array) ? count($array) : 0;
    }

    public function safeStrpos($haystack, $needle)
    {
        return strpos((string)$haystack, (string)$needle);
    }
    public function safeStripos($haystack, $needle)
    {
        return stripos((string)$haystack, (string)$needle);
    }

    public function safeStrtolower($string)
    {
        return strtolower((string)$string);
    }

    public function safeImplode($glue, $array)
    {
        return implode((string)$glue, (array)$array);
    }

    public function setTemplateDir($controllerBase)
    {
        $this->smarty->template_dir = $this->settings['dir_template'] . '/' . (isset($controllerBase) ? $controllerBase . '/' : '');
    }

    /**
     * Translate a string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function translate($params, $content, &$smarty, &$repeat)
    {
        if (!$this->translator) {
            return $content;
        }

        $c = $this->translator->translate($content) ?? '';

        if (isset($params)) {
            foreach ((array)$params as $key => $val) {
                if (substr($key, 0, 2) == '_s' && isset($val)) {
                    $c = preg_replace('/\%s/', $val, $c, 1);
                }
            }
        }
        return $c;
    }
}
