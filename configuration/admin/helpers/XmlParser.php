<?php

namespace Linnaeus\Admin\Helpers;

use XMLReader;

class XmlParser
{
    private $fileName = null;
    private $nodeName = null;
    private $getSingleNode = false;
    private $callbackFunction = null;
    private $doReturnValues = false;

    public function setFileName($fileName)
    {

        $this->fileName = $fileName;
    }

    public function setDoReturnValues($state)
    {

        if (is_bool($state)) {
            $this->doReturnValues = $state;
        }
    }

    public function setCallbackFunction($function)
    {

        if (is_callable($function)) {
            $this->callbackFunction = $function;

            $this->setDoReturnValues(false);
        }
    }

    public function getNode($name)
    {

        $this->getSingleNode = true;

        return $this->getNodes($name);
    }

    public function getNodes($name)
    {

        if (!isset($name)) {
            return null;
        }

        $this->setNodeName($name);

        $d = new XMLReader();

        $r = array();

        if ($d::open($this->fileName)) {
            while ($d->read() && $d->name !== $this->nodeName)
                ;

            while ($d->name === $this->nodeName) {
                $fixedNode = $this->fixNode($d->readOuterXML());

                libxml_use_internal_errors(true);

                $xml = simplexml_load_string($fixedNode);

                if ($xml === false) {
                    echo '<p><b>XML-parser failed</b></p><p>simplexml_load_string() returned the following error:<br/>';

                    foreach (libxml_get_errors() as $error) {
                        echo '&#149; "' . $error->message . '" in line ' . $error->line . ' at column ' . $error->column . '<br />';
                    }

                    die('</p><p>(abnormal program termination)</p>');
                } else {
                    if (isset($this->callbackFunction)) {
                        call_user_func($this->callbackFunction, $xml, $this->nodeName);
                    }

                    if ($this->getSingleNode === true) {
                        if ($this->doReturnValues === true) {
                            return $xml;
                        } else {
                            return;
                        }
                    }

                    if ($this->doReturnValues === true) {
                        $r[] = $xml;
                    }

                    $d->next($this->nodeName);
                }
            }
        }

        if ($this->doReturnValues === true) {
            return $r;
        }
    }


    private function setNodeName($name)
    {

        $this->nodeName = $name;
    }

    private function fixNode($str)
    {

        return $this->fixTags(html_entity_decode($str, ENT_QUOTES, "utf-8"));
    }

    private function fixTags($str)
    {

        $str = htmlspecialchars($str);

        $find = array(
            "/=/",
            "/&quot;/",
            "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\
]?)(\/|)&gt;/i",
            "/=\"\"/"
        );

        $replace = array(
            "=\"\"",
            "&quot;",
            "<$1$2$3$4$5$6$7$8$9$10>",
            "="
        );

        $str = preg_replace($find, $replace, $str);

        $str = trim(str_replace(array(PHP_EOL, "\t"), '', $str));

        return $str;
    }
}
