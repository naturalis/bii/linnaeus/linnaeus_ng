<?php

namespace Linnaeus\Admin\Helpers;

use ZipArchive;

class ZipFile
{
    private $tmpfile;
    private $zip;
    private $filename = 'archive';

    public function __construct()
    {
    }

    public function createArchive($filename = null)
    {
        $this->tmpfile = tempnam("tmp", "zip");
        $this->zip = new ZipArchive();
        $this->zip->open($this->tmpfile, \ZipArchive::OVERWRITE);
        if (!is_null($filename)) {
            $this->setFileName($filename);
        }
    }

    public function addFile($fullpath, $localname = null)
    {
        if ($localname) {
            $this->zip->addFile($fullpath, $localname);
        } else {
            $this->zip->addFile($fullpath);
        }
    }

    public function downloadArchive()
    {
        $this->zip->close();
        header('Content-Type: application/zip');
        header('Content-Length: ' . filesize($this->tmpfile));
        header('Content-Disposition: attachment; filename="' . $this->filename . '.zip"');
        readfile($this->tmpfile);
        unlink($this->tmpfile);
    }

    private function setFileName($filename)
    {
        $this->filename = $filename;
        $this->filename = strtolower(substr($this->filename, -4)) == ".zip" ? substr($this->filename, 0, -4) : $this->filename;
    }
}
