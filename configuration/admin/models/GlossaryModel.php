<?php

namespace Linnaeus\Admin\Models;

use Linnaeus\Admin\Models\AbstractModel;

final class GlossaryModel extends AbstractModel
{
    public function getSynonyms($params)
    {
        if (!$params) {
            return false;
        }

        $search = isset($params['search']) ? $params['search'] : false;
        $projectId = isset($params['projectId']) ? $params['projectId'] : false;

        $query = 'select distinct glossary_id from %PRE%glossary_synonyms
			 where synonym like "%' . mysqli_real_escape_string($this->databaseConnection, $search) . '%"
			 and project_id = ' . $projectId;

        return $this->freeQuery($query);
    }

    public function getTerms($params)
    {
        if (!$params) {
            return false;
        }

        $search = isset($params['search']) ? $params['search'] : false;
        $projectId = isset($params['projectId']) ? $params['projectId'] : false;
        $synonymsIds = isset($params['synonymsIds']) ? $params['synonymsIds'] : false;

        $b = false;
        foreach ((array)$synonymsIds as $val) {
            $b .= $val['glossary_id'] . ',';
        }
        if ($b) {
            $b = '(' . rtrim($b, ',') . ')';
        }

        $query = 'select * from  %PRE%glossary where
			(term like "%' . mysqli_real_escape_string($this->databaseConnection, $search) . '%"
			or definition like "%' . mysqli_real_escape_string($this->databaseConnection, $search) . '%" ' .
            ($b ? 'or id in ' . $b . ') ' : '') .
            'and project_id = ' . $projectId . '
		    order by language_id,term';

        return $this->freeQuery($query);
    }
}
