<?php

namespace Linnaeus\App;

set_time_limit(60);

if (getenv('DEBUG')) {
    error_reporting(E_ALL);
} else {
    error_reporting(E_ALL & ~(E_WARNING | E_NOTICE | E_STRICT));
}

date_default_timezone_set('Europe/Amsterdam');
if (getenv('GCMAXLIFETIME')) {
    ini_set('session.gc_maxlifetime', getenv('GCMAXLIFETIME'));
}
if (getenv('MEMORYLIMIT')) {
    ini_set('memory_limit', getenv('MEMORYLIMIT') ? getenv('MEMORYLIMIT') : '512M');
}
require_once 'constants.php';

if (!defined('FIXED_PROJECT_ID')) {
    define('FIXED_PROJECT_ID', getenv('FIXED_PROJECT_ID') ? getenv('FIXED_PROJECT_ID') : null);
}

class Configuration
{
    private $appFileRoot;
    private $skinName;

    public function __construct()
    {
        $this->appFileRoot = getenv('WEBROOT') ? getenv('WEBROOT') : '/var/www/';
        $this->skinName = getenv('SKIN') ? getenv('SKIN') : 'linnaeus_ng';
    }

    public function getDatabaseSettings()
    {
        $settings = [];
        $settings['tablePrefix'] = getenv('TABLE_PREFIX') ?: '';
        $settings['host'] = getenv('MYSQL_HOST') ?: 'database';
        $settings['user'] = getenv('MYSQL_READONLY_USER') ?: 'develop';
        $settings['password'] = getenv('MYSQL_READONLY_PASSWORD') ?: 'develop';
        $settings['read_write_user'] = getenv('MYSQL_USER') ?: 'develop';
        $settings['read_write_password'] = getenv('MYSQL_PASSWORD') ?: 'develop';
        $settings['database'] = getenv('MYSQL_DATABASE') ?: 'develop';
        $settings['characterSet'] = 'utf8';

        return $settings;
    }

    public function getSmartySettings()
    {
        $settings = [];
        $settings['dir_template'] = $this->appFileRoot . 'public/app/templates/';
        $settings['dir_config'] = $this->appFileRoot . 'public/app/templates/configs';
        $settings['dir_compile'] = getenv('SMARTYCOMPILE') ? getenv('SMARTYCOMPILE') : '/cache/app/smarty/templates_c';
        $settings['dir_cache'] = getenv('SMARTYCACHE') ? getenv('SMARTYCACHE') : '/cache/app/smarty/cache';
        $settings['compile_check'] = true;

        return $settings;
    }

    public function getGeneralSettings()
    {

        return array(
            'app' => array(
                'name' => getenv('NAME') ? getenv('NAME') : 'Linnaeus NG',
                'version' => getenv('VERSION') ? getenv('VERSION') : 'unknown',
                'versionTimestamp' => date('r'),
                'pathName' => 'app',
                'fileRoot' => $this->appFileRoot . 'public/app/',
                'skinName' => getenv('SKIN') ? getenv('SKIN') : 'linnaeus_ng',
                'skinNameWebservices' => 'webservices'
            ),
            'defaultController' => 'linnaeus',
            'integratedTemplate' => getenv('INTEGRATED_TEMPLATE') ?: 0,
            'startUpUrl' => '/index.php',
            'showEntryProgramLink' => true,
            'urlNoProjectId' => '../../../app/views/linnaeus/no_project.php',
            'urlUploadedProjectMedia' => '../../../shared/media/project/',
            'lngFileRoot' => $this->appFileRoot,
            'paths' => array(),
            'directories' => array(
                'log' => $this->appFileRoot . 'log',
                'mediaDirProject' => $this->appFileRoot . 'public/shared/media/project',
                'customStyle' => $this->appFileRoot . 'public/app/style/custom'
            ),
            'urlsToAdminEdit' => array(
                'introduction:topic' => '../../../admin/views/introduction/edit.php?id=%s',
                'glossary:term' => '../../../admin/views/glossary/edit.php?id=%s',
                'literature:reference' => '../../../admin/views/literature/edit.php?id=%s',
                'species:taxon' => '../../../admin/views/species/taxon.php?id=%s',
                'species:taxon:literature' => '../../../admin/views/species/literature.php?id=%s',
                'species:taxon:names' => '../../../admin/views/species/synonyms.php?id=%s',
                'module:topic' => '../../../admin/views/module/index.php?page=%s&freeId=%s',
                'linnaeus:content' => '../../../admin/views/content/content.php?page=%s&freeId=%s',
                'mapkey:examine_species' => '../../../admin/views/mapkey/species_edit.php?id=%s',
                'mapkey:l2_examine_species' => '../../../admin/views/mapkey/choose_species.php',
                'matrixkey:identify' => '../../../admin/views/matrixkey/index.php?id=%s',
                'key:index' => '../../../admin/views/key/step_show.php?id=%s',
            ),
            'useGlossaryPostIts' => false,
            'addedProjectIDParam' => 'epi',
        );
    }
}
