<?php

namespace Linnaeus\App;

class Database
{
    private static $instance = array();

    public static function createInstance($id, array $config)
    {
        if (isset(self::$instance[$id])) {
            return false;
        }

        self::$instance[$id] = mysqli_connect(
            $config['host'],
            $config['user'],
            $config['password'],
            $config['database']
        );

        if (mysqli_connect_error()) {
            return false;
        }

        if (isset($config['characterSet'])) {
            mysqli_query(
                self::$instance[$id],
                'SET NAMES ' . $config['characterSet']
            );
            mysqli_query(
                self::$instance[$id],
                'SET CHARACTER SET ' . $config['characterSet']
            );
        }

        // Problems with strict mode in Traits mode; force enable "loose" mode
        mysqli_query(
            self::$instance[$id],
            'SET SESSION sql_mode = "NO_ENGINE_SUBSTITUTION"'
        );

        return self::$instance[$id];
    }

    public static function getInstance($id, $settings)
    {
        if (isset(self::$instance[$id])) {
            if (property_exists(self::$instance[$id], 'client_info')) {
                return self::$instance[$id];
            }
        }
        return self::createInstance($id, $settings);
    }

    public static function deleteInstance($id)
    {
        if (isset(self::$instance[$id])) {
            unset(self::$instance[$id]);
        }
    }

    public static function setReadWriteConnection($id, array $config)
    {
        if (isset(self::$instance[$id])) {
            mysqli_change_user(
                self::$instance[$id],
                $config['read_write_user'],
                $config['read_write_password'],
                $config['database']
            );
        }
    }

    private function __clone()
    {
    }
}
