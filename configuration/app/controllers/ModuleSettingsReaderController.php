<?php

/**
 * Controller wich reads and sets the module and general settings
 */

namespace Linnaeus\App\Controllers;

use Linnaeus\App\Controllers\Controller;
use stdClass;

class ModuleSettingsReaderController extends Controller
{
    public $usedModels = array(
        'module_settings',
        'module_settings_values'
    );

    private $moduleController;
    private $moduleId;
    private $settingsValues;
    private $generalSettingsValues;
    private $useDefaultWhenNoValue = false;
    public $modelNameOverride = 'ModuleSettingsModel';

    /**
     * ModuleSettingsReaderController constructor.
     *
     * @param null $p
     */
    public function __construct($p = null)
    {
        parent::__construct($p);

        $this->setModuleController($this->controllerBaseName);
        $this->initialize();
    }

    public static function getGeneralSettingsId()
    {
        return GENERAL_SETTINGS_ID;
    }

    /**
     * ModuleSettingsReaderController destructor.
     */
    public function __destruct()
    {
        parent::__destruct();
    }

    public function setController($controller)
    {
        $this->setModuleController($controller);
        $this->initialize();
    }

    /**
     * Retrieving a module setting
     *
     * @param $p
     *
     * @return mixed|null|void
     */
    public function getModuleSetting($p)
    {
        if (is_array($p)) {
            $setting = isset($p['setting']) ? $p['setting'] : null;
            $subst = isset($p['subst']) ? $p['subst'] : null;
            $module = isset($p['module']) ? $p['module'] : null;
        } else {
            $setting = $p;
        }

        if (isset($module) && $module != $this->getModuleController()) {
            return $this->getExtraneousModuleSetting($p);
        }

        if (empty($setting)) {
            return;
        }

        foreach ((array) $this->getModuleSettingsValues() as $val) {
            if ($val['setting'] == $setting && !is_null($val['value'])) {
                return $val['value'];
            }
        }

        if (isset($subst)) {
            return $subst;
        }
    }

    /**
     * Retrieving a general setting
     *
     * @param $p
     *
     * @return mixed|null|void
     */
    public function getGeneralSetting($p)
    {
        if (is_array($p)) {
            $setting = isset($p['setting']) ? $p['setting'] : null;
            $subst = isset($p['subst']) ? $p['subst'] : null;
        } else {
            $setting = $p;
        }

        foreach ((array) $this->getGeneralSettingsValues() as $val) {
            if ($val['setting'] == $setting && !is_null($val['value'])) {
                return $val['value'];
            }
        }

        if (isset($subst)) {
            return $subst;
        }
    }

    /**
     * Assing a module setting
     *
     * @param $settings
     */
    public function assignModuleSettings(&$settings)
    {
        $settings = new stdClass();
        foreach ((array) $this->getModuleSettingsValues() as $val) {
            if (is_null($val['value']) && $this->getUseDefaultWhenNoValue() && !is_null($val['default_value'])) {
                $settings->{$val['setting']} = $val['default_value'];
            } else {
                $settings->{$val['setting']} = $val['value'];
            }
        }
    }

    /**
     * Assing a general setting
     *
     * @param $settings
     */
    public function assignGeneralSettings(&$settings)
    {
        $settings = new stdClass();
        foreach ((array) $this->getGeneralSettingsValues() as $val) {
            if (is_null($val['value']) && $this->getUseDefaultWhenNoValue() && !is_null($val['default_value'])) {
                $settings->{$val['setting']} = $val['default_value'];
            } else {
                $settings->{$val['setting']} = $val['value'];
            }
        }
    }

    /**
     * Set a default value
     *
     * @param bool $state
     */
    public function setUseDefaultWhenNoValue($state)
    {
        if (is_bool($state)) {
            $this->useDefaultWhenNoValue = $state;
        }
    }

    /**
     * Initialize the settings
     */
    private function initialize()
    {
        $this->setModuleId();
        $this->setModuleSettingsValues();
        $this->setGeneralSettingsValues();
    }

    private function getUseDefaultWhenNoValue()
    {
        return $this->useDefaultWhenNoValue;
    }

    private function setModuleController($m)
    {
        $this->moduleController = $m;
    }

    private function getModuleController()
    {
        return $this->moduleController;
    }

    private function resolveModuleId($controller)
    {
        $d = $this->models->Modules->getColumn(array("id" => array("controller" => $controller)));

        if ($d) {
            return $d[0]['id'];
        }
    }

    private function setModuleId()
    {
        $this->moduleId = $this->resolveModuleId($this->getModuleController());
    }

    private function getModuleId()
    {
        return $this->moduleId;
    }

    private function setModuleSettingsValues()
    {
        if (is_null($this->getModuleId())) {
            return;
        }

        $this->settingsValues = $this->models->ModuleSettingsModel->setModuleReaderSettingValues(
            array(
                'projectId' => $this->getCurrentProjectId(),
                'moduleId' => $this->getModuleId()
            )
        );
    }

    private function getModuleSettingsValues()
    {
        return $this->settingsValues;
    }

    private function setGeneralSettingsValues()
    {
        $this->generalSettingsValues = $this->models->ModuleSettingsModel->setModuleReaderSettingValues(
            array(
                'projectId' => $this->getCurrentProjectId(),
                'moduleId' => GENERAL_SETTINGS_ID
            )
        );
    }

    private function getGeneralSettingsValues()
    {
        return $this->generalSettingsValues;
    }

    private function getExtraneousModuleSetting($p)
    {
        $setting = isset($p['setting']) ? $p['setting'] : null;
        $subst = isset($p['subst']) ? $p['subst'] : null;
        $module = isset($p['module']) ? $p['module'] : null;

        if (empty($setting)) {
            return;
        }

        $moduleid = $this->resolveModuleId($module);

        if (empty($moduleid)) {
            return;
        }

        $settingsvalues = $this->models->ModuleSettingsModel->setModuleReaderSettingValues(
            array(
                'projectId' => $this->getCurrentProjectId(),
                'moduleId' => $moduleid,
                'setting' => $setting
            )
        );

        if ($settingsvalues) {
            if (!is_null($settingsvalues[0]['value'])) {
                return $settingsvalues[0]['value'];
            }
        }

        if (isset($subst)) {
            return $subst;
        }
    }
}
