<?php

namespace Linnaeus\App\Controllers;

use Linnaeus\App\Controllers\Controller;
use Linnaeus\App\Controllers\ModuleSettingsController;

class SearchController extends Controller
{
    protected $minSearchLength = 3;
    protected $maxSearchLength = 50;

    public const S_TOKENIZED_TERMS = 0;
    public const S_FULLTEXT_STRING = 1;
    public const S_CONTAINS_LITERALS = 2;
    public const S_IS_CASE_SENSITIVE = 3;
    public const S_RESULT_LIMIT_PER_CAT = 4;
    public const S_LIKETEXT_STRING = 5;
    public const S_UNSET_ORIGINAL_CONTENT = 6;
    public const S_EXTENDED_SEARCH = 7;
    public const S_LIKETEXT_REPLACEMENT = '###';
    public const __CONCAT_RESULT__ = '__CONCAT_RESULT__';
    public const V_RESULT_LIMIT_PER_CAT = 200;

    public const C_TAXA_SCI_NAMES = 100;
    public const C_TAXA_DESCRIPTIONS = 101;
    public const C_TAXA_SYNONYMS = 102;
    public const C_TAXA_VERNACULARS = 103;
    public const C_TAXA_ALL_NAMES = 104;
    public const C_SPECIES_MEDIA = 105;

    public $usedModels = array();

    public function __construct()
    {

        parent::__construct();
        $this->initialise();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    private function initialise()
    {
        $this->moduleSettings = new ModuleSettingsController(['controllerBaseName' => 'utilities']);
        $this->moduleSettings->setUseDefaultWhenNoValue(true);
        $this->minSearchLength = $this->moduleSettings->getModuleSetting('min_search_length', 3);
        $this->maxSearchLength = $this->moduleSettings->getModuleSetting('max_search_length', 50);
    }

    protected function validateSearchString($s)
    {
        return
            (strlen($s) >= $this->minSearchLength) &&  // is it long enough?
            (strlen($s) <= $this->maxSearchLength);    // is it short enough?
    }

    // removes interfering noise from search term
    protected function removeSearchNoise($search)
    {
        $noise = [
            $this->hybridMarker,
            $this->hybridMarkerHtml,
            $this->formaMarker,
            $this->hybridMarkerGraftChimaera,
            $this->varietyMarker,
            $this->subspeciesMarker,
            $this->nothoInfixPrefix . $this->varietyMarker,
            $this->nothoInfixPrefix . $this->subspeciesMarker,
        ];

        return preg_replace('/(\s+)/', ' ', str_replace($noise, ' ', $search));
    }
}
