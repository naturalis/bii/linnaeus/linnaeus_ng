<?php

namespace Linnaeus\App\Controllers;

use Linnaeus\App\Configuration;
use Linnaeus\App\Models\Table;
use stdClass;

class TranslatorController
{
    public $usedModels = array(
        'interface_texts',
        'interface_translations',
    );

    private $environment = null;
    private $languageId = null;
    private $models;
    private $text = null;
    private $translation = null;
    private $didTranslate = false;
    private $isNewString = false;
    private $newStrings = array();
    private $storeNewInterfaceStrings;

    public function __construct($env, $languageid, $storeNewInterfaceStrings = false)
    {
        $this->config = new Configuration();
        $this->generalSettings = $this->config->getGeneralSettings();

        $this->environment = $env;
        $this->languageId = $languageid;
        $this->storeNewInterfaceStrings = $storeNewInterfaceStrings;

        $this->loadModels();
        if ($this->storeNewInterfaceStrings) {
            $this->switchToReadWriteConnection();
        }
    }

    public function translate($s)
    {
        if (empty($s)) {
            return;
        }

        if (is_array($s)) {
            $translations = array();
            foreach ($s as $key => $val) {
                $this->text = $val;
                $this->doTranslate();

                if ($this->storeNewInterfaceStrings) {
                    $this->rememberNewString();

                    $this->saveNewStrings();
                    $this->newStrings = [];
                }

                if ($this->didTranslate) {
                    $translations[$key] = $this->translation;
                } else {
                    $translations[$key] = $this->text;
                }
            }
            return $translations;
        } else {
            $this->text = $s;
            $this->doTranslate();
            if ($this->storeNewInterfaceStrings) {
                $this->rememberNewString();

                $this->saveNewStrings();
                $this->newStrings = [];
            }

            if ($this->didTranslate) {
                return $this->translation;
            } else {
                return $this->text;
            }
        }
    }

    private function loadModels()
    {
        $this->models = new stdClass();

        foreach ((array) $this->usedModels as $model) {
            $modelClass = str_replace(' ', '', ucwords(str_replace('_', ' ', $model)));
            $this->models->$modelClass = new Table($model);
        }
    }

    private function doTranslate()
    {
        $this->didTranslate = false;
        $this->isNewString = false;

        if (empty($this->text)) {
            return;
        }

        if (empty($this->languageId)) {
            return;
        }

        // get id of the text
        $i = $this->models->InterfaceTexts->getColumn(
            array(
                'id' => array(
                    'text' => $this->text,
                    'env' => $this->environment
                ),
                'columns' => 'id'
            )
        );

        // if not found, return unchanged
        if (empty($i[0]['id'])) {
            $this->isNewString = true;
            return;
        }

        // fetch appropriate translation
        $it = $this->models->InterfaceTranslations->getColumn(
            array(
                'id' => array(
                    'interface_text_id' => $i[0]['id'],
                    'language_id' => $this->languageId
                ),
                'columns' => 'id,translation',
                'limit' => 1
            )
        );

        // if not found, return unchanged
        if (empty($it[0]['id'])) {
            return;
        }

        $this->translation = $it[0]['translation'];
        $this->didTranslate = true;
    }

    private function rememberNewString()
    {
        if (!$this->didTranslate) {
            $this->newStrings[] = $this->text;
        }
    }

    private function saveNewStrings()
    {
        $this->newStrings = array_unique($this->newStrings);

        foreach ($this->newStrings as $text) {
            $d = $this->models->InterfaceTexts->getColumn(array('id' => array('text' => $text, 'env' => $this->environment)));

            if (!$d) {
                $this->models->InterfaceTexts->save(array('id' => null, 'text' => $text, 'env' => $this->environment));
            }
        }
    }

    public function switchToReadWriteConnection()
    {
        foreach ($this->models as $model) {
            $model->setReadWriteConnection();
        }
    }
}
