<?php

namespace Linnaeus\App\Helpers;

class CheckUrl
{
    private $url;
    private $headers;
    private $response;
    private $httpCode;

    public function __construct()
    {
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function exists()
    {
        if (empty($this->url)) {
            return;
        }
        $this->fetch();
        return ($this->httpCode >= 200 && $this->httpCode < 400);
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getHeader($type)
    {
        foreach ((array) $this->headers as $key => $val) {
            if (strtolower($key) == strtolower($type)) {
                return $val;
            }
        }
    }

    private function fetch()
    {
        $this->headers = get_headers($this->url, 1);
        $this->response = $this->headers[0];
        $this->httpCode = substr(str_ireplace('HTTP/1.1 ', '', $this->response), 0, 3);
    }
}
