<?php

namespace Linnaeus\App\Helpers;

class CurrentUrl
{
    private $url;

    public function __construct()
    {
        $this->url = parse_url((stripos($_SERVER['SERVER_PROTOCOL'], 'https') === 0 ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        $this->setRemoveQuery(false);
        $this->setRemoveFragment(false);
    }

    public function setRemoveQuery($state)
    {
        $this->removeQuery = $state;
    }

    public function setRemoveFragment($state)
    {
        $this->removeFragment = $state;
    }


    public function getUrl()
    {
        return
            $this->url['scheme'] . "://" .
            (isset($this->url['user']) ? $this->url['user'] : "") .
            (isset($this->url['user']) || isset($this->url['pass']) ? ":" : "") .
            (isset($this->url['pass']) ? $this->url['pass'] : "") .
            (isset($this->url['user']) || isset($this->url['pass']) ? "@" : "") .
            $this->url['host'] .
            (isset($this->url['port']) ? ":" . $this->url['port'] : "") .
            $this->url['path'] .
            (isset($this->url['query']) && !$this->removeQuery ? "?" . $this->url['query'] : "") .
            (isset($this->url['fragment']) && !$this->removeFragment ? "#" . $this->url['fragment'] : "");

        return $this->url;
    }

    public function getParts()
    {
        return $this->url;
    }
}
