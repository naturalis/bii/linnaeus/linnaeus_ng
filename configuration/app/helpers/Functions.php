<?php

namespace Linnaeus\App\Helpers;

class Functions
{
    private $keyValueGlue = null;

    private function fission($v, $k)
    {
        return (!empty($k) ? $k . $this->keyValueGlue : '') . $v;
    }

    public function nuclearImplode($keyValueGlue, $elementGlue, $array, $skipEmptyValues = false)
    {
        if (!is_array($array)) {
            return $array;
        }

        $this->keyValueGlue = $keyValueGlue;

        if ($skipEmptyValues) {
            foreach ($array as $key => $val) {
                if (empty($val)) {
                    unset($array[$key]);
                }
            }
        }

        return implode($elementGlue, array_map(function ($v, $k) {
                return $this->fission($v, $k);
        }, $array, array_keys($array)));
    }
}
