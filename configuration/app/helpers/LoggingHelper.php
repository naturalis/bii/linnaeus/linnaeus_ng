<?php

namespace Linnaeus\App\Helpers;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class LoggingHelper
{
    private $logger = null;
    private $levels = array(
        0 => Logger::INFO,    // not an error
        1 => Logger::WARNING,    // non-fatal error
        2 => Logger::ERROR    // fatal error
    );
    private $level = Logger::ERROR;

    public function __construct()
    {
        $this->logger = new Logger('app');
        $this->level = getEnv('DEBUG') ? Logger::INFO : Logger::WARNING;
        $stream = new StreamHandler('php://stdout', $this->level);
        $this->logger->pushHandler($stream);
    }

    public function log($msg, $severity = 0, $source = false)
    {
        $level = array_key_exists($severity, $this->levels) ? $this->levels[$severity] : Logger::ERROR;
        if ($source) {
            $this->write($source, $level);
        }
        return $this->write($msg, $level);
    }

    public function write($msg, $level)
    {
        if (is_array($msg)) {
            foreach ((array)$msg as $val) {
                $this->logger->log($level, trim(preg_replace("/\s+/", " ", $val)));
            }
        } else {
            $this->logger->log($level, trim(preg_replace("/\s+/", " ", $msg)));
        }

        return true;
    }
}
