<?php

namespace Linnaeus\App\Helpers;

class Paginator
{
    private $itemsPerPage = 25;
    private $start = 0;
    private $items;
    private $result;

    public function setItemsPerPage($int)
    {
        if (is_int($int) && $int > 0) {
            $this->itemsPerPage = $int;
        }
    }

    public function setStart($start)
    {
        // determines first item to show
        if (is_int($start) && $start > 0) {
            $this->start = $start;
        }
    }

    public function setItems($items)
    {
        if (is_array($items)) {
            $this->items = $items;
        }
    }

    public function paginate()
    {
        if (empty($this->items)) {
            return;
        }

        //determine index of the first item to show on the previous page (if any)
        $prevStart = $this->start == 0 ? -1 : (($this->start - $this->itemsPerPage < 1) ? 0 : ($this->start - $this->itemsPerPage));

        //determine index of the first taxon to show on the next page (if any)
        $nextStart = ($this->start + $this->itemsPerPage >= count((array) $this->items)) ? -1 : ($this->start + $this->itemsPerPage);

        // slice out only the taxa we need
        $slice = array_slice($this->items, $this->start, $this->itemsPerPage);

        $this->result = array(
            'items' => $slice,
            'prevStart' => $prevStart,
            'currStart' => $this->start,
            'nextStart' => $nextStart
        );
    }

    public function getItems()
    {
        return $this->result;
    }
}
