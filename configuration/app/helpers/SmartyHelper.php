<?php

namespace Linnaeus\App\Helpers;

use Smarty;

/**
 * When using PHP 8.1+ and Smarty 4.3, deprecated warnings such as this started appearing in the logs:
 *
 * NOTICE: PHP message: PHP Deprecated:  Using php-function "array_merge" as a modifier is deprecated and will be removed in a future release. Use Smarty::registerPlugin to explicitly register a custom modifier. in /var/www/vendor/smarty/smarty/libs/sysplugins/smarty_internal_compile_private_modifier.php on line 114
 *
 * In order to tackle these, the Smarty initialision was moved to a dedicated helper rather
 * than being handled in the main Controller. Basically, it's sufficient to register the default
 * PHP function with the function used in the template. However, with PHP 8.1 being more picky
 * than 7.4, this resulted in more serious errors if e.g. null was passed where a string was
 * expected. The solution is to create "safe" callbacks for those problematic functions.
 *
 * The list of internal PHP functions being used in the templates is stored in `$smartyModifiers`.
 * This list may be incomplete. In most cases, it's enough to add the function to the list.
 * If warnings or errors still occur, you need to add a "safe" callback method. This can be mapped
 * to the default PHP function in the `initialize` method.
 */

class SmartyHelper
{
    private $smarty;
    private $settings;
    private $projectUrl;
    private $translator;
    private $smartyModifiers = [
        'addslashes',
        'array_merge',
        'chr',
        'count',
        'end',
        'htmlentities',
        'implode',
        'is_array',
        'json_encode',
        'mb_strtolower',
        'ord',
        'rand',
        'sprintf',
        'strip_tags',
        'strpos',
        'strstr',
        'strtolower',
        'strtoupper',
        'substr',
        'trim',
        'ucfirst',
        'ucwords',
        'urlencode',
    ];

    public function __construct($settings)
    {
        $this->smarty = new Smarty();
        $this->settings = $settings;
    }

    public function __call($name, $args)
    {
        return $this->smarty->$name($args[0] ?? null, $args[1] ?? null);
    }

    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }

    public function setProjectUrl($url)
    {
        $this->projectUrl = $url;
    }

    public function initialize()
    {
        $this->smarty->compile_dir = $this->settings['dir_compile'];
        $this->smarty->cache_dir = $this->settings['dir_cache'];
        $this->smarty->config_dir = $this->settings['dir_config'];
        $this->smarty->compile_check = $this->settings['compile_check'];
        $this->smarty->registerPlugin("block", "t", [$this, "translate"]);
        $this->smarty->registerPlugin("block", "snippet", [$this, "getSnippet"]);

        // Never switch caching on, it makes the application unstable!
        $this->smarty->caching = 0;
        $this->smarty->force_compile = (bool)getenv('DEBUG');
        if (getenv('SMARTY_WARNINGS')) {
            $this->smarty->error_reporting = E_ALL;
        } else {
            $this->smarty->error_reporting = E_ALL & ~(E_WARNING | E_NOTICE | E_STRICT);
        }

        // Add "safe" callbacks; when no safe option is provided, the vanilla PHP function is used
        foreach ($this->smartyModifiers as $modifier) {
            $callback = match ($modifier) {
                'count' => [$this, 'safeCount'],
                'implode' => [$this, 'safeImplode'],
                'strpos' => [$this, 'safeStrpos'],
                'strtolower' => [$this, 'safeStrtolower'],
                'trim' => [$this, 'safeTrim'],
                default => $modifier,
            };
            $this->smarty->registerPlugin('modifier', $modifier, $callback);
        }

        return $this->smarty;
    }

    public function safeCount($array)
    {
        return is_array($array) ? count($array) : 0;
    }

    public function safeStrpos($haystack, $needle)
    {
        return strpos((string)$haystack, (string)$needle);
    }

    public function safeStrtolower($string)
    {
        return strtolower((string)$string);
    }

    public function safeImplode($glue, $array)
    {
        return implode((string)$glue, (array)$array);
    }

    public function safeTrim($string)
    {
        return trim((string)$string);
    }

    public function setTemplateDir($skinName, $controllerBase)
    {
        $this->smarty->template_dir = $this->settings['dir_template'] . $skinName . '/' . $controllerBase . '/';
    }

    /**
     * Get a snippet from a file
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getSnippet($params, $content, &$smarty, &$repeat)
    {
        if (!$this->projectUrl || is_null($content)) {
            return;
        }

        $pu = $this->projectUrl; // project-specific dir
        $gu = $_SESSION['app']['system']['urls']['snippets']; // general dir

        $possibilities[1] = $pu . $content;

        if (isset($params['language'])) {
            $d = pathinfo($possibilities[1]);
            $possibilities[0] = $pu . $d["filename"] . '--' . sprintf('%04s', $params['language']) . '.' . $d["extension"];
        }

        $possibilities[3] = $gu . $content;

        if (isset($params['language'])) {
            $d = pathinfo($possibilities[3]);
            $possibilities[2] = $gu . $d["filename"] . '--' . sprintf('%04s', $params['language']) . '.' . $d["extension"];
        }

        foreach ($possibilities as $file) {
            if (file_exists($file)) {
                return file_get_contents($file);
            }
        }
    }

    /**
     * Translate a string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function translate($params, $content, &$smarty, &$repeat)
    {
        if (!$this->translator) {
            return $content;
        }

        $c = $this->translator->translate($content) ?? '';

        if (isset($params)) {
            foreach ((array)$params as $key => $val) {
                if (substr($key, 0, 2) == '_s' && isset($val)) {
                    $c = preg_replace('/\%s/', $val, $c, 1);
                }
            }
        }
        return $c;
    }
}
