<?php

namespace Linnaeus\App\Models;

use DateTime;
use Linnaeus\App\BaseClass;
use Linnaeus\App\Database;
use Linnaeus\App\Configuration;
use Linnaeus\App\Controllers\Controller;
use Linnaeus\App\Helpers\LoggingHelper;

class AbstractModel extends BaseClass
{
    public $databaseConnection;
    public $tableName;
    public $newId;
    public $doLog = true;

    protected $noKeyViolationLogging = false;
    protected $databaseSettings;
    protected $data;
    protected $tablePrefix;
    protected $id;
    protected $lastQuery;
    protected $logger;
    protected $projectId = false;
    protected $affectedRows = 0;
    protected $currentWhereArray = null;
    protected $tableExists = true;

    protected $dataBeforeQuery = true;
    protected $dataAfterQuery = true;
    protected $error;

    public function __construct()
    {
        parent::__construct();

        $this->databaseConnection = $this->getDatabaseConnection();
    }

    public function __destruct()
    {
        $this->disconnectFromDatabase();
    }

    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    public function escapeString($d)
    {
        return is_string($d) ? mysqli_real_escape_string($this->getDatabaseConnection(), $d) : $d;
    }

    public function save($data)
    {
        if ($this->hasId($data)) {
            return $this->update($data);
        } else {
            return $this->insert($data);
        }
    }

    public function getAffectedRows()
    {
        return $this->affectedRows;
    }

    public function getLastQuery()
    {
        return $this->lastQuery;
    }

    public function setNoKeyViolationLogging($state)
    {
        if (is_bool($state)) {
            $this->noKeyViolationLogging = $state;
        }
    }

    public function freeQuery($params)
    {
        if (is_array($params)) {
            $query = isset($params['query']) ? $params['query'] : null;
            $fieldAsIndex = isset($params['fieldAsIndex']) ? $params['fieldAsIndex'] : false;
        } else {
            $query = isset($params) ? $params : null;
            $fieldAsIndex = false;
        }

        if (empty($query)) {
            $this->log('Called freeQuery with an empty query', 1);
            return;
        }
        ;

        $query = str_ireplace('%table%', $this->tableName ?: "", $query);
        $query = str_ireplace('%pre%', $this->tablePrefix ?: "", $query);

        $set = mysqli_query($this->getDatabaseConnection(), $query);

        $error = mysqli_error($this->getDatabaseConnection());
        if ($error) {
            $this->log($error, 2);
            $this->setError($error);
        }

        $this->logQueryResult($set, $query, 'freeQuery');
        $this->setLastQuery($query);
        $this->setAffectedRows();

        unset($this->data);

        // Check if our set is the expected array and not the error boolean
        if ($set instanceof \mysqli_result) {
            while ($row = mysqli_fetch_assoc($set)) {
                if ($fieldAsIndex !== false && isset($row[$fieldAsIndex])) {
                    $this->data[$row[$fieldAsIndex]] = $row;
                } else {
                    $this->data[] = $row;
                }
            }
        }

        return isset($this->data) ? $this->data : null;
    }

    public function makeWhereString($p = null, $alias = null)
    {
        if (!is_null($p)) {
            $this->setCurrentWhereArray($p);
        }

        if (!is_array($this->getCurrentWhereArray())) {
            return;
        }

        $d = implode(
            ' and ',
            array_map(
                function ($v, $k) {
                    $d = is_numeric($v) ? "%s" : "'%s'";
                    return sprintf(chr(21) . "%s=$d", $k, $v);
                },
                $this->getCurrentWhereArray(),
                array_keys($this->getCurrentWhereArray())
            )
        );

        return str_replace(chr(21), (is_null($alias) ? null : $alias . '.'), $d);
    }

    public function getDataDelta()
    {
        $b = $b1 = isset($this->dataBeforeQuery) ? $this->dataBeforeQuery : null;
        $a = $a1 = isset($this->dataAfterQuery) ? $this->dataAfterQuery : null;

        if (isset($a1['created'])) {
            unset($a1['created']);
        }
        if (isset($a1['last_change'])) {
            unset($a1['last_change']);
        }
        if (isset($b1['created'])) {
            unset($b1['created']);
        }
        if (isset($b1['last_change'])) {
            unset($b1['last_change']);
        }

        return array(
            'before' => $b,
            'after' => $a,
            'changed' => serialize($b1) != serialize($a1)
        );
    }

    public function resetAffectedRows()
    {
        $this->affectedRows = 0;
    }

    protected function setCurrentWhereArray($p = null)
    {
        if (is_array($p)) {
            $this->currentWhereArray = $p;
        }
    }

    protected function getCurrentWhereArray()
    {
        return $this->currentWhereArray;
    }

    protected function log($msg, $level = 0)
    {
        if (!$this->doLog) {
            return;
        }

        if ($this->logger instanceof LoggingHelper) {
            $logmsg = sprintf("(%s) %s (%s: %s)", $this->projectId ?: '?', $msg, mysqli_errno($this->getDatabaseConnection()), mysqli_error($this->getDatabaseConnection()));
            $this->logger->log($logmsg, $level, 'Model:' . get_class($this));
        }
    }

    protected function setAffectedRows()
    {
        $this->affectedRows = mysqli_affected_rows($this->getDatabaseConnection());
    }

    protected function isDateTimeFunction($val)
    {
        try {
            $date = new DateTime($val);
            return !is_object($date);
        } catch (\Exception $e) {
            return true;
        }
    }

    protected function setLastQuery($query)
    {
        $this->lastQuery = $query;
    }

    protected function getDatabaseConnection()
    {
        $config = new Configuration();
        $database = new Database();
        $this->databaseConnection = $database::getInstance('lngApp', $config->getDatabaseSettings());
        if (!$this->databaseConnection) {
            die('Error ' . mysqli_connect_errno() . ': failed to connect to database');
        }
        return $this->databaseConnection;
    }

    public function setReadWriteConnection()
    {
        $config = new Configuration();
        $database = new Database();
        $database::setReadWriteConnection('lngApp', $config->getDatabaseSettings());
    }

    protected function disconnectFromDatabase()
    {
        if (is_resource($this->databaseConnection) && get_resource_type($this->databaseConnection) === 'mysql link') {
            $this->databaseConnection->close(); //Object oriented style
            //mysqli_close($connection); //Procedural style
            unset($this->databaseConnection);

            $database = new Database();
            $database::deleteInstance('lngApp');
        }
    }

    protected function reEngineerQuery($query)
    {
        $q = null;

        // inserts return an id, not a query
        if (is_int($query)) {
            $q = 'select * from ' . $this->tableName . ' where id = ' . $query;
        } elseif (stripos($query, 'delete') === 0) {
            $q = str_ireplace('delete from', 'select * from', $query);
        } elseif (stripos($query, 'update') === 0) {
            /* this will fail if there is a string with the substring " where " somewhere in the where-clause*/
            $d = preg_split('/ where /', $query);
            $q = 'select * from ' . $this->tableName . ' where ' . $d[count((array) $d) - 1];
        }
        return $q;
    }

    protected function retainDataBeforeQuery($query)
    {
        unset($this->dataBeforeQuery);

        if (empty($query)) {
            return;
        }

        $q = $this->reEngineerQuery($query);

        if (isset($q)) {
            $result = mysqli_query($this->getDatabaseConnection(), $q);
            while ($r = mysqli_fetch_assoc($result)) {
                $this->dataBeforeQuery[] = $r;
            }
        }
    }

    protected function retainDataAfterQuery($query, $failed = false)
    {
        unset($this->dataAfterQuery);

        if ($failed) {
            return;
        }

        $q = $this->reEngineerQuery($query);

        if (isset($q)) {
            $result = mysqli_query($this->getDatabaseConnection(), $q);
            while ($r = mysqli_fetch_assoc($result)) {
                $this->dataAfterQuery[] = $r;
            }
        }
    }

    protected function logQueryResult($set, $query, $type, $severity = 1)
    {
        if (!$set) {
            // 1062 = key violation
            if (
                mysqli_errno($this->getDatabaseConnection()) != 1062
                || $this->noKeyViolationLogging != true
            ) {
                $this->log('Failed query (' . $type . '): ' . $query, $severity);
            }
        }
    }

    public function cleanUp($pId, $ms)
    {

        if (empty($pId) || empty($ms) || !is_numeric($ms)) {
            return;
        }

        // ppl probably get confused by milli and micro...
        if (log10($ms) < 5) {
            $ms *= 1000; // it were milliseconds!
        }
        if (log10($ms) < 5) {
            $ms *= 1000; // no, even worse! it were seconds!
        }

        $this->execute("delete from %table%  where project_id = " . $pId . " and last_change <= TIMESTAMPADD(MICROSECOND ,-" . ($ms) . ",CURRENT_TIMESTAMP)");
    }

    protected function getDataType($n)
    {
        $mysql_data_type_hash = array(
            1 => 'tinyint',
            2 => 'smallint',
            3 => 'int',
            4 => 'float',
            5 => 'double',
            7 => 'timestamp',
            8 => 'bigint',
            9 => 'mediumint',
            10 => 'date',
            11 => 'time',
            12 => 'datetime',
            13 => 'year',
            16 => 'bit',
            //252 is currently mapped to all text and blob types (MySQL 5.0.51a)
            253 => 'varchar',
            254 => 'char',
            246 => 'decimal'
        );
        return array_key_exists($n, $mysql_data_type_hash) ? $mysql_data_type_hash[$n] : null;
    }

    public function setLocale($locale)
    {
        if (!$locale) {
            return false;
        }

        try {
            $this->freeQuery("SET lc_time_names = '" . mysqli_real_escape_string($this->getDatabaseConnection(), $locale) . "'");
        } catch (\Exception $e) {
            $this->log($e->getMessage(), 2);
        }
    }

    public function getLanguagesUsed($projectId = null)
    {
        if (is_null($projectId)) {
            return null;
        }

        $query = "
            select count(id) as `count`, language_id
			from %PRE%names
			where project_id=" . $projectId . "
			group by language_id
			order by `count` asc";

        return $this->freeQuery($query);
    }

    public function arrayHasData($p = array())
    {
        foreach ($p as $v) {
            if (is_array($v)) {
                $this->arrayHasData($v);
            }
            if ($v != '') {
                return true;
            }
        }
        return false;
    }

    public function generateTaxonParentageId($id)
    {
        return sprintf('%05s', $id);
    }

    public function setError($error)
    {
        $this->error = $error;
    }

    public function getError()
    {
        return $this->error;
    }
}
