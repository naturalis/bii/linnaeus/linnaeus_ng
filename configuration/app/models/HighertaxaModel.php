<?php

namespace Linnaeus\App\Models;

use Linnaeus\App\Models\SpeciesModel;

/* This model is a clone of SpeciesModel */

final class HighertaxaModel extends SpeciesModel
{
    public function __construct()
    {
        parent::__construct();
    }


    public function __destruct()
    {
        parent::__destruct();
    }
}
