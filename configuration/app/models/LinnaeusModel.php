<?php

namespace Linnaeus\App\Models;

use Linnaeus\App\Models\AbstractModel;

final class LinnaeusModel extends AbstractModel
{
    public function getFirstIntroductionTopicId($params)
    {

        $project_id = isset($params['project_id']) ? $params['project_id'] : null;

        if (is_null($project_id)) {
            return;
        }

        $query = "
			select
				*
			from
				%PRE%introduction_pages _a

			where
				_a.project_id = " . $project_id . "
				and _a.got_content = 1
			order by
				_a.show_order
			limit 1
		";

        $d = $this->freeQuery($query);

        return $d ? $d[0]['id'] : null;
    }
}
