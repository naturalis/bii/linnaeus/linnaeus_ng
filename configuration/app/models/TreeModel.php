<?php

namespace Linnaeus\App\Models;

use Linnaeus\App\Models\AbstractModel;

final class TreeModel extends AbstractModel
{
    public function getTreeTop($params)
    {
        $project_id = isset($params['project_id']) ? $params['project_id'] : null;
        $top_rank_id = isset($params['top_rank_id']) ? $params['top_rank_id'] : 10;

        if (is_null($project_id)) {
            return;
        }

        $query = "
			SELECT
				_a.id,
				_a.taxon,
				_r.rank
			FROM
				%PRE%taxa _a
			LEFT JOIN
			    %PRE%trash_can _trash
				ON _a.project_id = _trash.project_id
				AND _a.id = _trash.lng_id
				AND _trash.item_type='taxon'
			LEFT JOIN
			    %PRE%projects_ranks _p
				ON _a.project_id=_p.project_id
				AND _a.rank_id=_p.id
			LEFT JOIN
			    %PRE%ranks _r
				ON _p.rank_id=_r.id
			WHERE 
				_a.project_id = " . $project_id . " 
				AND ifnull(_trash.is_deleted,0)=0
				AND _a.parent_id IS NULL
				AND _r.id <= $top_rank_id
			";

        return $this->freeQuery($query);
    }

    public function getTreeBranch($params)
    {
        $project_id = isset($params['project_id']) ? $params['project_id'] : null;
        $language_id = isset($params['language_id']) ? $params['language_id'] : null;
        $type_id_preferred = isset($params['type_id_preferred']) ? $params['type_id_preferred'] : null;
        $type_id_valid = isset($params['type_id_valid']) ? $params['type_id_valid'] : null;
        $node = isset($params['node']) ? $params['node'] : null;

        if (is_null($project_id) || is_null($language_id) || is_null($type_id_preferred) || is_null($type_id_valid) || is_null($node)) {
            return;
        }

        $query = "
			SELECT
				_a.id,
				_a.parent_id,
				_a.is_hybrid,
				_a.rank_id,
				_p.rank_id AS base_rank_id,
				_a.taxon,
				IFNULL(_k.name,_l.commonname) AS name,
				_m.authorship,
				_r.rank,
				IFNULL(_q.label,_r.rank) AS rank_label,
				_p.rank_id AS base_rank_id,
				_sq.parentage
			FROM
				%PRE%taxa _a
			LEFT JOIN
				%PRE%taxon_quick_parentage _sq
				on _a.project_id = _sq.project_id
				and _a.id=_sq.taxon_id
			LEFT JOIN
			    %PRE%trash_can _trash
				ON _a.project_id = _trash.project_id
				AND _a.id = _trash.lng_id
				AND _trash.item_type='taxon'
			LEFT JOIN
			    %PRE%projects_ranks _p
				ON _a.project_id=_p.project_id
				AND _a.rank_id=_p.id
			LEFT JOIN
			    %PRE%labels_projects_ranks _q
				ON _a.rank_id=_q.project_rank_id
				AND _a.project_id = _q.project_id
				AND _q.language_id=" . $language_id . "
			LEFT JOIN
			    %PRE%ranks _r
				ON _p.rank_id=_r.id
			LEFT JOIN
			    %PRE%commonnames _l
				ON _a.id=_l.taxon_id
				AND _a.project_id=_l.project_id
				AND _l.language_id=" . $language_id . "
			LEFT JOIN
			    %PRE%names _k
				ON _a.id=_k.taxon_id
				AND _a.project_id=_k.project_id
				AND _k.type_id=" . $type_id_preferred . "
				AND _k.language_id=" . $language_id . "
			LEFT JOIN
			    %PRE%names _m
				ON _a.id=_m.taxon_id
				AND _a.project_id=_m.project_id
				AND _m.type_id=" . $type_id_valid . "
			WHERE 
				_a.project_id = " . $project_id . " 
				AND ifnull(_trash.is_deleted,0)=0
				AND (_a.id = " . $node . " OR _a.parent_id = " . $node . ")
			ORDER BY
				label
		";

        return $this->freeQuery($query);
    }

    public function getBranchTaxonCount($params)
    {
        $project_id = isset($params['project_id']) ? $params['project_id'] : null;
        $node = isset($params['node']) ? $params['node'] : null;

        if (is_null($project_id) || is_null($node)) {
            return;
        }

        $query = "
			SELECT
				COUNT(*) AS total
			FROM
				%PRE%taxon_quick_parentage _sq
			LEFT JOIN
			    %PRE%taxa _e ON _sq.taxon_id = _e.id AND _sq.project_id = _e.project_id
			LEFT JOIN
			    %PRE%trash_can _trash ON _e.project_id = _trash.project_id
				AND _e.id = _trash.lng_id
				AND _trash.item_type='taxon'
			WHERE 
				_sq.project_id = " . $project_id . " 
				AND ifnull(_trash.is_deleted,0)=0
				AND MATCH(_sq.parentage) AGAINST ('" . $this->generateTaxonParentageId($node) . "' IN boolean mode)
			";

        $d = $this->freeQuery($query);

        if (!empty($d)) {
            return $d[0]['total'];
        }
    }

    public function getBranchSpeciesCount($params)
    {
        $project_id = isset($params['project_id']) ? $params['project_id'] : null;
        $base_rank_id = isset($params['base_rank_id']) ? $params['base_rank_id'] : null;
        $node = isset($params['node']) ? $params['node'] : null;

        if (is_null($project_id) || is_null($base_rank_id) || is_null($node)) {
            return;
        }

        $query = "
			SELECT
				COUNT(_sq.taxon_id) AS total,
				_sq.taxon_id,
				_sp.presence_id,
				IFNULL(_sr.established,'undefined') AS established
			FROM 
				%PRE%taxon_quick_parentage _sq
			LEFT JOIN
			    %PRE%presence_taxa _sp ON _sq.project_id=_sp.project_id AND _sq.taxon_id=_sp.taxon_id
			LEFT JOIN
			    %PRE%presence _sr ON _sp.project_id=_sr.project_id AND _sp.presence_id=_sr.id
			LEFT JOIN
			    %PRE%taxa _e ON _sq.taxon_id = _e.id AND _sq.project_id = _e.project_id
			LEFT JOIN
			    %PRE%trash_can _trash ON _e.project_id = _trash.project_id
				AND _e.id = _trash.lng_id
				AND _trash.item_type='taxon'
			LEFT JOIN
			    %PRE%projects_ranks _f
				ON _e.rank_id=_f.id AND _e.project_id = _f.project_id
			WHERE
				_sq.project_id=" . $project_id . "
				AND IFNULL(_trash.is_deleted,0)=0
				AND _f.rank_id" . ($base_rank_id >= SPECIES_RANK_ID ? ">=" : "=") . " " . SPECIES_RANK_ID . "
				AND MATCH(_sq.parentage) AGAINST ('" . $this->generateTaxonParentageId($node) . "' IN boolean mode)
			GROUP BY
				_sr.established
			";

        return $this->freeQuery(array("query" => $query, "fieldAsIndex" => "established"));
    }

    public function hasChildren($params)
    {
        $project_id = isset($params['project_id']) ? $params['project_id'] : null;
        $node = isset($params['node']) ? $params['node'] : null;

        if (is_null($project_id) || is_null($node)) {
            return;
        }

        $query = "
			SELECT
				COUNT(*) AS total
			FROM
				%PRE%taxa _a
			LEFT JOIN
			    %PRE%trash_can _trash ON _a.project_id = _trash.project_id
				AND _a.id = _trash.lng_id
				AND _trash.item_type='taxon'
			WHERE 
				_a.project_id = " . $project_id . " 
				AND IFNULL(_trash.is_deleted,0)=0
				AND _a.parent_id = " . $node;

        $d = $this->freeQuery($query);

        if (!empty($d)) {
            return $d[0]['total'] > 0;
        }
    }
}
