<?php

/*
 * Dit script vervangt een oude Trezorix link naar een moderne NSR link. Bijvoorbeeld, de oude wasbeer pagina link hieronder,
 * https://www.nederlandsesoorten.nl/nsr/concept/000465032185/summary
 *
 * wordt herschreven door dit script als:
 * https://www.nederlandsesoorten.nl/linnaeus_ng/rewrite.php?p=1&u=nsr/concept/000465032185/summary&
 *
 * en vervolgens doorverwezen naar:
 * https://www.nederlandsesoorten.nl/linnaeus_ng/app/views/species/nsr_taxon.php?id=139011&cat=158&epi=1
 *
 * Het is hierbij mogelijk om een tab mee te geven, die door resolveTab() wordt verwerkt.
 */


use Linnaeus\App\Controllers\SpeciesController;

$c = new SpeciesController();

$whitelist = [
    "nederlandsesoorten.nl",
    "www.nederlandsesoorten.nl",
    "www-b.nederlandsesoorten.nl",
    "www-c.nederlandsesoorten.nl",
    "acc.nederlandsesoorten.nl"
];

$baseUrlLng = '/linnaeus_ng/app/views/';

function resolveNsrId($nsrid, $type)
{
    global $c;

    $nsrid = str_pad($nsrid, 12, '0', STR_PAD_LEFT);

    $type = $c->models->Taxa->escapeString($type);
    $nsrid = $c->models->Taxa->escapeString($nsrid);

    $data = $c->models->Taxa->freeQuery("select * from %PRE%nsr_ids where nsr_id like '%" . $type . "/" . $nsrid . "'");

    if (empty($data[0])) {
        $data = $c->models->Taxa->freeQuery("select * from %PRE%nsr_ids where nsr_id = '" . $nsrid . "'");
    }

    return $data[0];
}

function resolveTab($name)
{
    switch ($name) {
        case "recognition":
            return TAB_HERKENNING;

        case "similarSpecies":
            return TAB_GELIJKENDE_SOORTEN;

        case "summary":
            return TAB_SUMMARY;

        case "research":
            return TAB_ONDERZOEK;

        case "conservation":
            return TAB_BESCHERMING;

        case "biotopes":
            return TAB_BIOTOPEN;

        case "relatedSpecies":
            return TAB_RELATIES;

        case "biology":
            return TAB_LEVENSWIJZE;

        case "taxonomy":
            return 'CTAB_NAMES';

        case "images":
        case "imagesAndSounds":
            return 'CTAB_MEDIA';

        /*
        TAB_ALGEMEEN;
        TAB_BEDREIGING_EN_BESCHERMING
        TAB_BRONNEN
        TAB_HABITAT
        TAB_NAAMGEVING
        TAB_SAMENVATTING
        TAB_SCHADE_EN_NUT
        TAB_VERPLAATSING
        TAB_VERSPREIDING
        CTAB_CLASSIFICATION
        CTAB_TAXON_LIST
        CTAB_LITERATURE
        CTAB_DNA_BARCODES
        CTAB_NOMENCLATURE
        */
    }
}

$fixedUrls = [
    'nsr/nsr/i000399.html' => $baseUrlLng . 'search/nsr_search_extended.php',
    'nsr/nsr/i000398.html' => $baseUrlLng . 'search/nsr_search_pictures.php',
    'nsr/nsr/recentImages.html' => $baseUrlLng . 'search/nsr_recent_pictures.php',
    'natuurwidget' => '/node/48',
    'nsr/nsr/home.html' => '/',
    'nsr/nsr/i000000.html' => '/',
    'nsr/nsr/i000222.html' => '/node/12',
    'nsr/nsr/i000224.html' => '/node/13',
    'nsr/nsr/i000234.html' => '/node/14',
    'nsr/nsr/i000335.html' => '/node/15',
    'nsr/nsr/i000404.html' => '/node/16',
    'nsr/nsr/i000242.html' => '/node/17',
    'nsr/nsr/i000330.html' => '/node/49',
    'nsr/nsr/i000334.html' => '/node/50',
    'nsr/nsr/i000388.html' => '/node/18',
    'nsr/nsr/i000396.html' => '/node/48',
    'nlsr/nlsr/i000396.html' => '/node/48',
    'nsr/nsr/i000403.html' => '/content/colofon-natuurwidget',
    'nsr/nsr/i000397.html' => '/content/installatie-instructie',
    'nsr/nsr/i000363.html' => '/node/19',
    'nsr/nsr/i000366.html' => '/node/21',
    'nsr/nsr/i000369.html' => '/node/22',
    'nsr/nsr/i000374.html' => '/node/23',
    'nsr/nsr/i000372.html' => '/node/24',
    'nsr/nsr/i000385.html' => '/node/25',
    'nsr/nsr/i000386.html' => '/node/26',
    'nsr/nsr/i000406.html' => '/node/27',
    'nsr/nsr/i000323.html' => '/node/28',
    'nsr/nsr/i000324.html' => '/node/29',
    'nsr/nsr/i000326.html' => '/node/30',
    'nsr/nsr/i000325.html' => '/node/31',
    'nsr/nsr/i000327.html' => '/node/32',
    'nsr/nsr/english.html' => '/node/375',
    'nsr/nsr/links.html' => '/node/373',
    'nsr/nsr/contact.html' => '/node/535',
    'nsr/nsr/colofon.html' => '/content/colofon',
];


if (isset($url)) {
    $parsed_url = parse_url($url);
    if (array_key_exists('host', $parsed_url) && !in_array($parsed_url['host'], $whitelist)) {
        $c->log(sprintf("Possible phishing attempt: '%s' is not found in whitelist", $parsed_url['host']), 2);
        die();
    }

    $redirectUrl = $url . (count((array) $parameters) > 0 ? '?' . http_build_query($parameters) : '');

    preg_match('/^(nsr|nlsr|zoek|natuurwidget|get)(\/)?(.*)$/', $url, $matches);

    if ($matches[1] == 'get') {
        if (isset($parameters['page_alias'])) {
            if ($parameters['page_alias'] == 'topList' && isset($parameters['show']) && $parameters['show'] == 'photographers') {
                $redirectUrl = $baseUrlLng . 'search/nsr_photographers.php';
            } else {
                if ($parameters['page_alias'] == 'topList' && isset($parameters['show']) && $parameters['show'] == 'validators') {
                    $redirectUrl = $baseUrlLng . 'search/nsr_validators.php';
                } else {
                    if ($parameters['page_alias'] == 'searchImages' && isset($parameters['photographer'])) {
                        $redirectUrl = $baseUrlLng . 'search/nsr_search_pictures.php?photographer=' . $parameters['photographer'];
                    } else {
                        if ($parameters['page_alias'] == 'searchImages' && isset($parameters['validator'])) {
                            $redirectUrl = $baseUrlLng . 'search/nsr_search_pictures.php?validator=' . $parameters['validator'];
                        } else {
                            if ($parameters['page_alias'] == 'imageview' && isset($parameters['cid'])) {
                                $data = resolveNsrId($parameters['cid'], 'concept');
                                $redirectUrl = $baseUrlLng .
                                    "species/nsr_taxon.php?id=" . $data['lng_id'] .
                                    "&cat=" . CTAB_MEDIA .
                                    (isset($parameters['image']) ? "&img=" . $parameters['image'] : null);
                            } else {
                                if ($parameters['page_alias'] == 'conceptcard' && isset($parameters['cid']) && isset($parameters['detail'])) {
                                    $data = resolveNsrId($parameters['cid'], 'concept');
                                    $redirectUrl = $baseUrlLng . "species/nsr_taxon.php?id=" . $data['lng_id'] . "&cat=" . resolveTab($parameters['detail']);
                                } else {
                                    if ($parameters['page_alias'] == 'conceptcard' && isset($parameters['cid'])) {
                                        $data = resolveNsrId($parameters['cid'], 'concept');
                                        $redirectUrl = $baseUrlLng . "species/nsr_taxon.php?id=" . $data['lng_id'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (isset($parameters['id']) && $parameters['id'] == 'i000359') {
                $redirectUrl = 'nieuws' . (isset($parameters['date']) ? '/' . $parameters['date'] : '');
            } else {
                if ($parameters['site'] == 'nsr') {
                    $redirectUrl = '/';
                }
            }
        }
    } else {
        if ($matches[1] == 'zoek') {
            $search = @$matches[3];
            if (isset($search)) {
                $redirectUrl = $baseUrlLng . 'search/nsr_search.php?search=' . $search;
            }
        } else {
            if ($matches[1] == 'natuurwidget') {
                $redirectUrl = $fixedUrls['natuurwidget'];
            } else {
                if (isset($fixedUrls[$matches[0]])) {
                    $redirectUrl = $fixedUrls[$matches[0]];
                } else {
                    //preg_match('/(nsr|nlsr|zoek)\/([a-zA-Z]*)\/([^\/]*)(\/)(.*)$/',$url,$matches);
                    preg_match('/(nsr|nlsr|zoek)\/([a-zA-Z]*)((\/)([^\/]*))(\/(.*))?/', $url, $matches);

                    $type = @$matches[2];
                    $nsrid = @$matches[5];
                    $tab = @$matches[7];

                    if (isset($nsrid)) {
                        $data = resolveNsrId($nsrid, $type);

                        switch ($data['item_type']) {
                            case "taxon":
                                $redirectUrl = $baseUrlLng . "species/nsr_taxon.php?id=" . $data['lng_id'] . "&cat=" . resolveTab($tab);
                                break;
                            case "name":
                                $redirectUrl = $baseUrlLng . "species/name.php?id=" . $data['lng_id'];
                                break;
                        }
                    }
                }
            }
        }
    }
} else {
    $redirectUrl = '/';

    // redirecting drupal 2016 theme search
    if (in_array('search_block_form', $parameters)) {
        if (!empty($parameters['search_block_form'])) {
            $redirectUrl = $baseUrlLng . 'search/nsr_search.php?search=' . $parameters['search_block_form'];
        } else {
            $redirectUrl = $baseUrlLng . 'search/nsr_search_extended.php';
        }
    }
}

$c->redirect($redirectUrl);
