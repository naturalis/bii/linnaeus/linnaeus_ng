describe('Empty Linnaeus project pages tests', () => {

  it('can show the app page', () => {
    cy.visit('https://linnaeus.dryrun.link');
    cy.contains('Welcome to the project.').should('exist');
    cy.contains('Linnaeus').should('exist');
  })
 
  it('can show the admin page', () => {
    cy.visit('https://linnaeus.dryrun.link/admin');
    cy.contains('Linnaeus NG Administration').should('exist');
    cy.contains('Your username').should('exist');
    cy.contains('Log in').should('exist');
  })

})

