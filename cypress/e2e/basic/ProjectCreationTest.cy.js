describe('Test the creation of a simple Linnaeus installation', () => {
  
  beforeEach(() => {
    // Login routine
    cy.visit('https://linnaeus.dryrun.link/admin');
    /* ==== Generated with Cypress Studio ==== */
    cy.get('#username').clear();
    cy.get('#username').type('sysadmin');
    cy.get(':nth-child(2) > :nth-child(2) > input').clear();
    cy.get(':nth-child(2) > :nth-child(2) > input').type('sysadmin');
    cy.get(':nth-child(4) > td > input').click();
    /* ==== End Cypress Studio ==== */
  });

  // Create a project called "Helianthus project"
  it('can create a project', () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get('.modules > :nth-child(1) > a').click();
    cy.get('.admin-list > :nth-child(1) > a').click();
    cy.get(':nth-child(1) > [colspan="2"] > input').clear('Project visible title');
    cy.get(':nth-child(1) > [colspan="2"] > input').type('Helianthus project');
    cy.get('[type="submit"]').click();
    /* ==== End Cypress Studio ==== */
    cy.visit('https://linnaeus.dryrun.link');
    cy.contains('Helianthus project').should('exist');
  })

  // We need a ResourceSpace key for this test
  it.skip('can create an introduction module', () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get('.modules > :nth-child(1) > a').click();
    cy.get('.admin-list > :nth-child(2) > a').click();
    cy.get('[onclick="moduleActivateModule(1)"] > .a').click();
    cy.get('[onclick="modulePublishModule(1)"] > .a').click();
    /* ==== End Cypress Studio ==== */
  })
  
  it('can create an taxon editor module', () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get('.modules > :nth-child(1) > a').click();
    cy.get('.admin-list > :nth-child(2) > a').click();
    cy.get('[onclick="moduleActivateModule(15)"] > .a').click();
    cy.get('[onclick="modulePublishModule(15)"] > .a').click();
    /* ==== End Cypress Studio ==== */
  })

})

