describe('NSR backend tests', () => {

  beforeEach(() => {
    cy.visit('https://www-b.nederlandsesoorten.nl/linnaeus_ng/admin/views/projects/overview.php');
  });
  
  it.skip('can create taxon', () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get('#username').clear();
    cy.get('#username').type('sysadmin');
    cy.get(':nth-child(2) > :nth-child(2) > input').clear();
    cy.get(':nth-child(2) > :nth-child(2) > input').type('password_here');
    cy.get(':nth-child(4) > td > input').click();
    /* ==== End Cypress Studio ==== */

    /* ==== Generated with Cypress Studio ==== */
    cy.get('.modules > :nth-child(4) > a').click();
    cy.get('#admin-menu-bottom > :nth-child(1) > [href="taxon_new.php"]').click();
    cy.get('#concept_rank_id').select('20');
    cy.get(':nth-child(3) > td > .edit').click();
    cy.get('#__parent_taxon_id_INPUT').clear();
    cy.get('#__parent_taxon_id_INPUT').type('fran{enter}');
    cy.get('ul > :nth-child(1) > a').click();
    /* ==== End Cypress Studio ==== */
  })
})

