describe('NSR frontend tests', () => {

  beforeEach(() => {
    cy.visit('https://www-b.nederlandsesoorten.nl/linnaeus_ng/app/views/search/nsr_search_extended.php?epi=1');
  });
  
  //it('', () => {})

  it.skip('can use filters', () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get('#presenceStatusList').select('presence[15]');
    /* ==== End Cypress Studio ==== */
    cy.contains('Acacia dealbata').should('be.visible');
  })
  
  it.skip('can find photo', () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get('#content > .searchBoxContainer > .tabs > :nth-child(2) > a > :nth-child(1)').click();
    cy.get(':nth-child(2) > label').click();
    cy.get('#photographer').click();
    cy.get(':nth-child(2) > .filter > .completeList').click();
    cy.get(':nth-child(141) > :nth-child(1) > a').click();
    /* ==== End Cypress Studio ==== */
    cy.contains('Bethylus boops').should('be.visible');
  })
  
  it.skip('can do taxonomic tree', () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get('#content > .searchBoxContainer > .tabs > :nth-child(3) > a > :nth-child(1)').click();
    cy.get('#node-116298 > [href="#"] > .sci-name').click();
    cy.get('#node-139992 > [href="#"] > :nth-child(1)').click();
    cy.get('#node-139993 > [href="#"] > .sci-name').click();
    cy.get('#node-139994 > [href="#"] > .sci-name').click();
    cy.get('#node-188038 > [href="#"] > .sci-name').click();
    cy.get('.italics').click();
    cy.get('#node-188040 > .detail-link').click();
    /* ==== End Cypress Studio ==== */
    cy.contains('Neodasys chaetonotoideus').should('be.visible');
  })
  
  it.skip('can download csv', () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get('.export > a').click();
    /* ==== End Cypress Studio ==== */
  })
})

