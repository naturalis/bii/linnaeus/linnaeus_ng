/* This changes the setting of the linnaeus theme to the integrated version. */
/* You should only use this migration on sites that are integrated with drupal sites. */
update module_settings_values set value='linnaeus_ng_integrated' where value='linnaeus_ng_responsive';
