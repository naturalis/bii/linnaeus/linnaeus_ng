ALTER TABLE media_taxon MODIFY original_name VARCHAR(1024);
ALTER TABLE media_taxon MODIFY file_name VARCHAR(1024);
ALTER TABLE media_taxon MODIFY thumb_name VARCHAR(1024);
