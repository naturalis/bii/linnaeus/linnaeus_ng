#!/bin/sh
set -eu

docker create \
    --rm \
    --name linnaeus_ng_test \
    --network linnaeus_ng_web \
    -v "$PWD":/usr/src/app \
    -v "$PWD/node_modules":/usr/src/app/node_modules \
    cypress/base sh -c \
  "set -eu
  getent hosts traefik | awk '{ print \$1\" linnaeus.dryrun.link\" }' >> /etc/hosts
  cd /usr/src/app

  npm install
  npx cypress install
  npx cypress run"

#docker network connect angular-bioportal_drupal angular-bioportal_cypress
docker start --attach linnaeus_ng_test 
