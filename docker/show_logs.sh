#!/bin/sh
set -eu

compose_version=$(wget -qO- https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4)
image_version=build_$CI_COMMIT_REF_SLUG

docker run --rm -v "$PWD":"$PWD":ro -v /var/run/docker.sock:/var/run/docker.sock:ro  -e "IMAGE_VERSION=$image_version" \
  docker/compose:"$compose_version" logs -t
