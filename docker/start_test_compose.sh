#!/bin/sh
set -eu

#compose_version=$(wget -qO- https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4)
compose_version="1.29.2"
image_version=${IMAGE_VERSION:-develop}
if [ ${VERSION+x} ]; then
    image_version=$VERSION
fi
if [ ${CI_COMMIT_REF_SLUG+x} ]; then
    image_version=build_$CI_COMMIT_REF_SLUG
fi
if [ -z ${CI_REGISTRY_IMAGE+x} ]; then
    CI_REGISTRY_IMAGE="registry.gitlab.com/naturalis/bii/linnaeus/linnaeus_ng"
fi

mkdir -p test_data/database/storage
if [ ${CI+x} ]; then
    chown 999:999 test_data/database/storage
fi
mkdir -p test_data/database/init
if [ ${CI+x} ]; then
    chown 82:82 test_data/database/init
fi
mkdir -p test_data/project/0001
if [ ${CI+x} ]; then
    chown 82:82 test_data/project
fi
chmod 777 test_data/project
cp database/init/initdb.sql.gz test_data/database/init/initdb.sql.gz 
if [ ${CI+x} ]; then
    chown 82:82 test_data/database/init/initdb.sql.gz
fi

override="$PWD/overrides/docker-compose.ci.yml"
if [ ${DEBUG+x} ]; then
    echo "OVERRIDE!"
    override="$PWD/overrides/docker-compose.dev-ci.yml"
fi

docker pull "$CI_REGISTRY_IMAGE/traefik:$image_version"
docker pull "$CI_REGISTRY_IMAGE/docker-proxy:$image_version"
docker pull "$CI_REGISTRY_IMAGE/nginx:$image_version"
docker pull "$CI_REGISTRY_IMAGE/php-fpm:$image_version"
docker pull "$CI_REGISTRY_IMAGE/database:$image_version"
docker run --rm -v "$PWD":"$PWD":ro -v /var/run/docker.sock:/var/run/docker.sock:ro  -e "IMAGE_VERSION=$image_version" \
  docker/compose:"$compose_version" -f "$PWD/docker-compose.yml" -f "$override" up -d
