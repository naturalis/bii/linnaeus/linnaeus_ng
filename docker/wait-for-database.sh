#!/bin/sh
# wait-for-database.sh

set -e
  
until (echo "SELECT * FROM pages_taxa" | mysql --host=database --user=develop --password=develop develop | grep "Description") do
  >&2 echo "Database is unavailable - sleeping"
  sleep 1
done
  
>&2 echo "Database is up - executing command"
exec "$@"
