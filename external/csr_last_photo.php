<?php

	class ExternalService {


		private $sett;
		private $conn;
		private $results;
		private $output;
		private $query;
		private $label;

		private $poolSize=20;

		const PID = 1;
		const BASE_URL_190x100 = 'https://images.naturalis.nl/190x100/';
		const URL_RECENT_IMAGES = 'https://dutchcaribbeanspecies.org/linnaeus_ng/app/views/search/nsr_recent_pictures.php';
		const URL_TAXON = "/linnaeus_ng/app/views/species/nsr_taxon.php?epi=%s&id=%s";
		const URL_IMAGE_POPUP = "/linnaeus_ng/app/views/species/nsr_taxon.php?epi=%s&id=%s&cat=media&img=%s";
		const LANGUAGE_DUTCH = 24;
		const PREDICATE_PREFERRED_NAME = 'isPreferredNameOf';
		const PREDICATE_VALID_NAME = 'isValidNameOf';

		public function setDatabaseSettings( $dbSettings )
		{
			// [ host, user, password, database, tablePrefix, characterSet ]
			$this->sett = (object)$dbSettings;
		}

		public function main()
		{
			$this->init();
			$this->dbConnect();
			$this->getProject();
			$this->getLastImage();
			$this->dbDisconnect();
		}

		public function httpHeaders()
		{
			header('Content-Type: application/json');
		}

		public function setParameters( $params )
		{
			if (isset($params['size']) && is_numeric($params['size']) && $params['size']>0 && $params['size']<1000)
			{
				$this->poolSize=$params['size'];
			}
		}

		public function getOutput( $jsonEncode=true )		
		{

			$this->output->pId=$this::PID;
			$this->output->project=$this->results['project'][0]->title;
			$this->output->exported=date('c');
			$this->output->url_recent_images=$this::URL_RECENT_IMAGES;
			$this->output->image=$this->results['last_image'][0];


			$this->output->image->url_taxon=sprintf($this::URL_TAXON,$this::PID,$this->output->image->taxon_id);
			$this->output->image->url_image_popup=sprintf($this::URL_IMAGE_POPUP,$this::PID,$this->output->image->taxon_id,$this->output->image->file_name);

			// should the site ever become multi-lingual, this should come from the translation table
			$this->output->labels=(object)[
				'taxon_link'=>'Display all data',
				'more_recent_link'=>'More recent photos',
				'lokatie'=>'Location',
				'fotograaf'=>'Photographer',
				'validator'=>'Validator',
				'date_created'=>'Date created',
			];

			return $jsonEncode ? json_encode( $this->output ) : $this->output;
		}

		public function getProject()		
		{

			$query="
				select
					*
				from
					" . $this->sett->tablePrefix . "projects
				where 
					id = " . $this::PID
				;

			$this->setQuery( $query );
			$this->setResultLabel( 'project' );
			$this->runQuery();
		}

		public function getLastImage()		
		{

			$query="
				select 
					_a.media_id

				from
					" . $this->sett->tablePrefix . "media_meta _a

				left join
					" . $this->sett->tablePrefix . "media_meta _meta9
						on _a.id=_meta9.media_id
						and _a.project_id=_meta9.project_id
						and _meta9.sys_label='verspreidingsKaart'

				where 
					_a.sys_label = 'beeldbankDatumAanmaak'
					and _a.project_id = " . $this::PID . "
					and ifnull(_meta9.meta_data,0)!=1

				order by 
					_a.meta_date desc

				limit ".$this->poolSize
				;


			$this->setQuery( $query );
			$this->setResultLabel( 'last_images' );
			$this->runQuery();

			$ids=[];
			foreach((array)$this->results['last_images'] as $val)
			{
				$ids[]=$val->media_id;
			}

			$this->conn->query("SET lc_time_names = 'en_US'");

	        $query="
				select
					_a.taxon_id,
					_a.id as media_id,
					concat('".$this::BASE_URL_190x100."',_a.file_name) as url_image,
					_a.file_name,
					_b.meta_data as copyright,
					_d.meta_data as fotograaf,
					date_format(_e.meta_date,'%e %M %Y') as date_created,
					_f.meta_data as lokatie,
					_g.meta_data as validator,
					_k.name as dutch_name,
					trim(replace(ifnull(_m.name,''),ifnull(_m.authorship,''),'')) as scientific_name

				from 
					" . $this->sett->tablePrefix . "media_taxon _a
				
				left join 
					" . $this->sett->tablePrefix . "names _k
						on _a.taxon_id=_k.taxon_id
						and _a.project_id=_k.project_id
						and _k.type_id=(
							select 
								id 
							from " . $this->sett->tablePrefix . "name_types 
							where 
								project_id =  " . $this::PID . " 
								and nametype='".$this::PREDICATE_PREFERRED_NAME."'
						)
						and _k.language_id= " . $this::LANGUAGE_DUTCH . "

				left join 
					" . $this->sett->tablePrefix . "names _m
						on _a.taxon_id=_m.taxon_id
						and _a.project_id=_m.project_id
						and _m.type_id=(
							select
								id 
							from " . $this->sett->tablePrefix . "name_types 
							where 
								project_id =  " . $this::PID . " 
								and nametype='".$this::PREDICATE_VALID_NAME."'
						)

				left join 
					" . $this->sett->tablePrefix . "media_meta _b
						on _a.id=_b.media_id 
						and _a.project_id=_b.project_id 
						and _b.sys_label = 'beeldbankCopyright'
				left join 
					" . $this->sett->tablePrefix . "media_meta _d
						on _a.id=_d.media_id 
						and _a.project_id=_d.project_id 
						and _d.sys_label = 'beeldbankFotograaf'
				left join 
					" . $this->sett->tablePrefix . "media_meta _e
						on _a.id=_e.media_id 
						and _a.project_id=_e.project_id 
						and _e.sys_label = 'beeldbankDatumVervaardiging'
				left join 
					" . $this->sett->tablePrefix . "media_meta _f
						on _a.id=_f.media_id 
						and _a.project_id=_f.project_id 
						and _f.sys_label = 'beeldbankLokatie'
				left join 
					" . $this->sett->tablePrefix . "media_meta _g
						on _a.id=_g.media_id 
						and _a.project_id=_g.project_id 
						and _g.sys_label = 'beeldbankValidator'

				where
					_a.project_id =  " . $this::PID . "
					and _a.id in (" . implode(',',$ids) . ")

				order by rand() limit 0,1
			
			";

			$this->setQuery( $query );
			$this->setResultLabel( 'last_image' );
			$this->runQuery();
		}

		private function init()
		{	
			$this->output=new stdClass;
		}

		private function  dbConnect()
		{
			$this->conn = new mysqli( $this->sett->host,$this->sett->user,$this->sett->password,$this->sett->database );		
			$this->conn->set_charset( $this->sett->characterSet );
		}

		private function dbDisconnect()
		{
			$this->conn->close();
		}

		private function setQuery( $query )
		{
			$this->query = $query;
		}

		private function setResultLabel( $label )
		{
			$this->label = $label;
		}

		private function runQuery()
		{
			if ($result = $this->conn->query($this->query))
			{
			    while ($row = $result->fetch_assoc())
			    {
			        $this->results[$this->label][]=(object)$row;
			    }
			    $result->close();
			}
		}
	}

