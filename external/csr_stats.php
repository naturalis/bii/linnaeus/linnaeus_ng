<?php

class ExternalService
{
    private $sett;
    private $conn;
    private $results;
    private $output;
    private $query;
    private $label;

    public const PID = 1;
    public const SPECIES_RANK = 74;
    public const LANGUAGE_DUTCH = 24;
    public const LANGUAGE_ENGLISH = 26;
    public const LANGUAGE_PAPIAMENTO = 81;

    public function setDatabaseSettings($dbSettings): void
    {
        // [ host, user, password, database, tablePrefix, characterSet ]
        $this->sett = (object)$dbSettings;
    }

    public function main(): void
    {
        $this->init();
        $this->dbConnect();
        $this->getCountAll();
        $this->getOccurrences();
        $this->getIndigenousness();
        $this->getHabitats();
        $this->getSpeciesWithPhotos();
        $this->getTaxaPhotos();
        $this->getNames();
        $this->getPhotographersAndValidators();
        $this->getLiterature();
        $this->dbDisconnect();
    }

    public function httpHeaders()
    {
        header('Content-Type: application/json');
    }

    private function formatNumber($number)
    {
        return number_format($number, 0, ",", ".");
    }

    public function getOutput($jsonEncode = true)
    {
        $b = [];

        $b['all'] = ['label' => $this->output->all->l, 'number' => $this->formatNumber($this->output->all->n)];
        $b['sub_cat']['label'] = 'of which';
        $b['sub_cat']['data'] = [
            ['label' => 'marine', 'number' => $this->formatNumber($this->output->marine->n)],
            ['label' => 'terrestrial/freshwater/brackish water', 'number' => $this->formatNumber($this->output->terrestrial->n)],
            ['label' => 'introduced species', 'number' => $this->formatNumber($this->output->introduced->n)]
        ];


        $b['islands']['label'] = 'Species per island';
        $b['islands']['icon'] = 'verspreidingskaarten.png';
        foreach ($this->output->islands->n as $island => $number) {
            if (in_array($island, ['Bonaire', 'Curaçao'])) {
                $island = '(Klein) ' . $island;
            }
            $b['islands']['data'][] = ['label' => $island, 'number' => $this->formatNumber($number)];
        }

        $b['species_photo_count'] = [
            'label' => $this->output->species_photo_count->l,
            'icon' => 'soortenmetfotos.png',
            'number' => $this->formatNumber($this->output->species_photo_count->n)
        ];
        $b['taxon_photo_count'] = [
            'label' => $this->output->taxon_photo_count->l,
            'icon' => 'fotos.png',
            'number' => $this->formatNumber($this->output->taxon_photo_count->n)
        ];

        foreach ($this->output->names->n as $language => $number) {
            $b[strtolower($language) . '_names'] = [
                'label' => $language . ' names',
                'icon' => 'nederlandsenamen.png',
                'number' => $this->formatNumber($number)
            ];
        }

        $b['validators'] = [
            'label' => $this->output->validators->l,
            'icon' => 'specialisten.png',
            'number' => $this->formatNumber($this->output->validators->n)
        ];
        $b['photographers'] = [
            'label' => $this->output->photographers->l,
            'icon' => 'specialisten.png',
            'number' => $this->formatNumber($this->output->photographers->n)
        ];
        $b['literature'] = [
            'label' => $this->output->literature->l,
            'icon' => 'literatuurbronnen.png',
            'number' => $this->formatNumber($this->output->literature->n)
        ];

        return $jsonEncode ? json_encode($b) : $b;
    }

    private function init()
    {
        $this->output = new stdClass();
    }

    private function dbConnect()
    {
        $this->conn = new mysqli($this->sett->host, $this->sett->user, $this->sett->password, $this->sett->database);
        $this->conn->set_charset($this->sett->characterSet);
    }

    private function dbDisconnect()
    {
        $this->conn->close();
    }

    private function setQuery($query)
    {
        $this->query = $query;
    }

    private function setResultLabel($label)
    {
        $this->label = $label;
    }

    private function runQuery()
    {
        if ($result = $this->conn->query($this->query)) {
            while ($row = $result->fetch_assoc()) {
                $this->results[$this->label][] = (object)$row;
            }
            $result->close();
        }
    }

    private function getCountAll()
    {
        $query = "
				select
					count(_t.id) as count

				from 
					" . $this->sett->tablePrefix . "taxa _t

				left join 
					" . $this->sett->tablePrefix . "trash_can _trash
						on _t.project_id = _trash.project_id
						and _t.id =  _trash.lng_id
						and _trash.item_type='taxon'

				right join
					" . $this->sett->tablePrefix . "projects_ranks _r
						on _t.project_id = _r.project_id
						and _t.rank_id =  _r.id	
				where
					_t.project_id =  " . $this::PID . "
					and _r.rank_id =  " . $this::SPECIES_RANK . "
					and ifnull(_trash.is_deleted,0)=0
				";

        $this->setQuery($query);
        $this->setResultLabel('all');
        $this->runQuery();

        $this->output->all = (object)['l' => 'Species occurring in Dutch Caribbean', 'n' => 0];
        $this->output->all->n = $this->results['all'][0]->count;
    }

    private function getOccurrences()
    {

        $query = "
				select
					count(distinct _ttv.taxon_id) as count
				from 
					" . $this->sett->tablePrefix . "traits_taxon_values _ttv

				right join 
					" . $this->sett->tablePrefix . "traits_values _tv
						on _ttv.project_id = _tv.project_id
						and _ttv.value_id = _tv.id

				left join 
					" . $this->sett->tablePrefix . "trash_can _trash
						on _ttv.project_id = _trash.project_id
						and _ttv.taxon_id =  _trash.lng_id
						and _trash.item_type='taxon'

				right join 
					" . $this->sett->tablePrefix . "taxa _t
						on _ttv.project_id = _t.project_id
						and _ttv.taxon_id = _t.id

				right join
					" . $this->sett->tablePrefix . "projects_ranks _r
						on _t.project_id = _r.project_id
						and _t.rank_id =  _r.id	

				where
					_ttv.project_id = " . $this::PID . "
					and _r.rank_id =  " . $this::SPECIES_RANK . "
					and ifnull(_trash.is_deleted,0)=0
					and _tv.string_value in ('%s')
				";

        $islands = [
            'Aruba' => ['1 Aruba', '2 Aruba'],
            '(Klein) Bonaire' => ['1 Bonaire', '1 Klein Bonaire', '2 Bonaire', '2 Klein Bonaire'],
            '(Klein) Curaçao' => ['1 Curaçao', '1 Klein Curaçao', '2 Curaçao', '2 Klein Curaçao'],
            'Saba' => ['1 Saba', '2 Saba'],
            'Saba Bank' => ['1 Saba Bank', '2 Saba Bank'],
            'St. Eustatius' => ['1 St. Eustatius', '2 St. Eustatius'],
            'St. Maarten' => ['1 St. Maarten', '2 St. Maarten']
        ];

        $this->output->islands = (object)['l' => 'Species per island', 'n' => []];

        foreach ($islands as $island => $traits) {
            $q = sprintf($query, implode("','", $traits));

            $this->setQuery($q);
            $this->setResultLabel($island);
            $this->runQuery();

            $this->output->islands->n[$island] = $this->results[$island][0]->count;
        }

        uksort($this->output->islands->n, function ($a, $b) {
            return trim(str_ireplace('(klein)', '', $a)) > trim(str_ireplace('(klein)', '', $b));
        });
    }

    private function getIndigenousness()
    {
        $query = "
				select
					count(distinct _ttv.taxon_id) as count, substring(_tv.string_value,1,1) as sub
				from 
					" . $this->sett->tablePrefix . "traits_taxon_values _ttv

				right join 
					" . $this->sett->tablePrefix . "traits_values _tv
						on _ttv.project_id = _tv.project_id
						and _ttv.value_id = _tv.id

				left join 
					" . $this->sett->tablePrefix . "trash_can _trash
						on _ttv.project_id = _trash.project_id
						and _ttv.taxon_id =  _trash.lng_id
						and _trash.item_type='taxon'

				right join 
					" . $this->sett->tablePrefix . "taxa _t
						on _ttv.project_id = _t.project_id
						and _ttv.taxon_id = _t.id

				right join
					" . $this->sett->tablePrefix . "projects_ranks _r
						on _t.project_id = _r.project_id
						and _t.rank_id =  _r.id	

				where
					_ttv.project_id = " . $this::PID . "
					and _r.rank_id =  " . $this::SPECIES_RANK . "
					and ifnull(_trash.is_deleted,0)=0
					and (
						substring(_tv.string_value,1,1)='1' || substring(_tv.string_value,1,1)='2'
					)
				group by 
					substring(_tv.string_value,1,1)
				";

        //Presence status Indigenous = 1 ... /  Introduced = 2 ...

        $this->setQuery($query);
        $this->setResultLabel('indigenousness');
        $this->runQuery();

        $this->output->indigenous = (object)['l' => 'indigenous species', 'n' => 0];
        $this->output->introduced = (object)['l' => 'introduced species', 'n' => 0];

        foreach ($this->results['indigenousness'] as $val) {
            if ($val->sub == '1') {
                $this->output->indigenous->n += $val->count;
            }
            if ($val->sub == '2') {
                $this->output->introduced->n += $val->count;
            }
        }
    }

    private function getHabitats()
    {
        $query = "
				select
					count(distinct _ttv.taxon_id) as count

				from 
					" . $this->sett->tablePrefix . "traits_taxon_values _ttv

				right join
					" . $this->sett->tablePrefix . "traits_values _tv
						on _ttv.project_id = _tv.project_id
						and _ttv.value_id = _tv.id

				left join
					" . $this->sett->tablePrefix . "trash_can _trash
						on _ttv.project_id = _trash.project_id
						and _ttv.taxon_id =  _trash.lng_id
						and _trash.item_type='taxon'

				right join 
					" . $this->sett->tablePrefix . "taxa _t
						on _ttv.project_id = _t.project_id
						and _ttv.taxon_id = _t.id

				right join
					" . $this->sett->tablePrefix . "projects_ranks _r
						on _t.project_id = _r.project_id
						and _t.rank_id =  _r.id	

				where
					_ttv.project_id = " . $this::PID . "
					and _r.rank_id = " . $this::SPECIES_RANK . "
					and ifnull(_trash.is_deleted,0)=0
					and _tv.string_value in ('%s')
				";

        $zones = [
            'marine' => ['Marine'],
            'terrestrial' => ['Terrestrial', 'Freshwater', 'Brackish water'],
        ];

        $this->output->marine = (object)['l' => 'marine', 'n' => 0];
        $this->output->terrestrial = (object)['l' => 'terrestrial', 'n' => 0];

        foreach ($zones as $zone => $traits) {
            $q = sprintf($query, implode("','", $traits));
            $this->setQuery($q);
            $this->setResultLabel($zone);
            $this->runQuery();

            $this->output->{$zone}->n = $this->results[$zone][0]->count;
        }
    }

    private function getSpeciesWithPhotos()
    {
        $query = "
				select 
					count(distinct taxon_id) as count
				from
					" . $this->sett->tablePrefix . "media_taxon _ttv
				left join
					" . $this->sett->tablePrefix . "trash_can _trash
						on _ttv.project_id = _trash.project_id
						and _ttv.taxon_id =  _trash.lng_id
						and _trash.item_type='taxon'
				left join
					" . $this->sett->tablePrefix . "taxa _t
						on _ttv.project_id = _t.project_id
						and _ttv.taxon_id =  _t.id
				left join
					" . $this->sett->tablePrefix . "projects_ranks _r
						on _ttv.project_id = _r.project_id
						and _t.rank_id =  _r.id	
				where
					_ttv.project_id =  " . $this::PID . "
					and _r.rank_id >=  " . $this::SPECIES_RANK . "
					and ifnull(_trash.is_deleted,0)=0
				";

        $this->setQuery($query);
        $this->setResultLabel('species_photo_count');
        $this->runQuery();
        $this->output->species_photo_count = (object)['l' => '(Sub)species with photos', 'n' => $this->results['species_photo_count'][0]->count];
    }

    private function getTaxaPhotos()
    {
        $query = "
				select 
					count(*) as count
				from
					" . $this->sett->tablePrefix . "media_taxon _ttv
				left join 
					" . $this->sett->tablePrefix . "trash_can _trash
						on _ttv.project_id = _trash.project_id
						and _ttv.taxon_id =  _trash.lng_id
						and _trash.item_type='taxon'
				left join
					" . $this->sett->tablePrefix . "taxa _t
						on _ttv.project_id = _t.project_id
						and _ttv.taxon_id =  _t.id

				where
					_ttv.project_id = " . $this::PID . "
					and ifnull(_trash.is_deleted,0)=0
				";

        $this->setQuery($query);
        $this->setResultLabel('taxon_photo_count');
        $this->runQuery();
        $this->output->taxon_photo_count = (object)['l' => 'Photos', 'n' => $this->results['taxon_photo_count'][0]->count];
    }

    private function getNames()
    {
        $query = "
				select 
					count(*) as count,language as value,language_id
				from 
					" . $this->sett->tablePrefix . "names 
				left join 
					" . $this->sett->tablePrefix . "languages 
						on names.language_id = languages.id
				where 
					project_id = " . $this::PID . "
					and language_id in (" . $this::LANGUAGE_DUTCH . "," . $this::LANGUAGE_ENGLISH . "," . $this::LANGUAGE_PAPIAMENTO . ")
				group by 
					language,language_id
				order by
					field(language_id," . $this::LANGUAGE_DUTCH . "," . $this::LANGUAGE_ENGLISH . "," . $this::LANGUAGE_PAPIAMENTO . ")
				";

        $this->setQuery($query);
        $this->setResultLabel('names_by_language');
        $this->runQuery();

        $this->output->names = (object)['l' => 'Names per language', 'n' => []];

        foreach ($this->results['names_by_language'] as $val) {
            $language = trim($val->value);
            $this->output->names->n[$language] = $val->count;
        }
    }

    private function getPhotographersAndValidators()
    {
        $query = "
				select 
					count(distinct meta_data) as count, sys_label as value
				from
					" . $this->sett->tablePrefix . "media_meta
				where
					project_id = " . $this::PID . " 
					and sys_label in ('beeldbankValidator','beeldbankFotograaf') 
				group by 
					sys_label
				";

        $this->setQuery($query);
        $this->setResultLabel('photographers_and_validators');
        $this->runQuery();

        $this->output->validators = (object)['l' => 'Identification specialists', 'n' => 0];
        $this->output->photographers = (object)['l' => 'Photographers', 'n' => 0];

        foreach ($this->results['photographers_and_validators'] as $val) {
            if (trim($val->value) == 'beeldbankValidator') {
                $this->output->validators->n = $val->count;
            }
            if (trim($val->value) == 'beeldbankFotograaf') {
                $this->output->photographers->n = $val->count;
            }
        }
    }

    private function getLiterature()
    {
        $query = "
				select 
					count(*) as count
				from 
					" . $this->sett->tablePrefix . "literature2 
				where 
					project_id = " . $this::PID . " 
				";

        $this->setQuery($query);
        $this->setResultLabel('literature');
        $this->runQuery();

        $this->output->literature = (object)['l' => 'Literature references', 'n' => $this->results['literature'][0]->count];
    }
}
