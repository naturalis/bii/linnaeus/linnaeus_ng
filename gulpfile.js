var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var concat = require('gulp-concat');
var terser = require('gulp-terser');
var log = require('fancy-log');
var modernizr = require('gulp-modernizr');

gulp.task('modernizr', function() {
  return gulp.src('node_modules/modernizr/src/*.js')
    .pipe(modernizr())
    .pipe(gulp.dest('node_modules/modernizr/'))
});
gulp.task('admin-bundle', function () {
    // bundeling the essential admin javascript
    return browserify({ entries: ['./gulp/admin.js']}).bundle()
     .pipe(source('bundle.js'))
     .pipe(buffer())
     .pipe(terser())
     .on('error', log)
     .pipe(gulp.dest('./public/admin/vendor'))
});
gulp.task('admin-copy', function () {
    // copying essential admin javascript plugins
    return gulp.src([
        'node_modules/dropzone/dist/**/*',
        'node_modules/prettyPhoto/**/*',
        'node_modules/ckeditor4/**/*',
        'node_modules/tinymce/**/*',
        'node_modules/jquery-ui-dist/**/*',
        'node_modules/nestedSortable/jquery.mjs.nestedSortable.js'
    ], { base: 'node_modules' })
        .pipe(gulp.dest('./public/admin/vendor'));
});
gulp.task('app-bundle', function () {
    // bundling essential app javascript
    return browserify({ entries: ['./gulp/app.js']}).bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(terser())
        .on('error', log)
        .pipe(gulp.dest('./public/app/vendor'));
});
gulp.task('app-copy', function (done) {
    // copying essential app javascript plugins
    gulp.src([
        'node_modules/prettyPhoto/**/*',
        'node_modules/raphael/**/*'
    ], { base: 'node_modules' })
        .pipe(gulp.dest('./public/app/vendor'));
    gulp.src([
        'node_modules/\@fancyapps/fancybox/dist/**/*'
    ], { base: 'node_modules/\@fancyapps/fancybox/dist/' })
        .pipe(gulp.dest('./public/app/vendor/fancybox'));
    done();
});
gulp.task('default',
    gulp.series(
        'admin-bundle',
        'admin-copy',
        'app-bundle',
        'app-copy')
);
