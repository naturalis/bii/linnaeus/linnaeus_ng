# Docker compose overrides

This directory contains the specific docker compose overrides.

## docker-compose.ci.yml

Used for setting up the smoke and unittesting.

## docker-compose.default.yml

Default override for 'default' installs, which are the normal solo and multiple
configurations of linnaeus.

## docker-compose.identify.yml

Variant of the default override needed for the ectoedemia site. See issue
[LINNA-1810](https://jira.naturalis.nl/projects/LINNA/issues/LINNA-1810).

Summary: One article "A Linnaeus NG TM interactive key to the
Lithocolletinae of North-West Europe aimed at accelerating the accumulation
of reliable biodiversity data (Lepidoptera, Gracillariidae)" links to
identify.naturalis.nl and it is essential this link is kept working.

## docker-compose.integrated.yml

The integrated variant forms a combination with a drupal setup on the same machine,
it does not contain a traefik component.

## docker-compose.dierenzoeker.yml

The dierenzoeker variant.

## docker-compose.dev-default.yml

A development version of the default variant.
Used for local development and testing purposes.

## docker-compose.dev-integrated.yml

Development version of the integrated variant.
Used for local development and testing purposes of the integrated combination with a drupal site.
