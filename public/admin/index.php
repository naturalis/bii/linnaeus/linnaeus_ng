<?php

$adminPosition = strpos($_SERVER['REQUEST_URI'], '/admin');
$routeBase = ($adminPosition > 0) ? substr($_SERVER['REQUEST_URI'], 0, $adminPosition) : '';

header("Location:$routeBase/admin/views/projects/choose_project.php");
