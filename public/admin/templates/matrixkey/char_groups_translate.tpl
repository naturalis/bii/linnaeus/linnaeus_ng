{include file="../shared/admin-header.tpl"}

<style>
    table#language-form tr:first-child td {
        font-weight: bold;
    }
    table#language-form td {
        padding: 0 10px 2px 0;
    }

</style>

<div id="page-main">

	<p>{t}Add translations for character group names{/t}:</p>

    <form method="post">
        <table id="language-form">
            <tr>
                {foreach from=$languages v k}
                    <td>{$v.language}</td>
                {/foreach}
            </tr>
            {foreach from=$group_labels group group_id}
                {counter assign=i start=0 print=false}
                <tr>
                {foreach from=$group label lang_id}
                    {counter}
                    <td>{if $i == 1}{$label}{else}<input type="text" name="{$group_id}-{$lang_id}" value="{$label}"/>{/if}</td>
                {/foreach}
                </tr>
            {/foreach}
        </table>
        <p>
            <input type="submit" value="save" />
        </p>
    </form>

    <div>
        <a href="char_groups.php">{t}Organize characters in groups{/t}</a><br>
        <a href="char_groups_sort.php">{t}Change the display order of groups{/t}</a>
    </div>


</div>

{include file="../shared/admin-messages.tpl"}
{include file="../shared/admin-footer.tpl"}
