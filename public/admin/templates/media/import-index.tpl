{include file="../shared/admin-header.tpl"}
{include file="../shared/admin-messages.tpl"}

<div id="media-main">

    {if $file_name}
        {assign "action" "import.php"}
    {else}
        {assign "action" ""}
    {/if}

    <form method="post" action="{$action}" id="theForm" enctype="multipart/form-data">

    {if !$file_name}
        <p>
            Import images into Linnaeus using a csv file. The csv file should contain the following columns:
            <ol>
                <li>file_name</li>
                <li>new_file_name</li>
                <li>taxon</li>
                <li>caption</li>
                <li>overview (values: yes/no)</li>
            </ol>
        </p>
            <p>The new_file_name column offers the option to rename files. If this is not necessary, the names should be identical to those in the file_name column.</p>
            <p>Images can be uploaded to the server using the <a href="../files/index.php">File Management</a> module. The import expects them at:
            <b>www/{$session.admin.project.urls.project_media|replace:'../':''}</b>
            </p>
        <p style="margin-top:25px;">
            CSV-file to load: <input name="importFile" type="file" />&nbsp;*<br />
        </p>

    {else}

        <p>
            <b>CSV-file "{$file_name}" loaded.</b>
            (<span class="a" onclick="javascript:history.go(-1)">load another file</span>)
        </p>

        {if $missing_files}
            <p style="color: red;">The following {$missing_files|@count} files cannot be located:</p>
            <ul style="margin: 0;">
                {foreach $missing_files v k}
                    <li>{$v}</li>
                {/foreach}
            </ul>
        {/if}

        {if $missing_taxa}
            <p style="color: red;">The following {$missing_taxa|@count} taxa cannot be located:</p>
            <ul style="margin: 0;">
                {foreach $missing_taxa v k}
                    <li>{$v}</li>
                {/foreach}
            </ul>
        {/if}

    {/if}

    <p>
    {if !$file_name}
        {assign "label" "upload and verify csv file"}
    {else if $missing_files}
        {assign "label" "import anyway"}
    {else}
        {assign "label" "import"}
    {/if}
    <input type="submit" value="{$label}" />
    </p>
	</form>


</div>

{literal}
<script type="text/JavaScript">
$(document).ready(function() {
	$("#submit").click(function() {
		$("#theForm").submit();
	});
});
</script>
{/literal}

{include file="../shared/admin-footer.tpl"}
