{include file="../shared/admin-header.tpl"}

<style>
.small {
	color:#666;
	font-size: 1.0em;
}
li.main-list {
	padding-bottom:1px;
	margin-bottom:1px;
	border-bottom:1px solid #ddd;
	list-style-type:none;
	width: 80%;
}
table.lines {
	border-collapse:collapse;
	width: 100%;
}

table.lines tr td:nth-child(1) {
	min-width:300px;
}
table.lines tr td:nth-child(2) {  
	min-width:150px;
}
table.lines tr td:nth-child(3) {  
	min-width:200px;
}
.warnings {
	color:orange;
}
.errors {
	color:red;
}
div.messages {
	margin-left:10px;
}
.variable-list {
	color:#36F;
}
</style>


{include file="../shared/admin-messages.tpl"}


<div id="page-main">


{if $lines}

<h4>{t}Parsed lines:{/t}</h4>

<form method="post" action="import_status_file_process.php">
<input type="hidden" name="rnd" value="{$rnd}" />
<input type="hidden" name="action" value="save" />

<ul style="padding-left:0px;">
	<li class="main-list">
    	<table class="lines"><tr>
        	<td><span class="small">{t}taxon{/t}</span></td>
			<td><span class="small">{t}presence status{/t}</span></td>
	        <td><span class="small">{t}import status{/t}</span></td>
		</tr></table>
	</li>
    {$willsave=false}
	{foreach $lines v k}
    {if !$willsave && !$v.errors}{$willsave=true}{/if}
	<li class="main-list">
    	<table class="lines"><tr>
    		<td>{$v[$importColumns['conceptName']]}</td>
			<td>{$v[$importColumns['status']]}</td>
	        <td>
            	{if $v.errors}
                	<span class="errors">{t}errors, will not import{/t}</span>
                {else if $v.warnings}
                	<span class="warnings">{t}will import, with warnings{/t}</span><br />
                    <label><input type="checkbox" value="{$v.line_id}" name="do_not_import[]" />{t}do not import{/t}</label>
                {else}&#10003;{/if}
			</td>
		</tr></table>
        {if $v.warnings}
        <div class="messages">
        <span class="warnings">warning(s):</span>
        <ul>
        {foreach $v.warnings e}
        <li>
        	{$e.message}
        	{if $e.data}{$i=0}({foreach $e.data d dk}{if $i++>0}, {/if}{$dk}: {$d}{/foreach}){/if}
        </li>
        {/foreach}
        </ul>
        </div>
        {/if}
        {if $v.errors}
        <div class="messages">
        <span class="errors">error(s):</span>
        <ul>
        {foreach $v.errors e}
        <li>
        	{$e.message}
        	{if $e.data}{$i=0}({foreach $e.data d dk}{if $i++>0}, {/if}{$dk}: {$d}{/foreach}){/if}
		</li>
        {/foreach}
        </ul>
        </div>
        {/if}
	</li>
	{/foreach}
</ul>
{if $willsave}
<input type="submit" value="{t}save{/t}" />
{else}
{t}(nothing to save){/t}
{/if}
</form>

<p>

<a href="import_status_file_reset.php">{t}load a new file{/t}</a>

</p>

{else}

<h4>{t}Import a file:{/t}</h4>

<form method="post" action="" enctype="multipart/form-data">

	<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
	<input name="uploadedfile" type="file" /><br />

	<p>
	{t}Choose CSV field delimiter:{/t}
    <ul>
    	<li><label><input type="radio" name="delimiter" value="comma" checked="checked"/>, {t}(comma){/t}</label></li>
        <li><label><input type="radio" name="delimiter" value="semi-colon"/>; {t}(semi-colon){/t}</label></li>
        <li><label><input type="radio" name="delimiter" value="tab"/>{t}tab{/t}</label></li>
	</ul>
	</p>

	<p>
	<input type="submit" value="{t}upload{/t}" />
    </p>
</form>

<p>
{t}Use this import to link presence statuses to existing taxa.{/t}
{t}File and data must meet the following conditions:{/t}
</p>
<ul>
	<li>
    	{t}The format needs to be CSV.{/t}
        {t}Please take into account the following:{/t}
        <ul>
            <li>
            	{t}The field delimiter must be a comma, semi-colon or tab.{/t}
                {t}Although the name of the filetype suggests differently (the "CS" in CSV stands for "comma separated"), it is not always obvious by what character the fields are actually separated when saving as CSV from MS Excel or other spreadsheet programs. Sometimes the program allows you to choose the delimiter when exporting, more often it chooses the delimiting character for you.{/t}
                {t}If you are unsure what the delimiting character in your file is, please open it in a plain text-editor (like Notepad) to check, after you have saved it from your spreadsheet program.{/t}
            	{t}The example file below uses a semi-colon as delimiting character. This is also the default setting when importing; you can change it by selecting another delimiter above.{/t}
			</li>
            <li>{t}The fields in the CSV-file may be enclosed by " (double-quotes), but this is not mandatory.{/t}</li>
        </ul>
    </li>
	<li>{t}There should be one taxon per line. No header line should be present.{/t}</li>
	<li>{t}Each line for a taxon import should consist of the following fields, in this order:{/t}
		<ol>
			<li>{t}Valid scientific name{/t} {t}(mandatory){/t}</li>
			<li>{t}Presence status{/t} {t}(mandatory){/t}</li>
		</ol>
	</li>
	<li>{t}Presence statuses should match one the following codes:{/t}
		<ul class="variable-list">
			{foreach from=$statuses item=v}
				{if $v.index_label!=99}
					<li>{$v.index_label} = {$v.label}</li>
				{/if}
			{/foreach}
		</ul>
	</li>
</ul>

{/if}
</div>
</form>

{include file="../shared/admin-footer.tpl"}
