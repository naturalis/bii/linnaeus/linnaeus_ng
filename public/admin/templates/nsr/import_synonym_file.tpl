{include file="../shared/admin-header.tpl"}

<style>
.small {
	color:#666;
	font-size: 1.0em;
}
li.main-list {
	padding-bottom:1px;
	margin-bottom:1px;
	border-bottom:1px solid #ddd;
	list-style-type:none;
}
table.lines {
	border-collapse:collapse;
	width: 100%;
}
table.lines tr td:nth-child(1) {  
	min-width:300px;
}
table.lines tr td:nth-child(2) {  
	min-width:200px;
}
table.lines tr td:nth-child(3) {  
	min-width:100px;
}
table.lines tr td:nth-child(4) {  
	min-width:210px;
}
table.lines tr td:nth-child(5) {
	min-width:150px;
}
table.lines tr td:nth-child(6) {
	min-width:150px;
}
.warnings {
	color:orange;
}
.errors {
	color:red;
}
div.messages {
	margin-left:10px;
}
.variable-list {
	color:#36F;
}
</style>


{include file="../shared/admin-messages.tpl"}


<div id="page-main">


{if $lines}

<h4>{t}Parsed lines:{/t}</h4>


<form method="post" action="import_synonym_file_process.php">
<input type="hidden" name="rnd" value="{$rnd}" />
<input type="hidden" name="action" value="save" />

<ul style="padding-left:0px;">
	<li style="list-style-type:none;">
    	<table class="lines"><tr>
        	<td><span class="small">{t}synonym{/t}</span></td>
			<td><span class="small">{t}authorship{/t}</span></td>
			<td><span class="small">{t}rank{/t}</span></td>
        	<td><span class="small">{t}valid taxon{/t}</span></td>
			<td><span class="small">{t}name type{/t}</span></td>
			<td><span class="small">{t}import status{/t}</span></td>
		</tr></table>
	</li>
    {$willsave=false}
	{foreach $lines v k}
    {if !$willsave && !$v.errors}{$willsave=true}{/if}
	<li class="main-list">
    	<table class="lines"><tr>
    		<td>{$v[$importColumns['synonym']]}</td>
			<td>{$v[$importColumns['authorship']]}</td>
			<td>{$v[$importColumns['rank']]}</td>
        	<td>{$v[$importColumns['conceptName']]}</td>
	        <td>{$v[$importColumns['nameType']]}</td>
	        <td>
            	{if $v.errors}
                	<span class="errors">{t}errors, will not import{/t}</span>
                {else if $v.warnings}
                	<span class="warnings">{t}will import, with warnings{/t}</span><br />
                    <label><input type="checkbox" value="{$v.line_id}" name="do_not_import[]" />{t}do not import{/t}</label>
                {else}&#10003;{/if}
			</td>
		</tr></table>
        {if $v.warnings}
        <div class="messages">
        <span class="warnings">warning(s):</span>
        <ul>
        {foreach $v.warnings e}
        <li>
        	{$e.message}
        	{if $e.data}{$i=0}({foreach $e.data d dk}{if $i++>0}, {/if}{$dk}: {$d}{/foreach}){/if}
        </li>
        {/foreach}
        </ul>
        </div>
        {/if}
        {if $v.errors}
        <div class="messages">
        <span class="errors">error(s):</span>
        <ul>
        {foreach $v.errors e}
        <li>
        	{$e.message}
        	{if $e.data}{$i=0}({foreach $e.data d dk}{if $i++>0}, {/if}{$dk}: {$d}{/foreach}){/if}
		</li>
        {/foreach}
        </ul>
        </div>
        {/if}
	</li>
	{/foreach}
</ul>
{if $willsave}
<input type="submit" value="{t}save{/t}" />
{else}
{t}(nothing to save){/t}
{/if}
</form>

<p>

<a href="import_synonym_file_reset.php">{t}load a new file{/t}</a>

</p>

{else}

<h4>{t}Import a file:{/t}</h4>

<form method="post" action="" enctype="multipart/form-data">

	<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
	<input name="uploadedfile" type="file" /><br />

	<p>
	{t}Choose CSV field delimiter:{/t}
    <ul>
    	<li><label><input type="radio" name="delimiter" value="comma" checked="checked"/>, {t}(comma){/t}</label></li>
        <li><label><input type="radio" name="delimiter" value="semi-colon"/>; {t}(semi-colon){/t}</label></li>
        <li><label><input type="radio" name="delimiter" value="tab"/>{t}tab{/t}</label></li>
	</ul>
	</p>

	<p>
	<input type="submit" value="{t}upload{/t}" />
    </p>
</form>


	<p>
		{t}This import is largely similar to the <a href="import_taxon_file.php">taxon import</a>. The differences are:{/t}<br />
	</p>
	<ul>
		<li>{t}Each line for a synonym import should consist of the following fields, in this order:{/t}
			<ol>
				<li>{t}Synonym{/t} {t}(mandatory){/t}</li>
				<li>{t}Authorship{/t} {t}(optional){/t}</li>
				<li>{t}Rank{/t} {t}(mandatory){/t}</li>
				<li>{t}Valid scientific name{/t} {t}(mandatory){/t}</li>
				<li>{t}Name type{/t} {t}(mandatory){/t}</li>
			</ol>
		</li>
		<li>
			{t}The valid taxon to link the synonym to should be present, it will not be created on-the-fly.{/t}
			{t}Ranks should match those listed under <a href="import_taxon_file.php">taxon import</a>.{/t}
		</li>
		<li>{t}Name types should match one the following:{/t}
			<ul class="variable-list">
				{foreach from=$nameTypes item=v}
					<li>{$v.nametype_label}</li>
				{/foreach}
			</ul>
		</li>
		<li>
			{t}Rank of the synonym, valid taxon and name type should all be part of your project, otherwise the synonym will not be loaded!{/t}
		</li>
	</ul>

	<p>
		<a href="../../media/system/example-synonym-import-file.csv">{t}download a sample file{/t}</a><br>
		This example file can be used in conjunction with the taxon example import. <strong>Note that the name type must match the language of your project.</strong>
	</p>

{/if}
</div>
</form>

{include file="../shared/admin-footer.tpl"}
