{include file="../shared/admin-header.tpl"}

<style>
table tr {
	vertical-align:text-top;
}
table tr th {
	text-align:right;
}
</style>


<div id="page-main">

	{if $user.id}
    <h2>{t}edit{/t} {$user.first_name} {$user.last_name}</h2>
	{else}
    <h2>{t}new user{/t}</h2>
    {/if}

    <form id="theForm" method="post" onsubmit="return submitUserEditForm();">
    <input id="id" name="id" value="{$user.id}" type="hidden" />
    <input type="hidden" name="action" id="action" value="save"  />
    <input type="hidden" name="rnd" value="{$rnd}" />

    <table>
        <tr>
            <th>{t}Username:{/t}</th>
            <td>
            {if $user.id==$current_user || $can_edit}
            <input disabled type="text" id="username" name="username" value="{$user.username}" />
            {else}
            {$user.username}
            {/if}
            </td>
        </tr>
        {if $user.id==$current_user || $can_edit}
        <tr>
            <th>{t}Password:{/t}</th>
            <td><input type="password" id="password" name="password" value="" /></td>
        </tr>
        <tr>
            <th>{t}Password (repeat):{/t}</th>
            <td><input type="password" id="password_repeat" name="password_repeat" value="" /></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
		{/if}
    </table>

    <p>
		<input type="submit" value="save" /> &nbsp;&nbsp;
    </p>

    </form>

    <p>
        <a href="javascript:history.back()">{t}back{/t}</a>
    </p>

</div>

{include file="../shared/admin-messages.tpl"}

<script>
$(document).ready(function()
{
	roleID_sysAdmin={$smarty.const.ID_ROLE_SYS_ADMIN};
	roleID_leadExpert={$smarty.const.ID_ROLE_LEAD_EXPERT};
	roleID_editor={$smarty.const.ID_ROLE_EDITOR};

	$('#username').focus();

	{if !$user.id}

	$('#roles').on('change',function()
	{
		if ($('#roles :selected').val() < {$smarty.const.ID_ROLE_EDITOR})
		{
			$('[name=can_publish][value=1]').prop('checked',true);
		}
		else
		{
			$('[name=can_publish][value=0]').prop('checked',true);
		}
		setPermissions();
	});

	$('#roles').trigger('change');

	{/if}


});
</script>

{include file="../shared/admin-footer.tpl"}
