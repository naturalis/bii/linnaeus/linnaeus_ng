<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\Admin\Controllers\ActorsController;

$c = new ActorsController();

$c->indexAction();
