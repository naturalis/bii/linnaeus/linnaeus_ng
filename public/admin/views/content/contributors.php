<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\Admin\Controllers\ContentController;

$c = new ContentController();
$c->contributorsAction();
