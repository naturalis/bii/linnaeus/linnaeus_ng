<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\Admin\Controllers\FileManagementController;

$c = new FileManagementController();
$c->indexAction();
