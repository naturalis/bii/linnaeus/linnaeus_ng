<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\Admin\Controllers\GlossaryController;

$c = new GlossaryController();
$c->setExcludeFromReferer(true);
$c->setNoResubmitvalReset(true);
$c->ajaxInterfaceAction();
