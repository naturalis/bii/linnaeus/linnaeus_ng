<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\Admin\Controllers\ExportAppController;

$c = new ExportAppController();
$c->deleteUnusedScriptAction();
