<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\Admin\Controllers\ExportController;

$c = new ExportController();
$c->exportAction();
