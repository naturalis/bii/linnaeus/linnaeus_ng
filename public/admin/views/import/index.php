<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\ImportController;

$c = new ImportController();

$c->indexAction();
