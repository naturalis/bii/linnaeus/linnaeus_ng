<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\ImportL2Controller;

$c = new ImportL2Controller();

$c->l2KeysAction();
