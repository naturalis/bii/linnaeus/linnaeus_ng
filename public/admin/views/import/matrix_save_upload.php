<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\ImportMatrixController;

$c = new ImportMatrixController();

$c->saveUploadAction();
