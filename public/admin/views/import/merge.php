<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\MergeController;

$c = new MergeController();

$c->mergeAction();
