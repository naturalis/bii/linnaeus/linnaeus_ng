<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\Admin\Controllers\IndexController;

$c = new IndexController();
$c->setExcludeFromReferer(true);
$c->setNoResubmitvalReset(true);
$c->ajaxInterfaceAction();
