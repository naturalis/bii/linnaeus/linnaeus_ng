<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\IntroductionController;

$c = new IntroductionController();

$c->editAction();
