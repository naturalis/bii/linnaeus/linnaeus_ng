<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\Literature2Controller;

$c = new Literature2Controller();

$c->bulkProcessAction();
