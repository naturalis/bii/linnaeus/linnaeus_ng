<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\MatrixKeyExtController;

$c = new MatrixKeyExtController();

$c->overviewAction();
