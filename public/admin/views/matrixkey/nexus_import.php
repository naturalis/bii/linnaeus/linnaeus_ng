<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\ImportNexusController;

$c = new ImportNexusController();

$c->indexAction();
