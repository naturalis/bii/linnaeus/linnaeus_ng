<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\ImportSDDController;

$c = new ImportSDDController();

$c->sddImport2Action();
