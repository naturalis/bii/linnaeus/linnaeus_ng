<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\MediaConverterController;

$c = new MediaConverterController();

$c->initAction();
