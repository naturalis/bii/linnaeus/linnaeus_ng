<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\MediaImportController;

$c = new MediaImportController();

$c->initAction();
