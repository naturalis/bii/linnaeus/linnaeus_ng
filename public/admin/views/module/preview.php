<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\FreeModuleController;

$c = new FreeModuleController();

$c->previewAction();
