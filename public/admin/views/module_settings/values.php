<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\ModuleSettingsController;

$c = new ModuleSettingsController();

$c->valuesAction();
