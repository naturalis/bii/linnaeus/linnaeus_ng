<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\NsrTaxonActorsController;

$c = new NsrTaxonActorsController();

$c->actorsAction();
