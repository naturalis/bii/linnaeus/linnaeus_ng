<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\NsrTaxonManagement;

$c = new NsrTaxonManagement();
$c->setExcludeFromReferer(true);
$c->setNoResubmitvalReset(true);
$c->ajaxInterfaceAction();
