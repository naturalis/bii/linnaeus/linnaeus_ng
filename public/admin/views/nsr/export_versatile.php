<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\VersatileExportController;

$c = new VersatileExportController();

$c->exportAction();
