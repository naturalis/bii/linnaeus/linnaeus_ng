<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\NsrTaxonImagesController;

$c = new NsrTaxonImagesController();

$c->imageDataAction();
