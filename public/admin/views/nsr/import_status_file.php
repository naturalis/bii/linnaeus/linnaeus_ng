<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\NsrTaxonImportController;

$c = new NsrTaxonImportController();

$c->importStatusFileAction();
