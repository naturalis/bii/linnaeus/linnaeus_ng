<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\NsrTaxonImportController;

$c = new NsrTaxonImportController();

$c->importFileResetAction('import_status_file.php');
