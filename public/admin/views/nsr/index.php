<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\NsrTreeController;

$c = new NsrTreeController();

$c->indexAction();
