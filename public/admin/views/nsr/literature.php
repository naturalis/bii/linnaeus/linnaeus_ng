<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\NsrTaxonLiteratureController;

$c = new NsrTaxonLiteratureController();

$c->literatureAction();
