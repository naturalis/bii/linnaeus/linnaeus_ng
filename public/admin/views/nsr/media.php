<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\NsrTaxonMediaController;

$c = new NsrTaxonMediaController();

$c->mediaAction();
