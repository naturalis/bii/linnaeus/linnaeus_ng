<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\NsrPaspoortController;

$c = new NsrPaspoortController();

$c->paspoortAction();
