<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\NsrTaxonDeleteController;

$c = new NsrTaxonDeleteController();

$c->irrevocablyDeleteAction();
