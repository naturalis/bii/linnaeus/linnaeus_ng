<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\NsrActivityLogController;

$c = new NsrActivityLogController();

$c->indexAction();
