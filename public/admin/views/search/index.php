<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\SearchController;

$c = new SearchController();

$c->indexAction();
