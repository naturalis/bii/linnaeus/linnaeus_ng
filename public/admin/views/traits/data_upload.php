<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\TraitsDataController;

$c = new TraitsDataController();

$c->dataUploadAction();
