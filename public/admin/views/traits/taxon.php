<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\TraitsTaxonController;

$c = new TraitsTaxonController();

$c->taxonAction();
