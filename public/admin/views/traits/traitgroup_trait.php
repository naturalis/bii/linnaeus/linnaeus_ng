<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\TraitsTraitsController;

$c = new TraitsTraitsController();

$c->traitgroupTraitAction();
