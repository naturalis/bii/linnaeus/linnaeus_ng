<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\LoginController;

$c = new LoginController();

$c->setExcludeFromReferer(true);

$c->loginAction();
