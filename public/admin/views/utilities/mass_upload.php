<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Linnaeus\Admin\Controllers\UtilitiesController;

$c = new UtilitiesController();

$c->massUploadAction();
