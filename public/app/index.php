<?php

$appPosition = strpos($_SERVER['REQUEST_URI'], '/app');
$routeBase = ($appPosition > 0) ? substr($_SERVER['REQUEST_URI'], 0, $appPosition) : '';

header("Location:$routeBase/app/views/linnaeus/set_project.php");
