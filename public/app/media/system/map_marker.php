<?php

// this file exists solely to avoid the need to parametrise the imagepath in the mapkey.js javascript file

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\Controller;

$c = new Controller();

require_once 'skins/' . $c->generalSettings['app']['skinName'] . '/map_marker.php';
