<!DOCTYPE html>
<html>
		<head>
		<title>Dierenzoeker</title>
		<meta property="og:description" content="Zie je een dier in je huis of tuin, en weet je niet wat het is? Kijk goed en ontdek het in de Dierenzoeker."/>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="../../style/dierenzoeker/basics.css" />
		<link rel="stylesheet" type="text/css" href="../../style/dierenzoeker/jquery-ui-1.10.0.custom.min.css" />
		<link rel="stylesheet" type="text/css" href="../../style/dierenzoeker/prettyPhoto/prettyPhoto.css" />
    {if $googleAnalyticsCode}
		{include file="../shared/_google_analytics_code.tpl"}
	{/if}

	<link href="/linnaeus_ng/app/media/system/skins/dierenzoeker/favicon.ico" type="image/x-icon" rel="icon" />
    <link href="/linnaeus_ng/app/media/system/skins/dierenzoeker/favicon.ico" type="image/x-icon" rel="shortcut icon" />        

		<script type="text/javascript" src="../../javascript/jquery-3.0.0.min.js"></script>
		<script type="text/javascript" src="../../javascript/project_specific/backstretch.min.js"></script>

		</head>
        
        
        
        
    <body style="background-position-y:115px;background: url('../../media/system/skins/dierenzoeker/background_blurry.jpg');">       
    {if $googleAnalyticsCode}
        {include file="../shared/_google_analytics_frame.tpl"}
    {/if}
        <div class="main-wrapper">
            
            <div class="header" style="height:115px;">
                <div class="header-inner">

		            {include file="../shared/top-banners.tpl"}


                </div><!-- /header-inder -->            
            </div><!-- /header -->                        
            <div class="sub-header-wrapper" style="padding-bottom:1px;height:60%">
                <div class="sub-header" style="padding-bottom:200px;width:793px;margin-left:auto;margin-right:auto;height:100%">
                    <div class="sub-header-inner extra-inner" style="position:static;margin-left:0;padding:50px;padding-top:20px;width:693px;padding-top:0px;">


		{$page.content}


<script type="text/JavaScript">
$(document).ready(function(){
	document.title='{$headerTitles.title|@escape}';
});
</script>
{literal}

				<script language="JavaScript">
					$(function(){
						$(".optv-wrapper, .onderwijs-wrapper").append("<div class='clearer' />");
						$(".optv-popup-link").click(function(e){
							e.preventDefault();                                    
							openStream($(this).attr("href"))
							return false;
						});
						$(".extra-inner").find("p:first").css("width", "620px");
						
						$(".extra-inner p").css("font-weight", "normal");
														
						$(".onderwijs-popup-link").attr("target", "_blank");
						
						$.backstretch("../../media/system/skins/dierenzoeker/background_blurry.jpg");
					});
					
					//Copied from hetklokhuis.nl:
					
					function openWindow(p,n,w,h)
					{
						//console.log(w,h);                                
						var win = window.open(p, n, "width=" + w + ",height=" + h +",toolbar=no,location=no,directories=no,status=0,resizable=no,scrollbars=no,menubar=no");
						//win.moveTo(((screen.width/2)-(w/2)),((screen.height/2)-(h/2)));
					}
					
					function openStream(p)
					{
						var n = 'klokhuisstream';
						var w = 1024;
						var h = 700;
						openWindow( p, n, w, h);
					}
					
				</script>
			 </div>
			</div> 
		</div>
{/literal}
        
        {include file="../shared/bottom-banners.tpl"}

{literal}

       </div>
                    
    </body>
<script>
        $(document).ready(function(e)
        {
            $.backstretch("../../media/system/skins/dierenzoeker/background.jpg");
        });
</script>
{/literal}
</html>
