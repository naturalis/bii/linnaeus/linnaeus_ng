<script type="text/javascript">

    var url;
    var max=10;
    var results=[];
    var general_label="Visit page";
    var remoteServiceUrl="../../../shared/tools/remote_service.php";
    var spatial_filter;

    function draw()
    {
        if (results.length > 0) {
            var b = [];
            for (var i = 0; i < results.length; i++) {
                if (i >= max) continue;
                var tpl = fetchTemplate('aSoundTpl');
                b.push(
                    tpl
                        .replace(/%MP3-PATH%/, results[i].file)
                        .replace(/%LABEL%/, general_label)
                        .replace(/%URL%/g, results[i].url)
                        .replace(/%LICENSE%/g, results[i].lic)
                        .replace(/%CREATOR%/g, results[i].rec)
                        .replace(/%SPATIAL%/g, results[i].loc)
                        .replace(/%TEMPORAL%/g, results[i].date)
                );
            }
            if (results.length > max) {
                var xcLinkTpl = fetchTemplate('xcLinkTpl');
                b.push(
                    xcLinkTpl.replace(/%TOTAL%/, results.length)
                )
            }
            $('#results').html(b.join("\n"));

        // No results; present link to XC
        } else {
            $('#results').html(fetchTemplate('noResultsTpl'));
        }
    }

    function run()
    {
        url += "+area:europe";
        $.ajax({
            url : remoteServiceUrl,
            type: "POST",
            data: ({
                url : url,
                original_headers : 1,
                request_headers: "Accept: application/json"
            }),
            success : function(data)
            {
                results = data.recordings;
                draw();
            },
            complete : function( jqXHR, textStatus )
            {
                if( textStatus != "success" )
                {
                    $( '#results' ).html( fetchTemplate( 'errorOccurredTpl' ).replace( '%STATUS%', jqXHR.responseText ? jqXHR.responseText : textStatus ) );
                }
            }
        });
    }

    $(document).ready(function()
    {
        acquireInlineTemplates();
        url='{$external_content->full_url}';
        max={if $external_content->template_params_decoded->max}{$external_content->template_params_decoded->max}{else}10{/if};
        {if $external_content->template_params_decoded->general_label}general_label='{$external_content->template_params_decoded->general_label}';{/if}
        run();
    });
</script>

<h2 id="name-header">{$requested_category.title}</h2>

<!--
<a href="{$external_content->full_url}" target="_blank">{$external_content->full_url}</a>
-->

{if $content}
    <p>
        {$content}
    </p>
{/if}

<div id=results></div>

<div class="inline-templates" id="xcLinkTpl">
    <!--
        <a href="http://www.xeno-canto.org/species/{$external_content->subst_values['%GENUS%']}-{$external_content->subst_values['%SPECIES%']}?query=area:europe" target="_blank">
        Show all %TOTAL% recordings from Europe at Xeno-canto
        </a>
     -->
</div>

<div class="inline-templates" id="aSoundTpl">
    <!--
        <p>
        <audio class="my_audio" controls preload="none">
            <source src="%MP3-PATH%" type="audio/mpeg">
        </audio><br />
        <a href="%URL%" target="_blank" title="%LABEL%">%URL%</a><br />
        Recorded by: %CREATOR% (%SPATIAL%, %TEMPORAL%)<br />
        License: <a href="%LICENSE%" target="_blank">%LICENSE%</a>
        </p>
    -->
</div>

<div class="inline-templates" id="errorOccurredTpl">
    <!--
        An error has occurred (%STATUS%).
    -->
</div>

<div class="inline-templates" id="noResultsTpl">
<!--
    <div>
        No sound recordings from Europe found for this species.<br>
        Visit the
        <a href="http://www.xeno-canto.org/species/{$external_content->subst_values['%GENUS%']}-{$external_content->subst_values['%SPECIES%']}" target="_blank">
        Xeno-canto species page
        </a> for any recordings.
    </div>
-->
</div>
