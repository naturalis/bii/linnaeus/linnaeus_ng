{*
    Use the associative array below to sort the NSR legislation traits into categories.
    The laws must exactly match the trait sysnames, otherwise they will not be displayed.
    The value of the law is the url to its description in Drupal.

    Note that Rode Lijsten NL -> /rode-lijsten is hard-coded in the template, as it is
    parsed as an exception.

    Any new (or renamed) laws must be added to this template!
*}

{assign var=legislation value=[
    'Wetgeving' => [
        'Visserijwet' => 'visserijwet',
        'Wet natuurbescherming' => 'wet-natuurbescherming'
    ],
        'EU-regelingen' => [
        'CITES' => 'cites-verordeningen',
        'Habitatrichtlijn' => 'habitatrichtlijn',
        'Vogelrichtlijn' => 'vogelrichtlijn',
        'EU-exotenverordening' => 'eu-exotenverordening'
    ],
    'Internationale verdragen en overeenkomsten' => [
        'AEWA' => 'aewa',
        'Ascobans' => 'ascobans',
        'Bats Agreement' => 'bats-agreement',
        'Bern-conventie' => 'bern-conventie',
        'Bonn-conventie' => 'bonn-conventie',
        'Wadden Sea Seals' => 'wadden-sea-seals'
    ],
    'Beleid' => [
        'Soorten van de leefgebiedenbenadering' => 'leefgebiedenbenadering'
    ],
    'Signalering' => [
        'NEM' => 'nem'
    ],
    'Internationale rode lijsten' => [
        'IUCN Red List' => 'iucn-red-list',
        'OSPAR' => 'ospar'
    ]
]}

<style>
.traits.help {
	vertical-align:super;
	font-size:0.8em;
	font-weight:bold;
	margin-left:2px;
}
table.traits {
	width:510px;	
	border-collapse:collapse;
    margin-bottom: 40px;
}
table.traits td.credits {
    padding-top: 50px;
    vertical-align:middle;
}
table.traits td {
	padding:1px 8px 1px 0;
}
table.traits td li {
	list-style-position: inside;
}
.legend-cell {
	width: 250px;
}
td.criterium {
    padding-left: 20px !important;
}
.category {
    font-weight: bold;
    height: 40px;
    vertical-align:bottom;
}
.last-row {
	border-bottom:1px solid #eee;
	padding-bottom:0px;
}
ul.traits {
	list-style-type: disc;
	list-style-position: inherit;
}
ul.traits li {
	margin-left:12px;
}

img.rvo-logo {
    max-height: 94px;
}
</style>

<div style="margin-bottom:10px">

    {if $content}
    <div>
        {$content}
    </div>
    {/if}
    
    <div>
        <h2 id="name-header">{t}{if $external_content->template_params_decoded->page_title}{$external_content->template_params_decoded->page_title}{else}Kenmerken{/if}{/t}
        {if $external_content->template_params_decoded->help_url}
	        <a href="{$external_content->template_params_decoded->help_url}" target="_blank" title="{t}klik voor help over dit onderdeel{/t}" class="traits help">?</a>
        {/if}
        </h2>

        {if $external_content->content_json_decoded->result->data|@count > 0}
            <table class="traits">
                {foreach from=$legislation item=laws key=category}
                    {counter assign=i start=0 print=false}
                    {foreach from=$laws key=law item=drupal_url}
                        {foreach from=$external_content->content_json_decoded->result->data item=v}
                            {if $v->trait->sysname==$law}
                            {counter}
                            {if $i==1}
                                <tr><td class="legend-cell category" colspan="2">{$category}</td></tr>
                                <tr><td class="last-row" colspan="2"></td></tr>
                            {/if}
                            {foreach from=$v->values item=l key=k}
                                {capture "value"}{$l->value_start}{if $l->value_end} - {$l->value_end}{/if}{/capture}
                                 <tr>
                                     <td class="legend-cell">{if $k==0}<a href="/{$drupal_url}">{$v->trait->name}</a>{/if}</td>
                                    <td>{if $v->values|@count>1}<li>{/if}{$smarty.capture.value}</li></td>
                                </tr>
                            {/foreach}
                            <tr><td class="last-row" colspan="2"></td></tr>
                            {/if}
                        {/foreach}
                    {/foreach}
                {/foreach}

                {assign var="red_list" value=false}
                {foreach from=$external_content->content_json_decoded->result->data item=v}
                    {if $v->trait->sysname=='Rode Lijsten NL'}
                        {foreach from=$v->values item=l key=k}
                            {assign var="red_list" value=$l->value_start}
                        {/foreach}
                    {/if}
                {/foreach}

                {if $red_list}
                    <tr><td class="legend-cell category" colspan="2">Nationale Rode Lijsten</td></tr>
                    <tr><td class="last-row" colspan="2"></td></tr>
                    <tr>
                        <!-- Link to Drupal Rode lijsten is hard-coded here! -->
                        <td class="legend-cell"><a href="/rode-lijsten">Categorie</a></td>
                        <td>{$red_list}</td>
                    </tr>
                    <tr><td class="last-row" colspan="2"></td></tr>

                {/if}
            </table>
        {else}
            <p>Deze soort is niet opgenomen in een van de wetten, regelingen of beleidslijnen die op deze site worden bijgehouden. Voor een overzicht van alle opgenomen wetten, regelingen of beleidslijnen gaat u naar <a href="/wetten">Wetten, regelingen en beleid</a>.</p>
        {/if}

        <p>
            <a href="https://www.rijksoverheid.nl/ministeries/ministerie-van-landbouw-visserij-voedselzekerheid-en-natuur" target="_blank"><img class="rvo-logo" src="../../media/system/skins/linnaeus_ng_integrated/logo-lvvn.png" alt="Ga naar de website voor Ministerie van LVVN"/></a>
            <a href="https://www.rvo.nl" target="_blank"><img class="rvo-logo" src="../../media/system/skins/linnaeus_ng_integrated/logo-rvo.png" alt="Ga naar de website voor Rijksdienst voor ondernemend Nederland"/></a>
            <br>Deze informatie wordt mogelijk gemaakt door het <a href="https://www.rijksoverheid.nl/ministeries/ministerie-van-landbouw-visserij-voedselzekerheid-en-natuur">Ministerie van Landbouw, Visserij, Voedselzekerheid en Natuur</a> en beheerd door <a href="https://www.rvo.nl" target="_blank">Rijksdienst voor Ondernemend Nederland</a>.
        </p>

    {if $external_content->content_json_decoded->result->references}
        <br />
        <h4 class="source">{t}Publicatie{if $external_content->content_json_decoded->result->references|@count>1}s{/if}{/t}</h4>
        <ul class="traits">
        
        {foreach from=$external_content->content_json_decoded->result->references item=v}
            {if $external_content->content_json_decoded->result->references|@count>1}<li>{/if}
                {$v->formatted}
            {if $external_content->content_json_decoded->result->references|@count>1}</li>{/if}
        {/foreach}
        </ul>
    {/if}
    </div>
</div>