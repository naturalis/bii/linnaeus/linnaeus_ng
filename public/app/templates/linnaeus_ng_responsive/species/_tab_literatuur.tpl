{if $content.literature|@count > 0}
    <h2 id="name-header">{t}Literatuur{/t}</h2>
    <ul>
    {foreach $content.literature v k}
        <li class="general-list">{$v.formatted}</li>
    {/foreach}
    </ul>
    <br />
{/if}

{if $content.inherited_literature|@count > 0}
    <h2>{t}Literatuur gekoppeld aan bovenliggende taxa{/t}</h2>
    <ul>
    {foreach $content.inherited_literature v k}
        <li class="general-list">
             {$v.formatted} (<a href="?id={$v.referencing_taxon.id}">{$v.referencing_taxon.taxon}</a>)
        </li>
    {/foreach}
    </ul>
{/if}
