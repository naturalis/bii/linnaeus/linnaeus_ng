<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\GlossaryController;

$c = new GlossaryController();

$c->indexAction();
