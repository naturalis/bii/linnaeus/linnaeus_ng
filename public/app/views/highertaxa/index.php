<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
// @todo: check if the normal SpeciesController is used anywhere
use Linnaeus\App\Controllers\SpeciesControllerNSR;

$c = new SpeciesControllerNSR();
$c->higherSpeciesIndexAction();
