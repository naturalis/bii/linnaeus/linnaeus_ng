<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\IndexController;

$c = new IndexController();

$c->indexAction();
