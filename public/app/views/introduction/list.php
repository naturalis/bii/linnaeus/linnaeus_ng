<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\IntroductionController;

$c = new IntroductionController();

$c->listAction();
