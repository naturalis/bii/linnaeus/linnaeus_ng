<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\LinnaeusController;

$c = new LinnaeusController();
$c->setStoreHistory(false);
$c->ajaxInterfaceAction();
