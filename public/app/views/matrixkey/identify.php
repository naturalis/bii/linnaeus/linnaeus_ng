<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\MatrixKeyController;

$c = new MatrixKeyController();
$c->identifyAction();
