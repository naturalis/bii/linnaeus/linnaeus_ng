<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\FreeModuleController;

$c = new FreeModuleController();
$c->setStoreHistory(false);
$c->ajaxInterfaceAction();
