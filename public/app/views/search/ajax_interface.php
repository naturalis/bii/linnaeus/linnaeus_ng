<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\SearchControllerGeneral;

$c = new SearchControllerGeneral();
$c->setStoreHistory(false);
$c->ajaxInterfaceAction();
