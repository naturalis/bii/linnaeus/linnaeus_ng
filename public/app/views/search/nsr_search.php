<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\SearchControllerNSR;

$c = new SearchControllerNSR();
$c->searchAction();
