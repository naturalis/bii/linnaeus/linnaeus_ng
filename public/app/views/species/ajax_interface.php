<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\SpeciesController;

$c = new SpeciesController();
$c->setStoreHistory(false);
$c->ajaxInterfaceAction();
