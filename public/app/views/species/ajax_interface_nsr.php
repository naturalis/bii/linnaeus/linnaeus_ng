<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\SpeciesControllerNSR;

$c = new SpeciesControllerNSR();
$c->setStoreHistory(false);
$c->ajaxInterfaceAction();
