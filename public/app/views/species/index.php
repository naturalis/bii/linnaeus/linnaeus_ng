<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\SpeciesControllerNSR;

require_once '../../../../configuration/app/controllers/SpeciesControllerNSR.php';
$c = new SpeciesControllerNSR();
$c->indexAction();
