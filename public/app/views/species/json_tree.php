<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\TreeController;

$c = new TreeController();
$c->jsonTreeAction();
