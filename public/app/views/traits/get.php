<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\TraitsController;

$c = new TraitsController(array('checkForProjectId' => false));
$c->getAction();
