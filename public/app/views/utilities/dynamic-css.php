<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\UtilitiesController;

$c = new UtilitiesController(array('checkForProjectId' => false));
$c->setStoreHistory(false);
$c->dynamicCssAction();

/*

    see
    linnaeus_ng\www\app\templates\templates\utilities\dynamic-css.tpl
    for actual css code

*/
