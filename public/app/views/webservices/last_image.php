<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';
use Linnaeus\App\Controllers\WebservicesController;

$c = new WebservicesController(array('checkForProjectId' => false));
$c->lastImageAction();
