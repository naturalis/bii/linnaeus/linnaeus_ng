<?php

$pId = isset($_GET['p']) ? $_GET['p'] : null;
$url = isset($_GET['u']) ? $_GET['u'] : null;

require_once '../configuration/app/constants.php';

require_once __DIR__ . '/../vendor/autoload.php';
use Linnaeus\App\Controllers\Controller;

if (defined('FIXED_PROJECT_ID')) {
    $pId = FIXED_PROJECT_ID;
}

// silent fail on missing project id or no URL
//if (is_null($pId) || is_null($url)) // rewrite of drupal search has no URL
if (is_null($pId)) {
    $pId = 1;
    //return;
}

$c = new Controller();
$file = '../configuration/app/rewrite/rewrite-' . $c->getProjectFSCode($pId) . '.php';

// silent fail on no file with rewrite actions
if (!file_exists($file)) {
    $c->log("Rewrite action file is missing in configuration/app/rewrite", 2);
    return;
}

$parameters = array();

foreach ((array)$_GET as $key => $val) {
    if ($key != 'p' && $key != 'u') {
        $parameters[$key] = $val;
    }
}

/*
    available variables:
    $url: original requested URL
    $parameters: query-parameterd of original request (if applicable)
*/

require_once $file;
