<?php
// phpcs:ignoreFile

use XMLReader;

class RemoteService
{

    /*
        class to retrieve data from a remote webservice
        designed to circumvent AJAX cross-domain policies.

        known issues: no attempt is made to

        usage:

        $r = new RemoteService;
        $r->setUrl( 'http://domain.com/path/to/remote/webservice.asp' );
        $r->fetchData();
        $r->sendHeaders();
        $r->printData();
            or
        $data = $r->getData();  // without the headers

    */

    private $url;
    private $timeout = 10;
    private $data;
    private $headers;
    private $requestHeaders = ["Connection: close"];

    public function setUrl($url)
    {
        // Do not allow access to local files!
        $this->url = str_replace('file:', '', $url);
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setTimeout($timeout)
    {
        if (!is_null($timeout) && is_numeric($timeout)) {
            $this->timeout = $timeout;
        }
    }

    public function getTimeout()
    {
        return $this->timeout;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function fetchData()
    {
        try {
            $this->initialize();
            $this->fetchRemoteData();
            $this->validateData();
        } catch (Exception $e) {
            die("failed: " . $e->getMessage());
        }
    }

    public function sendHeaders()
    {
        foreach ((array)$this->headers as $val) {
            header($val);
        }
    }

    public function printData()
    {
        echo $this->data;
    }

    private function validateData ()
    {
        // json
        if (is_string($this->data) && is_array(json_decode($this->data, true))) {
            return;
        }

        // ... or oai-pmh xml
        $xml = new XMLReader();
        if (!$xml->XML($this->data)) {
            die('remote service returned invalid json or OAI-PMH xml');
        }
        $xml->setParserProperty(XMLReader::VALIDATE, true);
        $xml->setSchema('http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd');
        while ($xml->read()) {
            if (!$xml->isValid()) {
                die('remote service returned invalid OAI-PMH xml');
            }
        }
    }

    public function getData()
    {
        return $this->data;
    }

    public function setRequestHeaders($h)
    {
        if (is_array($h)) {
            $this->requestHeaders = array_merge($this->requestHeaders, $h);
        } else {
            $this->requestHeaders[] = $h;
        }
    }

    private function initialize()
    {
        if (empty($this->getUrl())) {
            die("no URL");
        }
        if (!filter_var($this->getUrl(), FILTER_VALIDATE_URL)) {
            die("not a valid URL");
        }
    }

    private function fetchRemoteData()
    {
        $this->setData(
            file_get_contents(
                $this->getUrl(),
                false,
                stream_context_create([
                        "ssl" => [
                            "verify_peer" => false,
                            "verify_peer_name" => false
                        ],
                        "http" => [
                            "header" => implode("\r\n", $this->requestHeaders) . "\r\n",
                            "ignore_errors" => true,
                            "timeout" => $this->getTimeout()
                        ]
                    ])
            )
        );

        $this->setHeaders($http_response_header);
    }

    private function setHeaders($headers)
    {
        $this->headers = $headers;
    }
}
