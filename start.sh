#!/bin/sh

set -eu

composer install || (echo "Please install composer (https://getcomposer.org/)"; exit 1)
mkdir -p data/linnaeus_ng
chmod -R 777 data/linnaeus_ng
docker-compose up
