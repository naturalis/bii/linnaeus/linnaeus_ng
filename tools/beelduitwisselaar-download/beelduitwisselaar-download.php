<?php

require_once __DIR__ . "/config.php";
require_once __DIR__ . "/class.beelduitwisselaar-download.php";

const NL = 24;

$downloader = new BeeldbankDownloader();

$downloader->setPrintParameters(false);

if (!empty($fetchFromDateOverride)) {
    $downloader->setFetchFromDateOverride($fetchFromDateOverride);
}

if (!empty($urlWebImageSearch)) {
    $downloader->seturlWebImageSearch($urlWebImageSearch);
}

$downloader->setConnectData($conn);
$downloader->setWebserviceUrl($webSeviceUrl);
$downloader->setFetchLimit(9999);

$downloader->setScpRemoteUser($remoteHost['user']);
$downloader->setScpRemoteUserPrivKeyFile(__DIR__ . '/imageupload.key');
$downloader->setScpRemoteAddress($remoteHost['host']);
$downloader->setScpRemoteBasePath($remoteHost['path']);
$downloader->setScpRemoteFolder($remoteHost['folder']);

$downloader->setDoWriteDeleteList(true);
$downloader->setScpOriginUser($origHost['user']);
$downloader->setScpOriginUserPrivKeyFile(__DIR__ . '/imageupload.key');
$downloader->setScpOriginAddress($origHost['host']);
$downloader->setScpOriginBasePath($origHost['path']);

// Default to Dutch for NSR; added to config for CSR!
$downloader->setMetaDataLanguage(!empty($lang) ? $lang : NL);
$downloader->run();
