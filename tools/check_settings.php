<?php

namespace Linnaeus;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../configuration/admin/constants.php';

use mysqli;
use Symfony\Component\Yaml\Yaml;
use Linnaeus\Admin\Configuration;

$yaml = __DIR__ . '/../configuration/default_settings.yml';

class CheckSettings
{
    private $mysqli;
    private $settings;

    private $moduleId;
    private $module;
    private $added = [];
    private $errors = [];

    public function __construct()
    {
        $config = new Configuration();
        $db = $config->getDatabaseSettings();
        $this->mysqli = new mysqli($db['host'], $db['user'], $db['password'], $db['database']);
        $this->mysqli->set_charset('utf8');
    }

    public function setYaml($yaml)
    {
        $parser = new Yaml();
        $this->settings = $parser::parseFile($yaml);
    }

    public function run()
    {
        // Delete redundant media setting
        $this->deleteSetting('rs_search_api');
        // Change setting use_variations to use_taxon_variations
        $this->renameSetting('use_variations', 'use_taxon_variations');

        foreach ($this->settings as $this->module => $moduleData) {
            $this->moduleId = $this->getModuleId($this->module);
            foreach ($moduleData['settings'] as $setting => $value) {
                if (!$this->settingExists($setting)) {
                    if ($this->addSetting($setting, $value['info'], $value['default_setting']) > 0) {
                        $this->added[] = $this->module . ': setting "' . $setting . "\" added";
                    } else {
                        $this->errors[] = $this->module . ': "' . $setting . "\" could NOT be added!";
                    }
                } else {
                    print $setting . " already set\n";
                }
            }
        }

        die($this->printResult());
    }

    private function getModuleId($module)
    {
        $r = null;
        $stmt = $this->mysqli->prepare('select id from modules where module = ?');
        $stmt->bind_param('s', $module);
        $stmt->execute();
        $stmt->bind_result($r);
        $stmt->fetch();
        $stmt->close();
        return $r ?? -1;
    }

    private function settingExists($setting)
    {
        $r = null;
        $stmt = $this->mysqli->prepare('select id from module_settings where setting = ?');
        $stmt->bind_param('s', $setting);
        $stmt->execute();
        $stmt->bind_result($r);
        $stmt->fetch();
        $stmt->close();
        return !empty($r);
    }

    // Should include module, but deliberately skipped, as for its sole purpose this may
    // casuse problems. The multi-access key has been renamed several times...
    private function renameSetting($oldName, $newName)
    {
        $q = 'update module_settings set setting = ? where setting = ?';
        $stmt = $this->mysqli->prepare($q);
        $stmt->bind_param('ss', $newName, $oldName);
        $stmt->execute();
        $stmt->close();
    }

    private function addSetting($setting, $info, $default)
    {
        $q = '
            insert into module_settings 
                (module_id, setting, info, default_value, created) 
            values 
                (?, ?, ?, ?, now())
        ';
        $stmt = $this->mysqli->prepare($q);
        $stmt->bind_param('isss', $this->moduleId, $setting, $info, $default);
        $stmt->execute();
        $stmt->close();
        return $this->mysqli->insert_id;
    }

    // Used for just one setting; add module to method when this is more broadly used!
    private function deleteSetting($setting)
    {
        $id = null;
        $q = 'select id from module_settings where setting = ?';
        $stmt = $this->mysqli->prepare($q);
        $stmt->bind_param('s', $setting);
        $stmt->execute();
        $stmt->bind_result($id);
        $stmt->fetch();
        $stmt->close();
        if (!empty($id)) {
            $this->mysqli->query('delete from module_settings where id = ' . $id);
            $this->mysqli->query('delete from module_settings_values where setting_id = ' . $id);
        }
    }

    private function printResult()
    {
        $output = "Result:";
        if (empty($this->added) && empty($this->errors)) {
            return $output . " project up-to-date, no updates required.\n";
        }
        if (!empty($this->added)) {
            foreach ($this->added as $added) {
                $output .= "\n- $added";
            }
        }
        if (!empty($this->errors)) {
            foreach ($this->errors as $error) {
                $output .= "\n- $error";
            }
        }
        return $output .= "\n";
    }
}

// Get to work!
$app = new CheckSettings();
$app->setYaml($yaml);
$app->run();
