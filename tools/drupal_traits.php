<?php
// phpcs:ignoreFile

/*
// Run with debugger!
php -dxdebug.start_with_request=yes tools/drupal_traits.php

// Drupal database
// -> import from backup on https://minlnv.nederlandsesoorten.nl

// Mapping Drupal laws, categories and subcategories to NSR traits
// -> import tsv from https://naturalis.atlassian.net/browse/LNV-75 into table _lnv_to_lng_mapping

// Species matching by NSR id
insert into _lnv_to_lng_species
    select t4.id, t1.field_wetenschappelijke_naamsrt_value, t2.field_nsr_id_value, t4.taxon, t3.nsr_id
    from lnv.field_data_field_wetenschappelijke_naamsrt t1
    left join lnv.field_data_field_nsr_id t2 on t1.entity_id = t2.entity_id
    left join develop.nsr_ids t3 on t2.field_nsr_id_value = trim(leading '0' from substring_index(nsr_id, "/", -1))
    left join develop.taxa t4 on t3.lng_id = t4.id
    where t3.nsr_id like 'tn.nlsr.concept%' or t3.nsr_id is null
    order by t1.field_wetenschappelijke_naamsrt_value

// Species matching by scientific name. Needs cleaning for duplicates as sometimes multiple subspecies are
// matching to a single species
insert into _lnv_to_lng_species_update
    select t2.id, t1.lnv_wetenschappelijk, null, t2.taxon, t3.nsr_id from _lnv_to_lng_species t1
    left join taxa t2 on SUBSTRING_INDEX(t1.lnv_wetenschappelijk, ' ', 2) = SUBSTRING_INDEX(t2.taxon, ' ', 2)
    left join nsr_ids t3 on t2.id = t3.lng_id
    where t1.id is null and t2.taxon is not null;

*/

require_once __DIR__ . '/../vendor/autoload.php';

use mysqli;
use Linnaeus\App\Configuration;

class LnvTraitsExport
{
    private $mysqliDrupal;
    private $mysqliNsr;
    private $species = [];
    private $laws = [];
    private $lawsToSpecies = [];
    private $lawsToSkip = [
        'Flora- en faunawet',
        'Doelsoortenlijst',
        'Natuurbeschermingswet'
    ];
    private $csv = [];
    private $categories = [];


    private function init()
    {
        $config = new Configuration();
        $settings = $config->getDatabaseSettings();
        $this->mysqliDrupal = new mysqli($settings['host'], 'root', 'root', 'lnv');
        $this->mysqliNsr = new mysqli($settings['host'], 'root', 'root', 'develop');
    }

    public function runLaws()
    {
        $this->init();
        $this->setSpecies();
        $this->setLaws();
        $this->setLawsToSpecies();

        $batches = array_chunk($this->species, 500, true);
        foreach ($batches as $i => $batch) {
            $this->createLawsCsv($batch);
            $this->writeCsv('lnv_traits_' . ($i + 1));
        }
    }

    public function runCategories()
    {
        $this->init();
        $this->setSpecies();
        $this->setCategories();

        $this->createCategoriesCsv();
        $this->writeCsv('lnv_categories');
    }

    private function createLawsCsv($batch = false)
    {
        // If no batch is given, all records will be processed at once
        if (!$batch) {
            $batch = $this->species;
        }

        $header = ['Species', 'ID SRTregister'];
        foreach ($this->laws as $law => $categories) {
            $header[] = $law;
            foreach ($categories as $category) {
                $header[] = $category;
            }
        }
        $matrix[] = $header;

        foreach ($batch as $species => $nsr) {
            $row = [$nsr['nsr_scientific'], $nsr['nsr_id']];
            foreach ($this->laws as $law => $categories) {
                $row[] = '';
                foreach ($categories as $category) {
                    if (isset($this->lawsToSpecies[$species][$law]) && $this->lawsToSpecies[$species][$law] == $category) {
                        $row[] = 'Yes';
                    } else {
                        $row[] = 'No';
                    }
                }
            }
            $matrix[] = $row;
        }

        // Transpose data
        foreach ($matrix as $key => $subArray) {
            foreach ($subArray as $subKey => $subValue) {
                $this->csv[$subKey][$key] = $subValue;
            }
        }
    }

    private function writeCsv($fileName)
    {
        $fp = fopen("/tmp/$fileName.tsv", 'w');
        foreach ($this->csv as $row) {
            foreach ($row as $value) {
                fwrite($fp, "$value\t");
            }
            fwrite($fp, "\r\n");
        }
        fclose($fp);
    }

    private function setLawsToSpecies($writeListOnly = false)
    {
        if ($writeListOnly) {
            $fp = fopen("/tmp/lnv.tsv", 'w');
            fwrite($fp, "wet\tcategorie\tsubcategorie\tsoort\r\n");
        }

        $result = $this->mysqliDrupal->query(
            "select t1.title, t2.field_subonderdeel_value as sub
                from node t1
                left join field_data_field_subonderdeel t2 on t1.nid = t2.entity_id
                where t1.type = 'regelgeving' and t1.title not like '%node:field-categorie:title%'"
        );

        while ($row = $result->fetch_object()) {
            $law = strtok($row->title, ':');
            $name = explode('@', str_replace(' - ', '@', $row->title))[1];
            preg_match_all('~\(([^()]*)\)~', $name, $matches);
            $species = $matches[1][0] ?? trim($name);
            $category = substr($row->title, strlen($law) + 2, -(strlen($name) + 3));

            if ($writeListOnly) {
                fwrite($fp, "$law\t$category\t{$row->sub}\t$species\r\n");
                continue;
            }

            if (!empty($species) && !in_array($law, $this->lawsToSkip)) {
                $subcategory = $row->sub;
                $nsrLaw = $this->getNsrLaw($law, $category, $subcategory);
                if (!empty($nsrLaw)) {
                    $this->lawsToSpecies[$species][$nsrLaw->law] = $nsrLaw->category;
                } else {
                    die("$species: $law - $category has no mapping");
                }
            }
        }
        if ($writeListOnly) {
            fclose($fp);
            die("List file lnv.tsv written. Remove true from setLawsToSpecies() for a regular export.\n\n");
        }
    }

    private function setLaws()
    {
        $result = $this->mysqliNsr->query("select distinct nsr_wet, nsr_cat from _lnv_to_lng_mapping");
        while ($row = $result->fetch_object()) {
            $this->laws[$row->nsr_wet][] = $row->nsr_cat;
        }
    }

    private function setSpecies()
    {
        $result = $this->mysqliNsr->query(
            "select id, lnv_wetenschappelijk as species, lng_wetenschappelijk as nsr_scientific,
                    trim(leading '0' from substring_index(lng_nsr_concept, '/', -1)) as nsr_id,
                    lnv_node_id as node_id
                from _lnv_to_lng_species 
                where id is not null

                union
                
                select id, lnv_wetenschappelijk as species, lng_wetenschappelijk as nsr_scientific, 
                    trim(leading '0' from substring_index(lng_nsr_concept, '/', -1)) as nsr_id,
                    lnv_node_id as node_id
                from _lnv_to_lng_species_update"
        );

        while ($row = $result->fetch_object()) {
            $this->species[$row->species] = [
                'nsr_id' => $row->nsr_id,
                'nsr_scientific' => $row->nsr_scientific,
                'node_id' => $row->node_id
            ];
        }
    }

    private function getNsrLaw($law, $category, $subcategory)
    {
        $query = "
                select nsr_wet as law, nsr_cat as category
                from _lnv_to_lng_mapping 
                where lnv_wet = '$law' and lnv_cat = '$category' ";
        if (!empty($subcategory)) {
            $query .= "and lnv_subcat = '$subcategory'";
        }
        $result = $this->mysqliNsr->query($query);
        return $result->fetch_object();
    }

    function getNsrId($species)
    {
        $result = $this->mysqliDrupal->query(
            "select t1.field_nsr_id_value as nsr_id
                from field_data_field_wetenschappelijke_naamsrt t1
                left join field_data_field_nsr_id t2 on t1.entity_id = t2.entity_id
                where t1.field_wetenschappelijke_naamsrt = '$species'"
        );
        return $result->fetch_object()->nsr_id;
    }

    private function setCategories()
    {
        $query = "
                select replace(t2.name, 'Soort', '') as category, t1.tid as id, t1.name
                from taxonomy_term_data as t1  
                left join taxonomy_vocabulary as t2 on t1.vid = t2.vid
                where t2.name in ('Soortstatus', 'Soortzeldzaamheid', 'Soorttrend')
                order by t2.name, t1.name";
        $result = $this->mysqliDrupal->query($query);
        while ($row = $result->fetch_object()) {
            $this->categories[$row->category][$row->id] = $row->name;
        }
    }

    private function getSpeciesStatuses($nsrId)
    {
        $statuses = [];
        $result = $this->mysqliDrupal->query("select tid from taxonomy_index where nid = $nsrId");
        while ($row = $result->fetch_object()) {
            $statuses[] = $row->tid;
        }
        return $statuses;
    }

    private function createCategoriesCsv()
    {
        $header = ['Species', 'ID SRTregister'];
        foreach ($this->categories as $category => $statuses) {
            $header[] = $category;
            foreach ($statuses as $status) {
                $header[] = $status;
            }
        }
        $matrix[] = $header;

        foreach ($this->species as $nsr) {
            $row = [$nsr['nsr_scientific'], $nsr['nsr_id']];
            foreach ($this->categories as $category => $statuses) {
                $row[] = '';
                $speciesStatuses = $this->getSpeciesStatuses($nsr['node_id']);
                foreach ($statuses as $statusId => $status) {
                    if (in_array($statusId, $speciesStatuses)) {
                        $row[] = 'Yes';
                    } else {
                        $row[] = 'No';
                    }
                }
            }
            $matrix[] = $row;
        }

        // Transpose data
        foreach ($matrix as $key => $subArray) {
            foreach ($subArray as $subKey => $subValue) {
                $this->csv[$subKey][$key] = $subValue;
            }
        }
    }
}

$lnv = new LnvTraitsExport();
$lnv->runLaws();
$lnv->runCategories();
