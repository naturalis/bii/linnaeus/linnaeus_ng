<?php

/*
 * This script can be used to export urls of NSR images for a specific group as requested in LINN-1551.
 *
 * This should be possible with a single query (see LINNA-1551), but in practice this proved to perform
 * extremely poorly.
 *
 * Usage:
 * $csvPath should point to a writable dir (this script doesn't bother to check if it's actually writable)
 * $groupId is the id of the higher taxon including the species of which the info should be exported
 * $groupName is used only to format the resulting csv file
 */
    require_once __DIR__ . '/../vendor/autoload.php';
    use Linnaeus\App\Configuration;

    $csvPath = '/tmp/';
    $groupId = 116299;
    $groupName = "plants";

    $fp = fopen($csvPath . 'nsr_photos_' . $groupName . '.csv', 'w');
    fputcsv($fp, ["genus", "species", "author", "url", "photographer", "address", "license"]);

    $c = new Configuration();
    $s = $c->getDatabaseSettings();
    $d = @mysqli_connect($s['host'], $s['user'], $s['password']) or die('cannot connect');
    @mysqli_select_db($d, $s['database']) or die('cannot select db');

    $sql = "
        select t1.id, t1.taxon_id, t1.file_name as url
        from media_taxon t1
        where t1.taxon_id in (
            select taxon_id 
            from taxon_quick_parentage 
            where match(parentage) against ('$groupId' in boolean mode)
        )";
    $res = $mysqli->query($sql) or die($mysqli->error);

while ($r = $res->fetch_array()) {
    $mediaId = $r['id'];
    $taxonId = $r['taxon_id'];
    $url = "https://images.naturalis.nl/original/" . $r['url'];
    list($genus, $species, $author) = getName($taxonId);
    $photographer = getMeta($mediaId, 'beeldbankFotograaf');
    $license = getMeta($mediaId, 'beeldbankLicentie');
    $address = getMeta($mediaId, 'beeldbankAdresMaker');

    echo "Writing {$r['url']} for $genus $species...\n";
    fputcsv($fp, [$genus, $species, $author, $url, $photographer, $address, $license]);
}

function getName($id)
{
    global $mysqli;
    $sql = "
            select uninomial, specific_epithet, authorship 
            from names
            where taxon_id = $id and type_id = 1";
    $res = $mysqli->query($sql);

    return $res->fetch_array();
}

function getMeta($id, $type)
{
    global $mysqli;
    $sql = "
            select meta_data 
            from media_meta
            where media_id = $id and sys_label  = '$type'";
    $res = $mysqli->query($sql);
    $row =  $res->fetch_row();

    return $row ? $row[0] : null;
}
