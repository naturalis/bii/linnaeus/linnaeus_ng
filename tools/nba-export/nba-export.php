<?php

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . "/../../configuration/admin/constants.php";

const LIFE = 116297;
const NL = 24;
const EN = 26;

$project_name = getenv('NBA_PROJECT') ?: 'nsr';
$concept_base_url = getenv('CONCEPT_BASE_URL') ?: 'https://nederlandsesoorten.nl/nsr/concept/';
$image_base_url = getenv('IMAGE_BASE_URL') ?: 'https://images.naturalis.nl/original/';
$language_id = getenv('NBA_LANGUAGE_ID') ?: NL;
$batch_size = getenv('EXPORT_BATCH_SIZE') ?: 10000;
$memory_limit = getenv('PHP_MEMORY_LIMIT') ?: '2048M';
$supress_ids = [LIFE];

$outdir = "/export/";
$outfilebasename = $project_name . "-export";
$filelist = "filelist";
$tag = date('Y.m.d--H.i.s');

if (!file_exists($outdir)) {
    if (!mkdir($outdir) && !is_dir($outdir)) {
        throw new \RuntimeException(sprintf('Directory "%s" was not created', $outdir));
    }
}

$files = glob($outdir . $outfilebasename . '*');
foreach ($files as $file) {
    if (is_file($file)) {
        echo "deleting " . $file . "\n";
        unlink($file);
    }
}

echo "truncating " . $outdir . $filelist . "\n";
$fp = fopen($outdir . $filelist, 'w');
fclose($fp);


$config = new \Linnaeus\Admin\Configuration();
$conn = $config->getDatabaseSettings();
$conn['project_id'] = 1;

require_once "class.nba-export.php";

ini_set('memory_limit', $memory_limit);

$exporter = new TaxonExporter();
$exporter->setConnectData($conn);
$exporter->setLanguageId($language_id);
$exporter->setIdsToSuppressInClassification($supress_ids);

$exporter->setConceptBaseUrl($concept_base_url);
$exporter->setImageBaseUrl($image_base_url);

$exporter->setValidNameTypeId(1);
$exporter->setFileNameBase($outfilebasename);
$exporter->setBatchSize($batch_size); // records per output file (files are numbered -00, -01 etc)
$exporter->setExportFolder($outdir);
$exporter->run();

echo "writing to " . $outdir . $filelist . "\n";
file_put_contents($outdir . $filelist, implode(PHP_EOL, $exporter->getFilelist()));
