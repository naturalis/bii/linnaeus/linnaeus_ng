<?php

require_once __DIR__ . '/../vendor/autoload.php';
use Linnaeus\App\Configuration;

$c = new Configuration();

$s = $c->getDatabaseSettings();

echo '
  this script changes the table prefix in a linnaeus NG database.

  reading from config file ' . $cfg . '  
  the current database is: ' . $s['database'] . ' @ ' . $s['host'] . '
  the current table prefix is: ' . $s['tablePrefix'] . '
  please enter the new table prefix (including optional ending underscore).
  leave the option  blank to remove the current prefix.
  
  ';

$prefix = prompt('enter prefix (or leave empty to remove): ');

echo '  about to change the prefix on all tables in ' . $s['database'] . ' from ' . $s['tablePrefix'] . ' to ' . ($prefix ? $prefix : '(no prefix)') . '
  
  ';

$cont = prompt('continue? (y/n) ', array('y', 'n'));

if ($cont != 'y') {
    die('
  aborted
	');
}

$d = @mysqli_connect($s['host'], $s['user'], $s['password']) or die('cannot connect');
@mysqli_select_db($d, $s['database']) or die('cannot select db');

$q = mysqli_query($d, 'show tables');

while ($r = mysqli_fetch_array($d)) {
    $pF = substr($r[0], 0, strlen($s['tablePrefix']));

    if ($pF == $s['tablePrefix']) {
        $new = $prefix . substr($r[0], strlen($s['tablePrefix']));
        echo '  renaming ' . $r[0] . ' to ' . $new . ' ... ';
        if (mysqli_query($d, 'rename table ' . $r[0] . ' to ' . $new) == 1) {
            echo 'done';
        } else {
            echo 'failed';
        }
        echo chr(10);
    } else {
        echo '  ignoring ' . $r[0] . ' (has no or wrong prefix)' . chr(10);
    }
}

mysqli_close($d);

echo '
  done
	
';

function prompt($prompt, $validInputs = null, $default = '')
{
    while (
        !isset($input) ||
        (is_array($validInputs) && !in_array($input, $validInputs)) ||
        (is_null($validInputs) && !isset($input)) ||
        ($validInputs == 'is_file' && !is_file($input))
    ) {
        echo $prompt;
        $input = strtolower(trim(fgets(STDIN)));
        if (empty($input) && !empty($default)) {
            $input = $default;
        }
    }
    return $input;
}
