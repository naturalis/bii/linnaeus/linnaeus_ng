<?php

namespace Linnaeus;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../configuration/admin/constants.php';

use Linnaeus\Admin\Configuration;
use Linnaeus\Admin\Helpers\PasswordEncoder;
use Linnaeus\Admin\Models\Table;

/**
 * Tool commando class to update password of a user
 */
class UpdatePassword
{
    private $errors = [];
    private $arguments = [];

    public function __construct()
    {
        $this->getArguments();
        $this->userModel = new Table('users');
        $this->passwordEncoder = new PasswordEncoder();
    }

    private function getArguments()
    {
        global $argv, $argc;

        $msg = "You need to supply a username and password.\n";
        $msg .= "Only use numbers or alphabetic characters in passwords.\n\n";
        $msg .= sprintf("%s [username] [password]\n", $argv[0]);

        if ($argc != 3) {
            die($msg);
        }

        $this->arguments['username'] = $argv[1];
        $this->arguments['password'] = $argv[2];
    }

    private function getUser($username)
    {
        $users = $this->userModel->getColumn(['id' => ['username' => trim($username)]]);
        if ($users && (count($users) == 1)) {
            return $users[0];
        } elseif ($users && (count($users) > 1)) {
            $this->errors[] = sprintf("More than one user found for username '%s'", $username);
        }
        $this->errors[] = sprintf("No user found for username '%s'", $username);
        return null;
    }

    private function userPasswordEncode($password)
    {
        $this->passwordEncoder->setForceMd5(false);
        $this->passwordEncoder->setPassword($password);
        $this->passwordEncoder->encodePassword();
        return $this->passwordEncoder->getHash();
    }

    private function handleErrors()
    {
        if (count($this->errors)) {
            foreach ($this->errors as $error) {
                print $error . "\n";
            }
            exit(1);
        }
    }

    public function run()
    {
        $user = $this->getUser($this->arguments['username']);
        $this->handleErrors();
        if (password_verify($this->arguments['password'], $user['password'])) {
            printf("Password of user '%s' is the same, no need to update.", $user['username']);

            return;
        }
        $encodedPassword = $this->userPasswordEncode($this->arguments['password']);
        $this->userModel->save([
            'id' => $user['id'],
            'password' => $encodedPassword,
            'last_password_change' => 'now()'
        ]);
        $user = $this->getUser($this->arguments['username']);
        if (password_verify($this->arguments['password'], $user['password'])) {
            printf("Password of user '%s' succesfully updated.", $user['username']);

            return;
        } else {
            printf("Failed to update password of user '%s'.", $user['username']);
            exit(1);
        }
    }
}

$command = new UpdatePassword();
$command->run();
