<?php

namespace Linnaeus;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../configuration/admin/constants.php';

use mysqli;
use Symfony\Component\Yaml\Yaml;
use Linnaeus\Admin\Configuration;

class UpdateSetting
{
    private $mysqli;

    private $errors = [];
    private $arguments = [];

    public function __construct()
    {
        $this->getArguments();
        $config = new Configuration();
        $db = $config->getDatabaseSettings();
        $this->mysqli = new mysqli($db['host'], $db['user'], $db['password'], $db['database']);
        $this->mysqli->set_charset('utf8');
    }

    public function getArguments()
    {
        global $argv, $argc;

        $msg = "You need to supply a setting name.\n\n";
        $msg .= sprintf("%s [setting-name] [setting-value]\n", $argv[0]);
        $msg .= "\nOr if a value isn't supplied, the value is retrieved via <stdin>.\n\n";
        $msg .= sprintf("%s [setting-name] < file-with-value.txt\n", $argv[0]);

        if ($argc < 2) {
            die($msg);
        }

        $this->arguments['default'] = false;
        $argcount = 1;
        if ($argv[$argcount] == '--default') {
            $this->arguments['default'] = true;
            $argcount++;
        }
        $this->arguments['setting'] = $argv[$argcount];
        if (array_key_exists($argcount + 1, $argv)) {
            $argcount++;
            $this->arguments['value'] = $argv[$argcount];
        } else {
            echo "Expecting a value via stdin, end your input with an empty line:\n";
            $this->arguments['value'] = $this->readStdIn();
        }
    }

    public function readStdIn()
    {
        $value = '';
        while ($line = fgets(STDIN)) {
            if (strlen(trim($line)) == 0) {
                break;
            }
            $value .= $line;
        }

        return trim($value);
    }


    public function run()
    {
        $before = ($this->arguments['default']) ? $this->getDefaultSettingValue($this->arguments['setting']) : $this->getSettingValue($this->arguments['setting']);
        if (strcmp($this->arguments['value'], $before) == 0) {
            echo "Settings value is the same, there is no need to update.\n";
            return;
        }
        if (!$before) {
            $this->createSettingValue($this->arguments['setting'], $this->arguments['value'], $this->arguments['default']);
        } else {
            $this->updateSettingValue($this->arguments['setting'], $this->arguments['value'], $this->arguments['default']);
        }

        $after = $this->getSettingValue($this->arguments['setting']);
        if (strcmp($before, $after) == 0) {
            echo "Settings not updated.\n";
        } else {
            echo "Settings value has been updated.\n";
        }
        if (count($this->errors)) {
            var_dump($this->errors);
            exit(1);
        }
    }

    private function getDefaultSettingValue($setting)
    {
        $result = null;
        $stmt = $this->mysqli->prepare('select `default_value` from `module_settings` where `setting` = ?');
        $stmt->bind_param('s', $setting);
        $stmt->execute();
        $stmt->bind_result($result);
        $stmt->fetch();
        $stmt->close();
        return $result;
    }

    private function getSetting($setting)
    {
        $result = null;
        $stmt = $this->mysqli->prepare('select `id` from `module_settings` where `setting` = ?');
        $stmt->bind_param('s', $setting);
        $stmt->execute();
        $stmt->bind_result($result);
        $stmt->fetch();
        $stmt->close();
        return $result;
    }

    private function getSettingValue($setting)
    {
        $settingid = $this->getSetting($setting);
        if (!$settingid) {
            return null;
        }
        $result = null;
        $stmt = $this->mysqli->prepare('select `value` from `module_settings_values` where `setting_id` = ?');
        $stmt->bind_param('s', $settingid);
        $stmt->execute();
        $stmt->bind_result($result);
        $stmt->fetch();
        $stmt->close();
        return $result;
    }

    private function createSettingValue($setting, $value, $default = false)
    {
        if ($default) {
            $stmt = $this->mysqli->prepare('insert into `module_settings` (`setting`, `default_value`) values (?, ?)');
            $stmt->bind_param('ss', $setting, $value);
            $stmt->execute();
            $stmt->close();
        } else {
            $settingid = $this->getSetting($setting);
            if (!$settingid) {
                return null;
            }
            $projectid = FIXED_PROJECT_ID ? FIXED_PROJECT_ID : -1;
            $stmt = $this->mysqli->prepare('insert into `module_settings_values` (`project_id`, `setting_id`, `value`, `created`) values(?, ?, ?, CURRENT_TIMESTAMP)');
            $stmt->bind_param('sss', $projectid, $settingid, $value);
            $stmt->execute();
            if ($stmt->errno) {
                $this->errors[] = ['nr' => $stmt->errno, 'msg' => $stmt->error];
            }
            $stmt->close();
        }
    }

    private function updateSettingValue($setting, $value, $default = false)
    {
        $settingid = $this->getSetting($setting);
        if (!$settingid) {
            return null;
        }
        if ($default) {
            $stmt = $this->mysqli->prepare('update `module_settings` set `default_value` = ? where `setting` = ?');
            $stmt->bind_param('ss', $value, $setting);
            $stmt->execute();
            if ($stmt->errno) {
                $this->errors[] = ['nr' => $stmt->errno, 'msg' => $stmt->error];
            }
            $stmt->close();
        } else {
            $stmt = $this->mysqli->prepare('update `module_settings_values` SET `value`  = ? where `setting_id` = ?');
            $stmt->bind_param('ss', $value, $settingid);
            $stmt->execute();
            if ($stmt->errno) {
                $this->errors[] = ['nr' => $stmt->errno, 'msg' => $stmt->error];
            }
            $stmt->close();
        }

        return (count($this->errors) == 0);
    }
}

$app = new UpdateSetting();
$app->run();
